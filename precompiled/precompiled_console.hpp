#pragma once

//
// Std includes.
//
#include <string>
#include <string_view>
#include <vector>
#include <map>
#include <unordered_map>
#include <memory>
#include <optional>
#include <functional>
#include <algorithm>
#include <type_traits>
#include <utility>
#include <cstdlib>
#include <cwctype>
#include <limits>

//
// Godot includes.
//
#include <core/object/object.h>
#include <core/object/ref_counted.h>
#include <core/object/class_db.h>
#include <core/io/resource.h>
#include <core/os/os.h>
#include <core/os/keyboard.h>

#include <scene/main/node.h>
#include <scene/resources/packed_scene.h>

#include <scene/gui/line_edit.h>
#include <scene/gui/box_container.h>
#include <scene/gui/label.h>
#include <scene/gui/rich_text_label.h>

//
// Basic project includes.
//
#include "bwn_core/containers/arrayView.hpp"
#include "bwn_core/containers/smallVector.hpp"

#include "bwn_core/types/uniqueRef.hpp"
#include "bwn_core/types/objectIdRef.hpp"

#include "bwn_core/types/singleton.hpp"
#include "bwn_core/types/stringId.hpp"
#include "bwn_core/types/stringView.hpp"

#include "bwn_core/utility/engineUtility.hpp"
#include "bwn_core/utility/formatUtility.hpp"
#include "bwn_core/utility/godotFormating.hpp"
#include "bwn_core/utility/methodInfoUtility.hpp"
#include "bwn_core/utility/nodeUtility.hpp"
#include "bwn_core/utility/objectCast.hpp"
#include "bwn_core/utility/objectUtility.hpp"
#include "bwn_core/utility/propertyInfoUtility.hpp"
#include "bwn_core/utility/referenceUtility.hpp"
#include "bwn_core/utility/templateAlgorithms.hpp"
#include "bwn_core/utility/typeParsingUtility.hpp"
#include "bwn_core/utility/variantUtility.hpp"
#include "bwn_core/utility/visitorUtility.hpp"
#include "bwn_core/utility/stringParsingUtility.hpp"

#include "bwn_core/debug/debug.hpp"
#include "bwn_core/debug/debugUtility.hpp"

#include "bwn_core/results/systemFacility.hpp"
#include "bwn_core/results/systemResult.hpp"