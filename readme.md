## Console parsing and execution library for the godot 4

This is small library which allows simple addition of console commands for my other godot modules.

This modules is based on cmake build system implementation and not the scons one.

Other dependencies for this library include:
- bwn_core_gdmodule