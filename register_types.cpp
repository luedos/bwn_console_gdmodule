#include "precompiled_console.hpp"

#include "register_types.h"

#include "bwn_console/consoleDatabase.hpp"
#include "bwn_console/consoleManager.hpp"
#include "bwn_console/consoleLineHistoryDatabase.hpp"
#include "bwn_console/controls/consoleWindow.hpp"
#include "bwn_console/controls/consoleLineEdit.hpp"
#include "bwn_console/controls/consoleItemSelector.hpp"
#include "bwn_console/controls/consoleLineHistory.hpp"
#include "bwn_console/controls/consoleRoot.hpp"
#include "bwn_console/controls/consoleArgumentVariants.hpp"
#include "bwn_console/controls/consoleMethodHint.hpp"

#include "bwn_console/debugMethods.hpp"

void initialize_bwn_console_module(const ModuleInitializationLevel _level)
{
	if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_CORE)
	{
		bwn::ConsoleDatabase::createSingleton();
	#if defined(DEBUG_ENABLED)
		bwn::registerConsoleDebugMethods();
	#endif // DEBUG_ENABLED
	}
	else if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_SERVERS)
	{
		bwn::ConsoleLineHistoryDatabase::createSingleton();
		bwn::ConsoleLineHistoryDatabase::getInstance()->loadDatabase();
	}
	else if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_SCENE)
	{
		GDREGISTER_CLASS(bwn::ConsoleWindow);
		GDREGISTER_CLASS(bwn::ConsoleLineEdit);
		GDREGISTER_CLASS(bwn::ConsoleLineHistory);
		GDREGISTER_CLASS(bwn::ConsoleItemSelector);
		GDREGISTER_CLASS(bwn::ConsoleArgumentVariants);
		GDREGISTER_CLASS(bwn::ConsoleMethodHint);
		GDREGISTER_CLASS(bwn::ConsoleRoot);
		GDREGISTER_CLASS(bwn::ConsoleManager);
	}
}

void uninitialize_bwn_console_module(const ModuleInitializationLevel _level)
{
	if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_CORE)
	{
		bwn::ConsoleDatabase::destroySingleton();
	}
	else if (_level == ModuleInitializationLevel::MODULE_INITIALIZATION_LEVEL_SERVERS)
	{
		bwn::ConsoleLineHistoryDatabase::getInstance()->dumpDatabase();
		bwn::ConsoleLineHistoryDatabase::destroySingleton();
	}
}