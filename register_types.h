#pragma once

#include <modules/register_module_types.h>

// Registers all godot classes
void initialize_bwn_console_module(const ModuleInitializationLevel _level);
// Clean up of the module
void uninitialize_bwn_console_module(const ModuleInitializationLevel _level);