#include "precompiled_console.hpp"

#include "bwn_console/consoleDatabase.hpp"
#include "bwn_console/parser/parser.hpp"
#include "bwn_core/algorithm/stringViewDivider.hpp"

bwn::ConsoleDatabase::ConsoleDatabase() = default;
bwn::ConsoleDatabase::~ConsoleDatabase() = default;

void bwn::ConsoleDatabase::ArgData::setName(const String& _name)
{
	m_name = _name;
}

void bwn::ConsoleDatabase::ArgData::setDefault(const String& _value)
{
	m_default = _value;
}

void bwn::ConsoleDatabase::ArgData::setVariantsCallback(VariantsCallback _callback)
{
	m_variantsCallback = std::move(_callback);
}

void bwn::ConsoleDatabase::ArgData::setDescription(const String& _description)
{
	m_description = _description;
}
    
void bwn::ConsoleDatabase::ArgData::getVariants(const bwn::string_view& _argumentString, std::vector<String>& o_variants) const
{
	o_variants.clear();

	if (m_variantsCallback)
	{
		return m_variantsCallback(_argumentString, o_variants);
	}
}

const String& bwn::ConsoleDatabase::ArgData::getName() const
{
	return m_name;
}

const String& bwn::ConsoleDatabase::ArgData::getDefault() const
{
	return m_default;
}

const String& bwn::ConsoleDatabase::ArgData::getDescription() const
{
	return m_description;
}

bool bwn::ConsoleDatabase::ArgData::hasVariantsCallback() const
{
	return m_variantsCallback.is_valid();
}

bwn::ConsoleDatabase::MethodHolder::MethodHolder(const String& _name)
	: m_name( _name )
{}

bwn::ConsoleDatabase::MethodHolder::~MethodHolder() = default;

const String& bwn::ConsoleDatabase::MethodHolder::getName() const
{
	return m_name;
}

bwn::ConsoleDatabase::MethodData::MethodData(const String& _name)
	: m_name( _name )
{}

void bwn::ConsoleDatabase::MethodData::resizeArgCount(const size_t _count)
{
	m_args.resize(_count);
}

void bwn::ConsoleDatabase::MethodData::setArgInfo(const ConsoleDatabase::ArgData& _info, const size_t _argId)
{
	if (m_args.size() <= _argId) {
		m_args.resize(static_cast<int>(_argId + 1));
	}

	m_args.set(static_cast<int>(_argId), _info);
}

void bwn::ConsoleDatabase::MethodData::setName(const String& _name)
{
	m_name = _name;
}

void bwn::ConsoleDatabase::MethodData::setDescription(const String& _description)
{
	m_description = _description;
}

void bwn::ConsoleDatabase::MethodData::setSignature(const String& _signature)
{
	m_signature = _signature;
}

void bwn::ConsoleDatabase::MethodData::clear()
{
	m_name.clear();
	m_signature.clear();
	m_description.clear();
	m_args.clear();
}

bwn::array_view<const bwn::ConsoleDatabase::ArgData> bwn::ConsoleDatabase::MethodData::getArgInfos() const
{
	return bwn::array_view<const ArgData>(m_args.ptr(), m_args.size());
}

const String& bwn::ConsoleDatabase::MethodData::getName() const
{
	return m_name;
}

const String& bwn::ConsoleDatabase::MethodData::getDescription() const
{
	return m_description;
}

const String& bwn::ConsoleDatabase::MethodData::getSignature() const
{
	return m_signature;	
}

bool bwn::ConsoleDatabase::MethodData::valid() const
{
	return !m_name.is_empty();
}

void bwn::ConsoleDatabase::removeMethod(const MethodId _nameId)
{
	const MethodsMap::const_iterator it = m_methods.find(_nameId);
	BWN_ASSERT_WARNING_COND(it != m_methods.end(), "Can't remove console method \"{}\" which wasn't registered.", _nameId.getString());
	if (it != m_methods.end()) {
		m_methods.erase(it);
	}
}

bwn::SystemResult bwn::ConsoleDatabase::call(const std::basic_string_view<char32_t>& _name, const bwn::array_view<const Variant>& _args) const
{
	const MethodsMap::const_iterator it = m_methods.find(MethodId(_name));

	BWN_ASSERT_WARNING_COND(it != m_methods.end() && it->second, "Can't call console method \"{}\" which wasn't registered.", bwn::wrap_utf(_name));
	if (it == m_methods.end() || !it->second) 
	{
		return bwn::SystemFacility::createError(Error::ERR_METHOD_NOT_FOUND,  bwn::format("Method '{}' is not registered.", bwn::wrap_utf(_name)));
	}

	return it->second->callV(_args);
}

const std::vector<bwn::ConsoleDatabase::MethodData>& bwn::ConsoleDatabase::getMethodsData() const
{
	return m_methodsData;
}

const bwn::ConsoleDatabase::MethodData* bwn::ConsoleDatabase::findMethodData(const bwn::string_view _commandName) const
{
	MethodsDataArr::const_iterator dataIt = std::find_if(
		m_methodsData.begin(), 
		m_methodsData.end(), 
		[_commandName](const MethodData& _data) -> bool
		{
			return _commandName == _data.getName().get_data();
		});

	return dataIt == m_methodsData.end()
		? nullptr
		: dataIt.base();
}

bwn::SystemResult bwn::ConsoleDatabase::executeCommandsFile(const String& _path, const bool _force) const
{
	Error error = Error::OK;
	const String fileContent = FileAccess::get_file_as_string(_path, &error);

	if (error != Error::OK)
	{
		return SystemFacility::createError(error, "Fail to open file");
	}

	StringViewDivider divider = StringViewDivider(bwn::wrap_view(fileContent));

	console::ParserView parser;
	std::vector<Variant> parsedArguments;
	while (!divider.finished())
	{
		const bwn::string_view lineView = divider.next(U"\n");
		if (lineView.empty())
		{
			continue;
		}

		parser.parse(lineView);
		if (parser.hasErrors())
		{
			if (_force)
			{
				BWN_ASSERT_WARNING(
					"Error parsing line '{}' from command file '{}' with next errors:\n{}",
					bwn::wrap_utf(lineView),
					_path,
					parser.getErrorsContext().formatErrors(U"\n\t"));
				continue;
			}

			return SystemFacility::createError(Error::ERR_PARSE_ERROR, parser.getErrorsContext().formatErrors(U";")); 
		}

		for (const console::Command& command : parser.getCommandsContext().getCommands())
		{
			const SystemResult argumentsParsingResult = command.formatArguments(parsedArguments);
			if (!argumentsParsingResult.success())
			{
				if (_force)
				{
					BWN_ASSERT_WARNING(
						"Failed to parse arguments in command '{}' from file '{}' with reason: {}",
						bwn::wrap_utf(lineView.substr(command.getStartPos(), command.getEndPos() - command.getStartPos())),
						_path,
						argumentsParsingResult.getErrorDescription());
					continue;
				}
				else
				{
					return argumentsParsingResult;
				}
			}

			const SystemResult callResult = call(command.getName(), parsedArguments);

			if (!callResult.success())
			{
				if (_force)
				{
					BWN_ASSERT_WARNING(
						"Failed to call command '{}' from file '{}' with reason: {}",
						bwn::wrap_utf(lineView.substr(command.getStartPos(), command.getEndPos() - command.getStartPos())),
						_path,
						callResult.getErrorDescription());
					continue;
				}
				else
				{
					return callResult;
				}
			}
		}
	}

	return SystemFacility::createSuccess();
}

bwn::SystemResult bwn::ConsoleDatabase::tryConvertToNode(const Variant& _input, const String& _typeHint, Node*& o_output)
{
	switch (_input.get_type())
	{
		case Variant::Type::NIL:
		{
			o_output = nullptr;
			return SystemFacility::createSuccess();
		}

		case Variant::Type::NODE_PATH:
		case Variant::Type::STRING:
		{
			const String mask = _input.operator String();
			if (mask.is_empty())
			{
				o_output = nullptr;
				return SystemFacility::createSuccess();
			}

			Node*const node = bwn::findSceneNodeByPath(bwn::wrap_view(mask), _typeHint);
			if (node == nullptr)
			{
				return SystemFacility::createError(
					Error::ERR_CANT_RESOLVE,
					bwn::format("Can't find node by mask '{}' in the main scene.", mask));
			}

			o_output = node;
			return SystemFacility::createSuccess();
		}

		case Variant::Type::OBJECT:
		{
			Object*const obj = _input;

			Node*const castedNode = Object::cast_to<Node>(obj);
			if (castedNode == nullptr)
			{
				return SystemFacility::createError(
					Error::ERR_CANT_RESOLVE,
					bwn::format(
						"Argument has type '{}', which is not node.",
						obj->get_class()));
			}

			o_output = castedNode;
			return SystemFacility::createSuccess();
		}

		default:
			break;
	}

	return SystemFacility::createError(
		Error::ERR_PARSE_ERROR,
		bwn::format(
			"Can't convert argument with type {}, into a node.",
			bwn::wrap_utf(bwn::getVariantTypeName(_input.get_type()))));
}

bwn::SystemResult bwn::ConsoleDatabase::tryConvertToResource(
	const Variant& _input,
	const String& _typeHint,
	Ref<Resource>& o_output)
{
	switch (_input.get_type())
	{
		case Variant::Type::NIL:
		{
			o_output.unref();
			return SystemFacility::createSuccess();
		}

		case Variant::Type::STRING:
		{
			const String path = _input.operator String();
			if (path.is_empty())
			{
				o_output.unref();
				return SystemFacility::createSuccess();
			}

			Error loadingError;
			const Ref<Resource> resource = ResourceLoader::load(
				path,
				_typeHint,
				ResourceFormatLoader::CACHE_MODE_REUSE,
				&loadingError);

			if (loadingError != Error::OK)
			{
				return SystemFacility::createError(
					loadingError,
					bwn::format(
						"Failed to load the resource '{}' with error {}.",
						path, bwn::wrap_utf(SystemFacility::stringFromError(loadingError))));
			}

			o_output = resource;
			return SystemFacility::createSuccess();
		}

		default:
			break;
	}

	return SystemFacility::createError(
		Error::ERR_PARSE_ERROR,
		bwn::format(
			"Can't convert argument with type {}, into resource of type {}.",
			bwn::wrap_utf(getVariantTypeName(_input.get_type())),
			_typeHint));
}