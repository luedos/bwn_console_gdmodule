#pragma once

#include "bwn_core/types/singleton.hpp"
#include "bwn_core/types/stringId.hpp"
#include "bwn_core/types/stringView.hpp"
#include "bwn_core/containers/arrayView.hpp"
#include "bwn_core/functional/callback.hpp"

#include "bwn_core/results/systemResult.hpp"

#include <core/string/ustring.h>
#include <core/object/ref_counted.h>
#include <core/templates/vector.h>

#include <vector>
#include <optional>
#include <unordered_map>
#include <string_view>
#include <memory>
#include <type_traits>

class Node;
class Resource;

namespace bwn
{
	class SystemResult;

	class ConsoleDatabase : public ManualSingleton<ConsoleDatabase>
	{
		//
		// Private classes.
		//
	private:
		// This is just a helper class, for some variant conversion edge cases.
		template<typename ArgumentT, typename=void>
		struct ArgumentTraits;

		// Simple interface for the method.
		// Will hold only one method callV,
		// and the rest of the logic will be hidden behind the implementation
		struct MethodHolder;

		// Actual implementation of the parsing for the method call.
		template<typename...ArgsTs>
		struct MethodHolderImpl;

		//
		// Public classes.
		//
	public:
		using MethodId = StringId;
		// A callback which is used by ArgData, to retrieve variants for the argument.
		// Accepts two parameters: current string of an argument, and vector of strings which must be filled (as a return value).
		using VariantsCallback = bwn::callback<void(const bwn::string_view&, std::vector<String>&)>;

		// ArgData and MethodData both done mostly as documentation.

		// Basic info about method argument.
		struct ArgData;

		// Just a simple holder for main method info.
		// This is separate class, so it can be accessed
		// without touching method container itself.
		struct MethodData;

		// This class is mostly used for more flexible customisation
		// of the method post-registration.
		template<typename...ArgsTs>
		class MethodProxy;

		//
		// Private typedefs.
		//
	private:
		using MethodsMap = std::unordered_map<MethodId, std::shared_ptr<MethodHolder>>;
		using MethodsDataArr = std::vector<MethodData>;

		//
		// Construction and destruction.
		//
	public:
		ConsoleDatabase();
		~ConsoleDatabase();

		//
		// Public interface.
		//
	public:
		// Registers callback for the specific method name.
		// Returns special proxy for the method, so
		template<typename...ArgsTs>
		MethodProxy<ArgsTs...> addMethod(const bwn::string_view& _name, bwn::callback<void(ArgsTs...)> _callback);
		// Unregisters callback.
		void removeMethod(const MethodId _nameId);
		// Calls specifc method.
		// Returns true if method was called.
		// If method failed, the error will be written into the "error" arg.
		SystemResult call(const bwn::string_view& _name, const bwn::array_view<const Variant>& _args) const;
		// Returns info for all methods.
		const std::vector<MethodData>& getMethodsData() const;
		// Searches for method with specific name. If not found returns nullptr.
		const MethodData* findMethodData(const bwn::string_view _commandName) const;
		// Loads execution file with console commands, and executes it line by line.
		// If 'force' argument is true, the console database will not stop if some commands fail to execute (in this case system result always 'success').
		// If 'force' argument is false, console database will stop on first command, and return error for it.
		SystemResult executeCommandsFile(const String& _path, const bool _force) const;

		//
		// Private methods.
		//
	private:
		// Just a helper which tries to convert variant to a specific node (treats String or NodePath types as a path to the node on the scene).
		static SystemResult tryConvertToNode(const Variant& _input, const String& _typeHint, Node*& o_output);
		// Just a helper which tries to convert variant to a resource (actually loads the resource if type is String).
		static SystemResult tryConvertToResource(const Variant& _input, const String& _typeHint, Ref<Resource>& o_output);

		//
		// Private members.
		//
	private:
		// Actual method containers, which holds calls and neccecery parse logic.
		MethodsMap m_methods;
		// Just a cache of the method info for easier access.
		MethodsDataArr m_methodsData;
	};

	struct ConsoleDatabase::MethodHolder
	{
		//
		// Construction and destruction.
		//
	public:
		MethodHolder(const String& _name);
		virtual ~MethodHolder();

		//
		// Public interface.
		//
	public:
		// The only method which will be overriden in the children.
		// All the parsing of the arguments, and even holding
		// of iternal callback, will happend inside implementations.
		// If anything went wrong (like parsing, or number of elements, etc)
		// will return false.
		virtual SystemResult callV(const bwn::array_view<const Variant>& _args) = 0;
		// Returns the method name.
		const String& getName() const;

		//
		// Private members.
		//
	private:
		// Name of the method.
		String m_name;
	};

	template<typename...ArgsTs>
	struct ConsoleDatabase::MethodHolderImpl : public ConsoleDatabase::MethodHolder
	{
		//
		// Public usings.
		//
	public:
		// The holder for arguments tuple.
		using ArgumentsTuple = std::tuple<typename std::remove_cv<typename std::remove_reference<ArgsTs>::type>::type...>;
		// The holder for default arguments tuple.
		using DefaultArgumentsTuple = std::tuple<std::optional<typename std::remove_cv<typename std::remove_reference<ArgsTs>::type>::type>...>;
		// Helper typedef, for quick access to type of specifc argument.
		template<uint32_t k_argId>
		using ArgType = typename std::tuple_element<k_argId, ArgumentsTuple>::type;

		//
		// Construction and destruction.
		//
	public:
		MethodHolderImpl(const String& _name, bwn::callback<void(ArgsTs...)> _callback);
		virtual ~MethodHolderImpl() override;

		//
		// Public interface.
		//
	public:
		// The actual parsing and call for the method.
		virtual SystemResult callV(const bwn::array_view<const Variant>& _args) override;
		// Sets the default for the specific argument.
		template<uint32_t k_argId>
		void setDefault(ArgType<k_argId> _value);
		// Sets the name for the specific argument.
		template<uint32_t ArgIdV>
		void setArgName(const String& _name);
		// Returns list of argument names.
		String getArgName(uint32_t _argId) const;

		//
		// Private members.
		//
	private:
		// Actual callback.
		bwn::callback<void(ArgsTs...)> m_callback;
		// Default arguments for the call.
		// Important thing is, that arguments are always mapped from left to right.
		// So, for example, if we have two arguments, first has default but second doesn't,
		// and we passing only one argument, this one argument will still be mapped to first argument,
		// and second will be without anything (aka call error).
		DefaultArgumentsTuple m_defaults;
		// Mostly just used for error generation.
		String m_argNames[sizeof...(ArgsTs)];
	};

	struct ConsoleDatabase::ArgData
	{
		//
		// Public interface.
		//
	public:
		// Sets the name of the argument.
		void setName(const String& _name);
		// Sets default argument value.
		// (This is not a variant, mostly because it doesn't need to,
		// because it will be used primarily like a documantation).
		void setDefault(const String& _value);
		// Sets callback for retrieving variants for the argument.
		void setVariantsCallback(VariantsCallback _callback);
		// Sets the argument description (used as optional thing for simpler console usage).
		void setDescription(const String& _description);

		// Returns a variants for the argument.
		void getVariants(const bwn::string_view& _argumentString, std::vector<String>& o_variants) const;
		// Returns name of the argument.
		const String& getName() const;
		// Returns default value for the argument.
		const String& getDefault() const;
		// Returns the description of the argument.
		const String& getDescription() const;

		// Returns true if variants callback was set.
		bool hasVariantsCallback() const;

		//
		// Private members.
		//
	private:
		// Name of the argument.
		String m_name;
		// The default value for the argument.
		String m_default;
		// The argument description.
		String m_description;
		// Callback for retrieving variants.
		VariantsCallback m_variantsCallback;
	};

	struct ConsoleDatabase::MethodData
	{
		//
		// Construction and destruction.
		//
	public:
		MethodData(const String& _name);

		//
		// Public interface.
		//
	public:
		void resizeArgCount(const size_t _count);
		// Sets info about specific argument.
		void setArgInfo(const ConsoleDatabase::ArgData& _info, const size_t _argId);
		// Sets method name.
		void setName(const String& _name);
		// Sets description of the method.
		void setDescription(const String& _description);
		// Sets signature of the method.
		void setSignature(const String& _signature);
		// Clears all data.
		void clear();

		// Returns info about arguments.
		bwn::array_view<const ConsoleDatabase::ArgData> getArgInfos() const;
		// Returns method name.
		const String& getName() const;
		// Returns description of the method.
		const String& getDescription() const;
		// Returns signature of the method.
		const String& getSignature() const;
		// Returns true if method data is valid (at least has a name).
		bool valid() const;

		//
		// Private members.
		//
	private:
		// Name of the method.
		String m_name;
		// Method signature, just as a addition for the description.
		String m_signature;
		// Method description provided by the register function.
		String m_description;
		// Info about each method argument. We are using godot vector instead of std,
		// because method data will be initialized once, but then copied all around,
		// so it would be better to use shared_ptr nature of godot Vector instead of copying items every time.
		Vector<ConsoleDatabase::ArgData> m_args;
	};

	template<typename...ArgsTs>
	class ConsoleDatabase::MethodProxy
	{
		// because we need to call constructors somehow.
		friend ConsoleDatabase;

		//
		// Public typedefs.
		//
	public:
		template<uint32_t ArgIdV>
		using ArgType = typename ConsoleDatabase::MethodHolderImpl<ArgsTs...>::template ArgType<ArgIdV>;

		//
		// Construction and destruction.
		//
	private:
		MethodProxy() = default;
		MethodProxy(std::shared_ptr<MethodHolderImpl<ArgsTs...>> _method);
		// Every copy/move construction will be deleted,
		// because we don't really want the caller to store this class.
		MethodProxy(const MethodProxy&) = delete;
		MethodProxy(MethodProxy&&) = delete;
		MethodProxy& operator=(const MethodProxy&) = delete;
		MethodProxy& operator=(MethodProxy&&) = delete;
	public:
		// In the destructor we will generate the description for the method, based on all seted info.
		~MethodProxy();

		//
		// Public interface.
		//
	public:
		// Sets the name for the argument by index ArgIdV (this done mostly just for a description).
		// Returns lvalue reference to itself, so those calls can be chained.
		template<uint32_t ArgIdV>
		MethodProxy& argName(const String& _name);
		// Sets the default value for the argument by index ArgIdV.
		// Returns lvalue reference to itself, so those calls can be chained.
		template<uint32_t ArgIdV>
		MethodProxy& argDefault(ArgType<ArgIdV> _default);
		// Sets the description for the argument by index ArgIdV.
		// Returns lvalue reference to itself, so those calls can be chained.
		template<uint32_t ArgIdV>
		MethodProxy& argDescription(const String& _description);
		// Sets the callback which will retrieve possible variants for the argument by index ArgIdV.
		// Returns lvalue reference to itself, so those calls can be chained.
		template<uint32_t ArgIdV>
		MethodProxy& argVariants(VariantsCallback _callback);
		// Sets the description of the method.
		MethodProxy& setDescription(String _description);
		// Returns true if proxy is valid (aka method was created successfully).
		bool isValid() const noexcept;

		//
		// Private methods.
		//
	private:
		// Generates signature of the method based on a arg names/types and name of method itself.
		String generateSignature() const;

		//
		// Private members.
		//
	private:
		// Actual method implementation, this proxy is works on.
		std::shared_ptr<MethodHolderImpl<ArgsTs...>> m_methodImpl;
		// This will be used to generate description, and later for the ArgData.
		ConsoleDatabase::ArgData m_argInfos[sizeof...(ArgsTs)];
		// The description of the method, which will be applied in the destructor.
		String m_description;
	};
} // namespace bwn

#include "bwn_console/consoleDatabase.inl"