#pragma once

// Just in case
#include "bwn_console/consoleDatabase.hpp"
#include "bwn_console/consoleLineFilter.hpp"

#include "bwn_core/utility/formatUtility.hpp"
#include "bwn_core/utility/variantUtility.hpp"
#include "bwn_core/utility/engineUtility.hpp"
#include "bwn_core/functional/callback.hpp"

#include "bwn_core/results/systemFacility.hpp"

template<typename ArgumentT, typename>
struct bwn::ConsoleDatabase::ArgumentTraits
{
	static SystemResult tryConvert(const Variant& _input, ArgumentT& o_output)
	{
		static constexpr Variant::Type k_resultType = DecayedVariantTraits<ArgumentT>::k_type;
		if (!Variant::can_convert(_input.get_type(), k_resultType))
		{
			return SystemFacility::createError(
				Error::ERR_PARSE_ERROR,
				bwn::format(
					"Can't convert argument with type {}, into type {}.",
					bwn::wrap_utf(getVariantTypeName(_input.get_type())),
					bwn::wrap_utf(getVariantTypeName(k_resultType))));
		}
		// By default, we will let variant convert.
		o_output = _input;
		return SystemFacility::createSuccess();
	}

	static VariantsCallback getDefaultVariantsCallback()
	{
		return VariantsCallback();
	}
};

template<typename NodeT>
struct bwn::ConsoleDatabase::ArgumentTraits<
	NodeT*,
	typename std::enable_if<
		std::is_base_of< Node, NodeT>::value
	>::type >
{
	static SystemResult tryConvert(const Variant& _input, NodeT*& o_output)
	{
		Node* node;
		const bwn::SystemResult result = ConsoleDatabase::tryConvertToNode(_input, NodeT::get_class_static(), node);
		if (!result.success())
		{
			return result;
		}

		if (node == nullptr)
		{
			o_output = nullptr;
			return SystemFacility::createSuccess();
		}

		NodeT*const castedNode = Object::cast_to<NodeT>(node);
		if (castedNode == nullptr)
		{
			if (Variant::can_convert(_input.get_type(), Variant::Type::STRING))
			{
				const String mask = _input.operator String();
				return SystemFacility::createError(
					Error::ERR_CANT_RESOLVE,
					bwn::format(
						"Node by name '{}' (which was found by mask '{}' in the main scene) has invalid type. Node type '{}', but required is '{}'.",
						node->get_name(),
						mask,
						node->get_class(),
						NodeT::get_class_static()));
			}
			else
			{
				return SystemFacility::createError(
					Error::ERR_CANT_RESOLVE,
					bwn::format(
						"Can't convert node of type '{}' into required type '{}'.",
						node->get_class(),
						NodeT::get_class_static()));
			}
		}

		o_output = castedNode;
		return SystemFacility::createSuccess();
	}

	static VariantsCallback getDefaultVariantsCallback()
	{
		return VariantsCallback(
			[](const bwn::string_view& _argument, std::vector<String>& _variants)
			{
				bwn::pushMatchedNodes(_argument, NodeT::get_class_static(), _variants);
			});
	}
};

template<typename ResourceT>
struct bwn::ConsoleDatabase::ArgumentTraits<
	Ref<ResourceT>,
	typename std::enable_if<
		std::is_base_of< Resource, ResourceT>::value
	>::type >
{
	static SystemResult tryConvert(const Variant& _input, Ref<ResourceT>& o_output)
	{
		Ref<Resource> resource;
		const String typeHint = ResourceT::get_class_static();
		const bwn::SystemResult result = ConsoleDatabase::tryConvertToResource(_input, typeHint, resource);
		if (!result.success())
		{
			return result;
		}

		Ref<ResourceT> castedResource = Object::cast_to<ResourceT>(resource.ptr());
		if (castedResource.is_null())
		{
			const String path = _input.operator String();
			return SystemFacility::createError(
				Error::ERR_BUG,
				bwn::format("Failed to load the resource '{}' with correct type '{}'.", path, typeHint));
		}

		o_output = castedResource;
		return SystemFacility::createSuccess();
	}

	static VariantsCallback getDefaultVariantsCallback()
	{
		return VariantsCallback();
	}
};

template<typename...ArgsTs>
bwn::ConsoleDatabase::MethodHolderImpl<ArgsTs...>::MethodHolderImpl(const String& _name, bwn::callback<void(ArgsTs...)> _callback)
	: MethodHolder(_name)
	, m_callback(std::move(_callback))
{}

template<typename...ArgsTs>
bwn::ConsoleDatabase::MethodHolderImpl<ArgsTs...>::~MethodHolderImpl() = default;

template<typename...ArgsTs>
bwn::SystemResult bwn::ConsoleDatabase::MethodHolderImpl<ArgsTs...>::callV(const bwn::array_view<const Variant>& _args)
{
	BWN_ASSERT_WARNING_COND(m_callback, "No callback was provided for the console method.");
	if (!m_callback)
	{
		return bwn::SystemFacility::createError(
			Error::ERR_UNCONFIGURED,
			bwn::format("The callback for method '{}' was not provided.", getName()));
	}

	const std::size_t argsCount = _args.size();
	if (argsCount > sizeof...(ArgsTs))
	{
		return bwn::SystemFacility::createError(
			Error::ERR_INVALID_PARAMETER,
			bwn::format("To the method '{}' was provided {} additional parameters. ", getName(), argsCount - sizeof...(ArgsTs)));
	}

	// Special class to invoke tuples members one by one.
	using Foreach = bwn::TupleForeach<std::make_index_sequence<sizeof...(ArgsTs)>>;

	// Checking that even if don't have enough arguments, we at least have the defaults.
	{
		std::size_t currentId = 0;
		SystemResult error = SystemFacility::createSuccess();

		Foreach::call([argsCount, this, &currentId, &error](auto&& defArg) -> bool
		{
			if (currentId >= argsCount && !defArg.has_value())
			{
				// Checking that we have a default for not provided argument.
				const bool argHasName = !this->m_argNames[currentId].is_empty();
				error = SystemFacility::createError(
					Error::ERR_INVALID_PARAMETER,
					bwn::format(
						"Invalid argument #{}{}{}. The argument is not provided, and doesn't have default value.",
						currentId + 1,
						(argHasName ? ", with name " : ""),
						bwn::wrap_utf(argHasName ? m_argNames[currentId].get_data() : U"")));
				return false;
			}

			++currentId;
			return true;
		},
		m_defaults);

		if (!error.success())
		{
			return error;
		}
	}

	ArgumentsTuple callArgs;

	{
		std::size_t currentId = 0;
		SystemResult error = SystemFacility::createSuccess();
		// assigning arguments.
		Foreach::call([argsCount, _args, this, &currentId, &error](auto& callArg, auto&& defArg) ->bool
		{
			using ArgumentType = typename std::remove_cv<typename std::remove_reference<decltype(callArg)>::type>::type;
			using Converter = ArgumentTraits<ArgumentType>;

			if (currentId < argsCount)
			{
				const bool argHasName = !this->m_argNames[currentId].is_empty();
				// Try to get convert from input argument.
				const SystemResult result = Converter::tryConvert(_args[currentId], callArg);
				if (!result.success())
				{
					error = SystemFacility::createError(
						result.getError(),
						bwn::format(
							"Failed to convert argument #{}{}{}, with next error: {}",
							currentId + 1,
							(argHasName ? ", with name " : ""),
							bwn::wrap_utf(argHasName ? m_argNames[currentId].get_data() : U""),
							result.getErrorDescription()));

					return false;
				}
			}
			else
			{
				// If input arguments ran out, get the default.
				callArg = *defArg;
			}
			
			++currentId;
			return true;
		},
		callArgs,
		m_defaults);

		if (!error.success())
		{
			return error;
		}
	}

	// Finally calling the method.
	std::apply(m_callback, callArgs);
	return bwn::SystemFacility::createSuccess();
}

template<typename...ArgsTs>
template<uint32_t ArgIdV>
void bwn::ConsoleDatabase::MethodHolderImpl<ArgsTs...>::setDefault(ArgType<ArgIdV> _value)
{
	static_assert(ArgIdV < sizeof...(ArgsTs), "Incorrect index for method argument.");
	std::get<ArgIdV>(m_defaults) = std::move(_value);
}

template<typename...ArgsTs>
template<uint32_t ArgIdV>
void bwn::ConsoleDatabase::MethodHolderImpl<ArgsTs...>::setArgName(const String& _name)
{
	static_assert(ArgIdV < sizeof...(ArgsTs), "Incorrect index for method argument.");
	m_argNames[ArgIdV] = std::move(_name);
}

template<typename...ArgsTs>
String bwn::ConsoleDatabase::MethodHolderImpl<ArgsTs...>::getArgName(uint32_t _argId) const
{
	BWN_ASSERT_WARNING_COND(_argId < sizeof...(ArgsTs), "Incorrect index {} for method argument (args count: {}).", _argId, sizeof...(ArgsTs));
	if (_argId < sizeof...(ArgsTs))
	{
		return m_argNames[_argId];
	}

	return String();
}

template<typename...ArgsTs>
bwn::ConsoleDatabase::MethodProxy<ArgsTs...>::MethodProxy(std::shared_ptr<ConsoleDatabase::MethodHolderImpl<ArgsTs...>> _method)
	: m_methodImpl(std::move(_method))
{
	// Initializing the default variants callbacks.
	size_t argumentIndex = 0;
	const auto setDefaultVariantsCallback =
		[&argumentIndex, this](const VariantsCallback _variantsCallback)
		{
			this->m_argInfos[argumentIndex].setVariantsCallback(_variantsCallback);
			++argumentIndex;
		};
	(setDefaultVariantsCallback(ArgumentTraits<ArgsTs>::getDefaultVariantsCallback()), ...);
}

template<typename...ArgsTs>
bwn::ConsoleDatabase::MethodProxy<ArgsTs...>::~MethodProxy()
{
	if (!isValid())
	{
		return;
	}

	ConsoleDatabase::MethodData* methodInfo = nullptr;
	auto consoleDB = ConsoleDatabase::getInstance();

	const String methodName = m_methodImpl->getName();

	for (ConsoleDatabase::MethodData& info : consoleDB->m_methodsData)
	{
		if (info.getName() == methodName)
		{
			methodInfo = &info;
			break;
		}
	}

	BWN_ASSERT_WARNING_COND(methodInfo != nullptr, "Can't find method info for console method {}.", methodName);
	if (methodInfo == nullptr) {
		return;
	}

	for (int argId = int(sizeof...(ArgsTs)) - 1; argId >= 0; --argId) {
		methodInfo->setArgInfo(std::move(this->m_argInfos[argId]), static_cast<std::size_t>(argId));
	}

	methodInfo->setName(std::move(methodName));
	methodInfo->setDescription(std::move(m_description));
	methodInfo->setSignature(std::move(generateSignature()));
}

template<typename...ArgsTs>
template<uint32_t ArgIdV>
bwn::ConsoleDatabase::MethodProxy<ArgsTs...>& bwn::ConsoleDatabase::MethodProxy<ArgsTs...>::argName(const String& _name)
{
	static_assert(ArgIdV < sizeof...(ArgsTs), "Invalid argument index provided.");
	if (isValid())
	{
		this->m_argInfos[ArgIdV].setName(_name);
		this->m_methodImpl->template setArgName<ArgIdV>(_name);
	}

	return *this;
}

template<typename...ArgsTs>
template<uint32_t ArgIdV>
bwn::ConsoleDatabase::MethodProxy<ArgsTs...>& bwn::ConsoleDatabase::MethodProxy<ArgsTs...>::argDefault(ArgType<ArgIdV> _value)
{
	static_assert(ArgIdV < sizeof...(ArgsTs), "Invalid argument index provided.");
	if (isValid())
	{
		this->m_argInfos[ArgIdV].setDefault(bwn::format(L"{}", _value));
		this->m_methodImpl->template setDefault<ArgIdV>(std::move(_value));
	}

	return *this;
}

template<typename...ArgsTs>
template<uint32_t ArgIdV>
bwn::ConsoleDatabase::MethodProxy<ArgsTs...>& bwn::ConsoleDatabase::MethodProxy<ArgsTs...>::argDescription(const String &_description)
{
	static_assert(ArgIdV < sizeof...(ArgsTs), "Invalid argument index provided.");
	if (isValid())
	{
		this->m_argInfos[ArgIdV].setDescription(_description);
	}

	return *this;
}

template<typename...ArgsTs>
template<uint32_t ArgIdV>
bwn::ConsoleDatabase::MethodProxy<ArgsTs...>& bwn::ConsoleDatabase::MethodProxy<ArgsTs...>::argVariants(VariantsCallback _callback)
{
	static_assert(ArgIdV < sizeof...(ArgsTs), "Invalid argument index provided.");
	if (isValid()) 
	{
		this->m_argInfos[ArgIdV].setVariantsCallback(std::move(_callback));
	}
	
	return *this;
}

template<typename...ArgsTs>
bwn::ConsoleDatabase::MethodProxy<ArgsTs...>& bwn::ConsoleDatabase::MethodProxy<ArgsTs...>::setDescription(String _description)
{
	if (isValid()) {
		this->m_description = std::move(_description);
	}

	return *this;
}

template<typename...ArgsTs>
bool bwn::ConsoleDatabase::MethodProxy<ArgsTs...>::isValid() const noexcept
{
	return this->m_methodImpl != nullptr;
}

template<typename...ArgsTs>
String bwn::ConsoleDatabase::MethodProxy<ArgsTs...>::generateSignature() const
{
	if (!isValid())
	{
		return String();
	}

	// We are basically accumulating and formatting string as wstring, and after that copying buffer into String.
	std::u32string description = this->m_methodImpl->getName().get_data();

	constexpr Variant::Type types[] = { DecayedVariantTraits<ArgsTs>::k_type... };
	for (std::size_t i = 0; i << sizeof...(ArgsTs); ++i)
	{
		const ConsoleDatabase::ArgData& arg = this->m_argInfos[i];
		const bool hasDefault = !arg.getDefault().is_empty();
		description += fmt::format(
			U" {} [{}{}{}]",
			arg.getName(),
			bwn::wrap_utf(bwn::getVariantTypeName(types[i])),
			bwn::wrap_utf(hasDefault ? ":" : ""),
			arg.getDefault().get_data()
		);
	}

	return description.c_str();
}


template<typename...ArgsTs>
bwn::ConsoleDatabase::MethodProxy<ArgsTs...> bwn::ConsoleDatabase::addMethod(
	const std::basic_string_view<char32_t>& _name, 
	bwn::callback<void(ArgsTs...)> _callback)
{
	const MethodId id = MethodId(_name);
	const MethodsMap::const_iterator it = m_methods.find(id);
	BWN_ASSERT_WARNING_COND(it == m_methods.end(), "Can't add console method \"{}\" which already exist.", bwn::wrap_utf(_name));
	if (it != m_methods.end()) {
		return {};
	}

	const String methodName = String(_name.data(), _name.size());

	std::shared_ptr<MethodHolderImpl<ArgsTs...>> implPtr = std::make_shared<MethodHolderImpl<ArgsTs...>>(methodName, std::move(_callback));
	// Copying name and ptr here, because we will move them later anyway.
	m_methods.emplace(std::move(id), implPtr);
	m_methodsData.emplace_back(std::move(methodName));

	return MethodProxy<ArgsTs...>(std::move(implPtr));
}