#include "precompiled_console.hpp"

#include "bwn_console/consoleLineFilter.hpp"

bool bwn::genericConsoleMatch(const bwn::string_view& _filterString, const bwn::string_view& _testString)
{
	// TODO: create more sophisticated algorithm of filtering strings..

	// if we found string inside another string this is a match, with one exception:
	// this must not be exact string. So if we found string in another string,
	// but they have the same size (so this is basically the same strings),
	// this is not match.
	const bool hasInnerString = std::search(
		_testString.begin(), 
		_testString.end(), 
		_filterString.begin(), 
		_filterString.end(), 
		[](const wchar_t _left, const wchar_t _right) -> bool
		{
			return std::towupper(_left) == std::towupper(_right);
		}) != _testString.end();

	return hasInnerString && _filterString.length() != _testString.length();
}

bool bwn::pushIfConsoleMatch(const bwn::string_view& _filterString, const char32_t*const _testString, std::vector<String>& _variants)
{
	return bwn::pushIfConsoleMatch(_filterString, String(_testString), _variants);
}

bool bwn::pushIfConsoleMatch(const bwn::string_view& _filterString, const bwn::string_view& _testString, std::vector<String>& _variants)
{
	return bwn::pushIfConsoleMatch(_filterString, String(_testString.data(), _testString.length()), _variants);
}

bool bwn::pushIfConsoleMatch(const bwn::string_view& _filterString, const String& _testString, std::vector<String>& _variants)
{
	const bool matched = bwn::genericConsoleMatch(_filterString, bwn::wrap_view(_testString));
	if (matched)
	{
		_variants.push_back(_testString);
	}

	return matched;
}

namespace
{
	void insertStringWithSlash(std::basic_string<char32_t>& _buffer, const bwn::string_view& _string)
	{
		const size_t additionalLength = _string.length() + 1;
		const size_t oldSize = _buffer.length();
		_buffer.resize(oldSize + additionalLength);

		// Moving string to the end
		{
			const char32_t*const beginIt = _buffer.data();
			const char32_t* inputIt = beginIt + oldSize - 1;
			char32_t* outputIt = const_cast<char32_t*>(inputIt) + additionalLength;

	        while (inputIt >= beginIt)
	        {
				*outputIt = *inputIt;
				--outputIt;
				--inputIt;
	        }
		}

		_buffer[0] = '/';
		std::memcpy(_buffer.data() + 1, _string.data(), _string.length() * sizeof(char32_t));
	}

	String composeRootNodePath(const Node& _node, const Node& _rootNode)
	{
		std::basic_string<char32_t> buffer;
		const Node* currentNode = &_node;

		while (currentNode != &_rootNode)
		{
			const String nodeName = currentNode->get_name();
			insertStringWithSlash(buffer, bwn::wrap_view(nodeName));
			currentNode = currentNode->get_parent();
		}

		// Root is never pushed, so we need to push it separately.
		const String rootName = _rootNode.get_name();
		insertStringWithSlash(buffer, bwn::wrap_view(rootName));

		return String( buffer.data(), buffer.length() );
	}

	bool checkNodeType(const Node& _node, const String& _type)
	{
		return _type.is_empty() || _type == U"Node" || _node.is_class(_type);
	}
} // namespace

void bwn::pushMatchedNodes(const bwn::string_view& _nodeMask, std::vector<String>& _variants)
{
	if (_nodeMask.empty())
	{
		return;
	}

	const SceneTree*const sceneTree = SceneTree::get_singleton();
	const Node*const rootNode = sceneTree->get_current_scene();
	if (rootNode == nullptr)
	{
		return;
	}

	const bool isRootPath = _nodeMask.front() == '\\' || _nodeMask.front() == '/';
	const bool isExactPath = _nodeMask.size() > size_t(isRootPath) && (_nodeMask.back() == '\\' || _nodeMask.back() == '/');
	const bwn::string_view relativePath = _nodeMask.substr(size_t(isRootPath), _nodeMask.length() - size_t(isRootPath) - size_t(isExactPath));

	const std::pair<bwn::string_view, bwn::string_view> splitPath = bwn::splitPathOnEnd(relativePath);
	const bwn::string_view& restPath = splitPath.first;
	const bwn::string_view& firstName = splitPath.second;

	if (firstName.empty())
	{
		return;
	}

	const bool onlyOneNode = restPath.empty() && !isExactPath;
	if (isRootPath && onlyOneNode)
	{
		const String rootName = rootNode->get_name();
		const bwn::string_view rootNameView = bwn::wrap_view(rootName);
		if (rootNameView.find(firstName) != bwn::string_view::npos)
		{
			_variants.push_back(bwn::concat(bwn::string_view(U"/"), rootNameView));
		}

		// If this is root path, and only one node, where is no need to search further.
		return;
	}
	// Depth should be one less then actual node count, and nodes count is one more then amount of slashes.
	const size_t nodesDepth = std::count_if(
		relativePath.begin(),
		relativePath.end(),
		[](const char32_t _character){ return _character == '\\' || _character == '/'; });

	bwn::visitTree(
		*rootNode,
		nodesDepth,
		isRootPath ? nodesDepth : std::numeric_limits<size_t>::max(),
		[&_variants, rootNode, &relativePath, isExactPath](const Node& _node)
		{
			const bool valid = isExactPath
				? bwn::checkNodeHasExactPath(relativePath, _node, rootNode->get_parent())
				: bwn::checkNodeHasPartialPath(relativePath, _node, rootNode->get_parent());

			if (valid)
			{
				_variants.push_back(::composeRootNodePath(_node, *rootNode));
			}
		});
}

void bwn::pushMatchedNodes(const bwn::string_view& _nodeMask, const String& _type, std::vector<String>& _variants)
{
	if (_nodeMask.empty())
	{
		return;
	}

	const SceneTree*const sceneTree = SceneTree::get_singleton();
	const Node*const rootNode = sceneTree->get_current_scene();
	if (rootNode == nullptr)
	{
		return;
	}

	const bool isRootPath = _nodeMask.front() == '\\' || _nodeMask.front() == '/';
	const bool isExactPath = _nodeMask.size() > size_t(isRootPath) && (_nodeMask.back() == '\\' || _nodeMask.back() == '/');
	const bwn::string_view relativePath = _nodeMask.substr(size_t(isRootPath), _nodeMask.length() - size_t(isRootPath) - size_t(isExactPath));

	const std::pair<bwn::string_view, bwn::string_view> splitPath = bwn::splitPathOnEnd(relativePath);
	const bwn::string_view& restPath = splitPath.first;
	const bwn::string_view& firstName = splitPath.second;

	if (firstName.empty())
	{
		return;
	}

	const bool onlyOneNode = restPath.empty() && !isExactPath;
	if (isRootPath && onlyOneNode)
	{
		if (!::checkNodeType(*rootNode, _type))
		{
			return;
		}

		const String rootName = rootNode->get_name();
		const bwn::string_view rootNameView = bwn::wrap_view(rootName);
		if (rootNameView.find(firstName) != bwn::string_view::npos)
		{
			_variants.push_back(bwn::concat(bwn::string_view(U"/"), rootNameView));
		}

		// If this is root path, and only one node, where is no need to search further.
		return;
	}

	// Depth should be one less then actual node count, and nodes count is one more then amount of slashes.
	const size_t nodesDepth = std::count_if(
		relativePath.begin(),
		relativePath.end(),
		[](const char32_t _character){ return _character == '\\' || _character == '/'; });

	bwn::visitTree(
		*rootNode,
		nodesDepth,
		isRootPath ? nodesDepth : std::numeric_limits<size_t>::max(),
		[&_variants, &_type, rootNode, &relativePath, isExactPath](const Node& _node)
		{
			if (!::checkNodeType(_node, _type))
			{
				return;
			}

			const bool valid = isExactPath
				? bwn::checkNodeHasExactPath(relativePath, _node, rootNode->get_parent())
				: bwn::checkNodeHasPartialPath(relativePath, _node, rootNode->get_parent());

			if (valid)
			{
				_variants.push_back(::composeRootNodePath(_node, *rootNode));
			}
		});
}
