#pragma once

#include "bwn_core/types/stringView.hpp"

#include <core/string/ustring.h>

#include <vector>

namespace bwn
{
	// Checks if filter string is matched in terms of specific console argument.
	bool genericConsoleMatch(const bwn::string_view& _filterString, const bwn::string_view& _testString);
	// Checks argument againts filter string, and if it matches with genericConsoleMatch pushes it to the _variants vector. Returns true if argument was pushed.
	bool pushIfConsoleMatch(const bwn::string_view& _filterString, const char32_t*const _testString, std::vector<String>& _variants);
	bool pushIfConsoleMatch(const bwn::string_view& _filterString, const bwn::string_view& _testString, std::vector<String>& _variants);
	bool pushIfConsoleMatch(const bwn::string_view& _filterString, const String& _testString, std::vector<String>& _variants);

	void pushMatchedNodes(const bwn::string_view& _nodeMask, std::vector<String>& _variants);
	void pushMatchedNodes(const bwn::string_view& _nodeMask, const String& _type, std::vector<String>& _variants);
} // namespace bwn