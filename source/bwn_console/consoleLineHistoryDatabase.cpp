#include "precompiled_console.hpp"

#include "bwn_console/consoleLineHistoryDatabase.hpp"
#include "bwn_console/consoleDatabase.hpp"

#include <core/io/file_access.h>

bwn::ConsoleLineHistoryDatabase::ConsoleLineHistoryDatabase()
	: m_databasePath( "user://console.txt" )
{
	ConsoleDatabase::getInstance()->addMethod(
		U"change_console_history_database_path",
		bwn::wrap_callback([this](const String& _path)
		{
			this->setDatabasePath(_path);
			this->loadDatabase();
		}));
	ConsoleDatabase::getInstance()->addMethod(U"dump_console_history", bwn::wrap_callback(this, &ConsoleLineHistoryDatabase::dumpDatabase));
}

bwn::ConsoleLineHistoryDatabase::~ConsoleLineHistoryDatabase()
{
	ConsoleDatabase::getInstance()->removeMethod(BWN_STRING_ID("change_console_history_database_path"));
	ConsoleDatabase::getInstance()->removeMethod(BWN_STRING_ID("dump_console_history"));
}

const std::vector<String>& bwn::ConsoleLineHistoryDatabase::getLines() const
{
	return m_consoleLines;
}

void bwn::ConsoleLineHistoryDatabase::setLines(const bwn::array_view<const String>& _lines)
{
	m_consoleLines.clear();
	m_consoleLines.reserve(_lines.size());
	for (const String& consoleLine : _lines)
	{
		m_consoleLines.push_back(consoleLine);
	}
}

void bwn::ConsoleLineHistoryDatabase::dumpDatabase() const
{
	if (m_databasePath.is_empty())
	{
		return;
	}

	Error openError;
	Ref<FileAccess> file = FileAccess::open(m_databasePath, FileAccess::WRITE, &openError);
	BWN_ASSERT_WARNING_COND(openError == Error::OK, "Failed to dump console database to a file '{}'.", m_databasePath);
	if (openError != Error::OK)
	{
		return;
	}

	String previousLine = String();
	size_t lineCount = 0;
	static constexpr size_t k_maxConsoleLines = 100;
	for (std::vector<String>::const_reverse_iterator lineIt = m_consoleLines.crbegin(); lineIt != m_consoleLines.crend(); ++lineIt)
	{
		if (lineCount > k_maxConsoleLines)
		{
			// No more than 100 lines
			break;
		}

		if (lineIt->is_empty() || *lineIt == previousLine)
		{
			continue;
		}

		file->store_line(*lineIt);
		previousLine = *lineIt;
		++lineCount;
	}
}

void bwn::ConsoleLineHistoryDatabase::loadDatabase()
{
	m_consoleLines.clear();

	if (!FileAccess::exists(m_databasePath))
	{
		return;
	}

	Error openError;
	const String fileContent = FileAccess::get_file_as_string(m_databasePath, &openError);
	BWN_ASSERT_WARNING_COND(openError == Error::OK, "Failed to open console database file '{}', with error {}.", SystemFacility::stringFromError(openError));

	if (openError != Error::OK || fileContent.is_empty())
	{
		return;
	}

	const auto iterateNewLine = [&fileContent](const int previousNewLineIndex) -> int
	{
		int charIndex = previousNewLineIndex;
		const int contentSize = fileContent.size();
		for (; charIndex < contentSize && fileContent[charIndex] != L'\n'; ++charIndex)
		{}

		return charIndex;
	};

	int lineStartIndex = 0;
	int lineEndIndex = iterateNewLine(lineStartIndex);

	const int contentSize = fileContent.size();
	while (lineStartIndex < contentSize)
	{
		int lineSize = lineEndIndex - lineStartIndex;
		if (lineSize > 0 && fileContent[lineEndIndex - 1] == L'\r')
		{
			--lineSize;
		}

		if (lineSize != 0)
		{
			m_consoleLines.push_back(fileContent.substr(lineStartIndex, lineSize));
		}

		lineStartIndex = lineEndIndex + 1;
		lineEndIndex = iterateNewLine(lineStartIndex);
	}

	std::reverse(m_consoleLines.begin(), m_consoleLines.end());
}

void bwn::ConsoleLineHistoryDatabase::setDatabasePath(const String& _path)
{
	if (_path.is_empty())
	{
		m_databasePath.clear();
		return;
	}

	if (_path.find("user://") != 0)
	{
		m_databasePath = String("user://") + _path;
	}
	else
	{
		m_databasePath = _path;
	}
}
