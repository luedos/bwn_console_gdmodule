#pragma once

#include "bwn_core/types/singleton.hpp"
#include "bwn_core/containers/arrayView.hpp"

#include <core/string/ustring.h>

#include <vector>

namespace bwn
{
	class ConsoleLineHistoryDatabase : public ManualSingleton<ConsoleLineHistoryDatabase>
	{
		//
		// Construction and destruction.
		//
	public:
		ConsoleLineHistoryDatabase();
		~ConsoleLineHistoryDatabase();

		//
		// Public interface.
		//
	public:
		const std::vector<String>& getLines() const;
		void setLines(const bwn::array_view<const String>& _lines);
		void dumpDatabase() const;
		void loadDatabase();
		void setDatabasePath(const String& _path);

		//
		// Private members.
		//
	private:
		std::vector<String> m_consoleLines;
		String m_databasePath;
	};
} // namespace bwn
