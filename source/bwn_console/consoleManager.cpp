#include "precompiled_console.hpp"

#include "bwn_console/consoleManager.hpp"
#include "bwn_console/controls/consoleRoot.hpp"

bwn::ConsoleManager::ConsoleManager() = default;
bwn::ConsoleManager::~ConsoleManager() = default;

void bwn::ConsoleManager::_bind_methods()
{
	bwn::bindMethod(D_METHOD("show_console"), &ConsoleManager::showConsole);
	bwn::bindMethod(D_METHOD("hide_console"), &ConsoleManager::hideConsole);
	bwn::bindMethod(D_METHOD("is_console_visible"), &ConsoleManager::isConsoleVisible);
	bwn::bindMethod(D_METHOD("set_console_visible", "is_visible"), &ConsoleManager::setConsoleVisible);
	bwn::bindMethod(D_METHOD("switch_console_visibility"), &ConsoleManager::switchConsoleVisibility);

	bwn::bindMethod(D_METHOD("_input"), &ConsoleManager::input);
}

void bwn::ConsoleManager::_notification(const int _notification)
{
	switch (_notification)
	{
		case NOTIFICATION_POSTINITIALIZE:
		{
			set_process_input(true);
		}
		break;

		case NOTIFICATION_ENTER_TREE:
		{
			initSingleton(this);
		}
		break;

		case NOTIFICATION_EXIT_TREE:
		{
			if (isSingletonValid() && getInstance() == this)
			{
				clearSingleton();
			}
		}
		break;
	}
}

void bwn::ConsoleManager::input(const Ref<InputEvent>& _event)
{
	const Ref<InputEventKey> keyEvent = _event;

	if (!keyEvent.is_valid() || !keyEvent->is_pressed() || keyEvent->is_meta_pressed())
	{
		return;
	}

	const Key keycode = keyEvent->get_keycode();
	if (keycode != Key::QUOTELEFT)
	{
		return;
	}

	const auto consoleRoot = ConsoleRoot::getInstanceUnsafe();
	if (!consoleRoot)
	{
		return;
	}

	if (consoleRoot->is_visible())
	{
		consoleRoot->hideConsole();
	}
	else
	{
		consoleRoot->showConsole();
	}

	get_viewport()->set_input_as_handled();
}

void bwn::ConsoleManager::showConsole() const
{
	if (const auto consoleRoot = ConsoleRoot::getInstanceUnsafe())
	{
		consoleRoot->showConsole();
	}
}

void bwn::ConsoleManager::hideConsole() const
{
	if (const auto consoleRoot = ConsoleRoot::getInstanceUnsafe())
	{
		consoleRoot->hideConsole();
	}
}

bool bwn::ConsoleManager::isConsoleVisible() const
{
	if (const auto consoleRoot = ConsoleRoot::getInstanceUnsafe())
	{
		return consoleRoot->is_visible();
	}

	return false;
}

void bwn::ConsoleManager::setConsoleVisible(const bool _visible) const
{
	if (const auto consoleRoot = ConsoleRoot::getInstanceUnsafe())
	{
		if (_visible)
		{
			consoleRoot->showConsole();
		}
		else
		{
			consoleRoot->hideConsole();
		}
	}
}

void bwn::ConsoleManager::switchConsoleVisibility() const
{
	if (const auto consoleRoot = ConsoleRoot::getInstanceUnsafe())
	{
		if (!consoleRoot->is_visible())
		{
			consoleRoot->showConsole();
		}
		else
		{
			consoleRoot->hideConsole();
		}
	}
}