#pragma once

#include "bwn_core/types/singleton.hpp"

#include <core/object/ref_counted.h>
#include <scene/main/node.h>

namespace bwn
{
	class ConsoleManager : public Node, public PlainSingleton<ConsoleManager>
	{
		GDCLASS(ConsoleManager, Node)

		//
		// Construction and destruction.
		//
	public:
		ConsoleManager();
		virtual ~ConsoleManager() override;

		//
		// Godot methods.
		//
	private:
		static void _bind_methods();
		void _notification(const int _notification);
		void input(const Ref<InputEvent>& _event) override;

		//
		// Public methods.
		//
	public:
		// Shows current console.
		void showConsole() const;
		// Hides current console.
		void hideConsole() const;
		// Returns true if console exist and it's active.
		bool isConsoleVisible() const;
		// Directly sets the visibility of a console.
		void setConsoleVisible(const bool _visible) const;
		// Switches the console visibility.
		void switchConsoleVisibility() const;
	};
} // namespace bwn