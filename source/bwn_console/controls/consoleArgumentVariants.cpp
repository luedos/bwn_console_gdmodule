#include "precompiled_console.hpp"

#include "bwn_console/controls/consoleArgumentVariants.hpp"
#include "bwn_console/controls/consoleItemSelector.hpp"
#include "bwn_console/controls/consoleLineEdit.hpp"

bwn::ConsoleArgumentVariants::ConsoleArgumentVariants()
	: m_itemSelector( BWN_ONLY_IN_DEBUG("selector label") )
	, m_parentLineEdit( nullptr )
	, m_inserter()
	, m_variants()
{}

bwn::ConsoleArgumentVariants::~ConsoleArgumentVariants() = default;

void bwn::ConsoleArgumentVariants::_bind_methods()
{
	BWN_BIND_PROPERTY(
		"item_selector",
		&ConsoleArgumentVariants::setItemSelectorPath,
		&ConsoleArgumentVariants::getItemSelectorPath,
		PROPERTY_HINT_NODE_PATH_VALID_TYPES,
		ConsoleItemSelector::get_class_static());
}

void bwn::ConsoleArgumentVariants::_notification(int _notification)
{
	switch (_notification)
	{
		case NOTIFICATION_READY:
		{
			set_process_input(true);
		}
		break;

		// Fallthrough.
		case NOTIFICATION_PARENTED:
		case NOTIFICATION_UNPARENTED:
		{
			updateLineEdit();
		}
		break;

		case NOTIFICATION_ENTER_TREE:
		{
			ConsoleItemSelector*const previousSelectorPtr = m_itemSelector.getNode();
			m_itemSelector.onEnterTree(*this);
			ConsoleItemSelector*const newSelectorPtr = m_itemSelector.getNode();

			if (previousSelectorPtr != newSelectorPtr)
			{
				unregisterInSelector(previousSelectorPtr);
				registerInSelector(newSelectorPtr);
			}
		}
		break;

		default:
		break;
	}
}

void bwn::ConsoleArgumentVariants::setItemSelectorPath(const NodePath& _path)
{
	ConsoleItemSelector*const previousSelectorPtr = m_itemSelector.getNode();
	m_itemSelector.setNode(*this, _path);
	ConsoleItemSelector*const newSelectorPtr = m_itemSelector.getNode();

	if (previousSelectorPtr != newSelectorPtr)
	{
		unregisterInSelector(previousSelectorPtr);
		registerInSelector(newSelectorPtr);
	}
}

const NodePath& bwn::ConsoleArgumentVariants::getItemSelectorPath() const
{
	return m_itemSelector.getPath();
}

void bwn::ConsoleArgumentVariants::input(const Ref<InputEvent>& _event)
{
	// We can not handle events if line edit doesn't have focus (or we don't have line edit in general).
	if (m_parentLineEdit == nullptr || !m_parentLineEdit->hasFocus())
	{
		return;
	}

	const Ref<InputEventKey> keyEvent = _event;

	if (!keyEvent.is_valid() || !keyEvent->is_pressed())
	{
		return;
	}

	if (keyEvent->is_ctrl_pressed() && keyEvent->get_keycode() == Key::SPACE)
	{
		updateSelectorVariants(true);
		get_viewport()->set_input_as_handled();
	}
}

void bwn::ConsoleArgumentVariants::updateLineEdit()
{
	clearLineEdit();

	m_parentLineEdit = Object::cast_to<ConsoleLineEdit>(get_parent());
	if (m_parentLineEdit != nullptr)
	{
		m_parentLineEdit->connect("console_text_interaction", callable_mp(this, &ConsoleArgumentVariants::onConsoleTextInteraction));
		m_parentLineEdit->connect("cursor_position_changed",  callable_mp(this, &ConsoleArgumentVariants::onCursorPositionChanged));
	}
}

void bwn::ConsoleArgumentVariants::clearLineEdit()
{
	if (m_parentLineEdit != nullptr)
	{
		m_parentLineEdit->disconnect("console_text_interaction", callable_mp(this, &ConsoleArgumentVariants::onConsoleTextInteraction));
		m_parentLineEdit->disconnect("cursor_position_changed", callable_mp(this, &ConsoleArgumentVariants::onCursorPositionChanged));
	}

	m_parentLineEdit = nullptr;
}

void bwn::ConsoleArgumentVariants::fetchCommandNames(const bwn::string_view& _commandName)
{
	m_variants.clear();

	const auto consoleDatabase = ConsoleDatabase::getInstance();
	const std::vector<ConsoleDatabase::MethodData>& methods = consoleDatabase->getMethodsData();
	m_variants.reserve(methods.size());

	for (const ConsoleDatabase::MethodData& data : methods)
	{
		const String& methodName = data.getName();
		if (genericConsoleMatch(_commandName, bwn::wrap_view(methodName)))
		{
			m_variants.push_back(methodName);
		}
	}

	m_description = "command names:";
}

void bwn::ConsoleArgumentVariants::reloadVariants(const bool _force)
{
	m_variants.clear();

	if (m_parentLineEdit == nullptr)
	{
		return;
	}

	const console::CursorContext& cursorContext = m_parentLineEdit->getCursorContext();
	const console::Parser& parser = m_parentLineEdit->getParser();

	const bwn::string_view argumentString = parser.findArgumentStringRef(cursorContext);
	if (argumentString.empty() && !_force)
	{
		return;
	}

	if (cursorContext.localArgumentNumber == 0)
	{
		fetchCommandNames(argumentString);
		return;
	}

	const console::CommandsContext& commandsContext = parser.getCommandsContext();
	const bwn::string_view commandName = commandsContext.getCommandNameRef(cursorContext.commandIndex);
	if (commandName.empty())
	{
		// The command index could be invalid.
		return;
	}

	const ConsoleDatabase::MethodData& methodData = m_parentLineEdit->getConsoleMethodData();
	if (!methodData.valid())
	{
		return;
	}

	const size_t argumentIndex = cursorContext.localArgumentNumber - 1;
	const bwn::array_view<const bwn::ConsoleDatabase::ArgData> args = methodData.getArgInfos();
	if (argumentIndex >= args.size())
	{
		// Argument index could be invalid.
		return;
	}

	const bwn::ConsoleDatabase::ArgData& argumentData = args[argumentIndex];

	argumentData.getVariants(argumentString, m_variants);

	if (m_variants.empty() && argumentString.empty() && !argumentData.getDefault().is_empty())
	{
		// If we havent fetch anything, and we still haven't written anything either,
		// we still can try to use default value.
		m_variants.push_back(argumentData.getDefault());
	}

	const String& argumentName = argumentData.getName();
	if (!argumentName.is_empty())
	{
		m_description = bwn::format("variants for argument '{}':", argumentName);
	}
	else
	{
		m_description = bwn::format("variants for argument number {}:", argumentIndex + 1);
	}
}

void bwn::ConsoleArgumentVariants::updateSelectorVariants(const bool _force)
{
	ConsoleItemSelector*const itemSelectorPtr = m_itemSelector.getNode();
	if (itemSelectorPtr == nullptr)
	{
		return;
	}

	itemSelectorPtr->setCurrentUser(*this, &ConsoleArgumentVariants::variantAcceptedCallback);
	reloadVariants(_force);
	itemSelectorPtr->setVariants(std::move(m_variants));
	itemSelectorPtr->setDescription(m_description);
}

void bwn::ConsoleArgumentVariants::onConsoleTextInteraction(const String& _string, const StringId _interactionId)
{
	if (_interactionId == BWN_STRING_ID("input"))
	{
		// If text changed due to input from user, we should reload variants just as for changing cursor position.
		updateSelectorVariants(false);
	}
	else
	{
		// If text was changed due to some console plugin (like history. or ourselves), we should clear selector.
		unregisterInSelector(m_itemSelector.getNode());
		m_variants.clear();
	}
}

void bwn::ConsoleArgumentVariants::onCursorPositionChanged(const uint32_t _newPosition)
{
	updateSelectorVariants(false);
}

void bwn::ConsoleArgumentVariants::registerInSelector(ConsoleItemSelector*const _selector)
{
	if (_selector != nullptr)
	{
		_selector->setCurrentUser(*this, &ConsoleArgumentVariants::variantAcceptedCallback);
	}
}

void bwn::ConsoleArgumentVariants::unregisterInSelector(ConsoleItemSelector*const _selector)
{
	if (_selector != nullptr)
	{
		_selector->safeClearCurrentUser(*this);
	}
}

bwn::string_view bwn::ConsoleArgumentVariants::getPostfixForArgument(const console::CursorContext& _cursor, const console::CommandsContext& _commands) const
{
	const bwn::string_view spaceView = bwn::string_view(U" ");

	if (_cursor.commandIndex >= _commands.getCommands().size())
	{
		// If we past the last command, the space is required.
		return spaceView;
	}

	const console::Command& command = _commands.getCommands()[_cursor.commandIndex];
	// Is console line has any other argument, or we are redacting the last one.
	// The local argument number is always one more the respected argument index,
	// so by providing it directly, we basically ask for the next argument in console.
	const bool noMoreArgumentsInConsole = command.computeArgumentSpan(_cursor.localArgumentNumber).empty();

	const bool parsingCommandName = _cursor.localArgumentNumber == 0;
	// Is the actual command has any other argument past what ever we are currently redacting.
	const ConsoleDatabase::MethodData& methodData = m_parentLineEdit->getConsoleMethodData();
	const bool commandHasOtherArguments = methodData.getArgInfos().size() > _cursor.localArgumentNumber;

	return noMoreArgumentsInConsole && (parsingCommandName || commandHasOtherArguments)
		? spaceView
		: bwn::string_view();
}

void bwn::ConsoleArgumentVariants::variantAcceptedCallback(Object& _this, ConsoleItemSelector& _itemSelector, const String& _variant)
{
	ConsoleArgumentVariants& thisRef = static_cast<ConsoleArgumentVariants&>(_this);

	if (thisRef.m_parentLineEdit == nullptr)
	{
		return;
	}

	const console::CursorContext& cursorContext = thisRef.m_parentLineEdit->getCursorContext();
	const console::CommandsContext& commandsContext = thisRef.m_parentLineEdit->getParser().getCommandsContext();
	const String& oldText = thisRef.m_parentLineEdit->getParser().getText();
	String newText;

	const console::ArgumentInserter::Replacement replacement(
		bwn::string_view(),
		wrap_view(_variant),
		thisRef.getPostfixForArgument(cursorContext, commandsContext));

	const uint32_t newCharacterPos = thisRef.m_inserter.replaceArgument(
		replacement,
		commandsContext.getCommands(),
		cursorContext,
		wrap_view(oldText),
		newText);

#if defined(DEBUG_ENABLED)
	if (newCharacterPos == console::ArgumentInserter::k_characterNpos)
	{
		const std::string error = cursorContext.localArgumentNumber == 0
			? fmt::format(
				"Was unable to replace command name of command #{}, by text {}.",
				cursorContext.commandIndex + 1,
				_variant)
			: fmt::format(
				"Was unable to replace argument #{} from command #{}, by text {}.",
				cursorContext.localArgumentNumber,
				cursorContext.commandIndex + 1,
				_variant);
		BWN_ASSERT_WARNING(error);
	}
#endif // DEBUG_ENABLED

	if (newCharacterPos != console::ArgumentInserter::k_characterNpos)
	{
		thisRef.m_parentLineEdit->setConsoleText(newText, newCharacterPos, BWN_STRING_ID("selector"));
		thisRef.unregisterInSelector(&_itemSelector);
	}
}

