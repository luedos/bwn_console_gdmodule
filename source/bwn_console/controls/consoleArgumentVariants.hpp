#pragma once

#include "bwn_core/containers/arrayView.hpp"
#include "bwn_core/types/stringView.hpp"
#include "bwn_core/types/nodeProperty.hpp"
#include "bwn_core/types/stringId.hpp"

#include "bwn_console/parser/argumentInserter.hpp"
#include "bwn_console/consoleDatabase.hpp"

#include <core/object/ref_counted.h>
#include <scene/main/node.h>

#include <vector>
#include <array>

class InputEvent;

namespace bwn
{
	class ConsoleItemSelector;
	class ConsoleLineEdit;
	namespace console
	{
		class CommandsContext;
		struct CursorContext;
	} // namespace console

	class ConsoleArgumentVariants : public Node
	{
		GDCLASS(ConsoleArgumentVariants, Node)

		//
		// Construction and destruction.
		//
	public:
		ConsoleArgumentVariants();
		virtual ~ConsoleArgumentVariants() override;

		//
		// Godot methods.
		//
	private:
		static void _bind_methods();
		void _notification(int _notification);

		//
		// Public interface.
		//
	public:
		// Setter and getter for the item selector.
		void setItemSelectorPath(const NodePath& _path);
		const NodePath& getItemSelectorPath() const;

		//
		// Private methods.
		//
	private:
		// Input for the gui. Mostly used for arrows up and down.
		void input(const Ref<InputEvent>& _event) override;

		// Updates line edit from parent (subscribes to it's events etc.).
		void updateLineEdit();
		// Clears current line edit (unsubscribes from it's events etc.).
		void clearLineEdit();

		// Fills m_variants as names from all commands registered in console database.
		void fetchCommandNames(const bwn::string_view& _commandName);
		// General algorithm to fetch variants into m_variants.
		void reloadVariants(const bool _force);

		// Automatically registers in selector, reloads variants and updates selector with them.
		void updateSelectorVariants(const bool _force);

		// Callback triggered on text was changed quietly in console line.
		void onConsoleTextInteraction(const String& _string, const StringId _interactionId);
		// Callback triggered on line edit cursor position changed.
		void onCursorPositionChanged(const uint32_t _newPosition);

		// Registers itself in the item selector as an item selector current owner.
		void registerInSelector(ConsoleItemSelector*const _selector);
		// Unregisters itself from item selector.
		void unregisterInSelector(ConsoleItemSelector*const _selector);

		// Small helper function which deduces whenever it's needed or not to add additional space into the end of the replacement.
		// If space is required returns string view with it, otherwise empty string view is returned.
		bwn::string_view getPostfixForArgument(const console::CursorContext& _cursor, const console::CommandsContext& _commands) const;

		// This callback will be sent by the item selector on any variant being accepted.
		static void variantAcceptedCallback(Object& _this, ConsoleItemSelector& _itemSelector, const String& _variant);

		//
		// Private members.
		//
	private:
		// Item selector which is used to display variants.
		NodeProperty<ConsoleItemSelector, ConsoleArgumentVariants, void> m_itemSelector;
		// Line edit used for the selector.
		ConsoleLineEdit* m_parentLineEdit = nullptr;
		// This is just for fast and cached argument replacements.
		console::ArgumentInserter m_inserter;
		// Temp variants which are populated by variants collected from argument.
		std::vector<String> m_variants;
		// Temp cached description, which is filled after variants reload.
		String m_description;
	};
} // namespace bwn
