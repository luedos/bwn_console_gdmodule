#include "precompiled_console.hpp"

#include "bwn_console/controls/consoleItemSelector.hpp"
#include "bwn_console/controls/consoleLineEdit.hpp"

bwn::ConsoleItemSelector::ConsoleItemSelector()
	: m_labelResource( BWN_ONLY_IN_DEBUG("selector label") )
	, m_parentConsoleLine( BWN_ONLY_IN_DEBUG("console line")  )
	, m_contextOwner()
	, m_acceptedCallback( nullptr )
	, m_variants()
	, m_labels{ nullptr }
	, m_descriptionLabel()
	, m_variantsSpanStart( 0 )
	, m_currentLabelIndex( 0 )
	, m_labelUsedCount( 0 )
	, m_initialized( false )
{
	set_mouse_filter(MouseFilter::MOUSE_FILTER_IGNORE);
}

bwn::ConsoleItemSelector::~ConsoleItemSelector()
{
	clearLabels();
}

void bwn::ConsoleItemSelector::_bind_methods()
{
	// bwn::bindMethod(D_METHOD("_input"), &ConsoleItemSelector::input);

	BWN_BIND_PROPERTY(
		"label_resource",
		&ConsoleItemSelector::setLabelResource,
		&ConsoleItemSelector::getLabelResource,
		PropertyHint::PROPERTY_HINT_RESOURCE_TYPE,
		PackedScene::get_class_static());

	BWN_BIND_PROPERTY(
		"console_line_edit",
		&ConsoleItemSelector::setConsoleLineEditPath,
		&ConsoleItemSelector::getConsoleLineEditPath,
		PropertyHint::PROPERTY_HINT_NODE_PATH_VALID_TYPES,
		Control::get_class_static());
}

void bwn::ConsoleItemSelector::_notification(int _notification)
{
	switch (_notification)
	{
		case NOTIFICATION_PROCESS:
		{
			updatePosition();
		}
		break;

		case NOTIFICATION_ENTER_TREE:
		{
			instanceLabels();
			m_parentConsoleLine.onEnterTree(*this);
		}
		break;
		
		case NOTIFICATION_EXIT_TREE:
		{
			clearLabels();
		}
		break;

		default:
		break;
	}
}

Ref<PackedScene> bwn::ConsoleItemSelector::getLabelResource() const
{
	return m_labelResource.getResource();
}

void bwn::ConsoleItemSelector::setConsoleLineEditPath(const NodePath& _path)
{
	m_parentConsoleLine.setNode(*this, _path);
}

const NodePath& bwn::ConsoleItemSelector::getConsoleLineEditPath() const
{
	return m_parentConsoleLine.getPath();
}

void bwn::ConsoleItemSelector::setCurrentUser(Object& _user, const ItemAcceptedCCallbackType _callback)
{
	BWN_TRAP_COND(_callback != nullptr, "The nullptr is provided for the callback in console item selector. Who did this?");
	if (m_contextOwner.get() == &_user && m_acceptedCallback == _callback)
	{
		return;
	}

	m_contextOwner = ObjectIdRef<Object>(_user);
	m_acceptedCallback = _callback;

	clearVariants();
	clearDescription();
}

bool bwn::ConsoleItemSelector::trySetCurrentUser(Object& _user, const ItemAcceptedCCallbackType _callback)
{
	const Object*const previousOwner = m_contextOwner.get();
	if (previousOwner != nullptr && previousOwner != &_user)
	{
		// If previous owner is active and not our new user, we can't reset owner.
		return false;
	}

	BWN_TRAP_COND(_callback != nullptr, "The nullptr is provided for the callback in console item selector. Who did this?");
	if (previousOwner == &_user && m_acceptedCallback == _callback)
	{
		// If nothing changed, no reason to clear anything, but we still need to return true.
		return true;
	}

	m_contextOwner = ObjectIdRef<Object>(_user);
	m_acceptedCallback = _callback;

	clearVariants();
	clearDescription();
	return true;
}

void bwn::ConsoleItemSelector::safeClearCurrentUser(const Object& _user)
{
	if (m_contextOwner.get() != &_user)
	{
		return;
	}

	m_contextOwner.reset();
	m_acceptedCallback = nullptr;

	clearVariants();
	clearDescription();
}

bool bwn::ConsoleItemSelector::isCurrentUser(const Object& _user) const
{
	return m_contextOwner.get() == &_user;
}

void bwn::ConsoleItemSelector::setDescription(const String& _description)
{
	if (Label*const descriptionLabelPtr = m_descriptionLabel.get())
	{
		if (_description.is_empty())
		{
			if (descriptionLabelPtr->get_parent() == this)
			{
				remove_child(descriptionLabelPtr);
			}
		}
		else if (descriptionLabelPtr->get_parent() != this)
		{
			add_child(descriptionLabelPtr);
			move_child(descriptionLabelPtr, 0);
		}

		descriptionLabelPtr->set_text(_description);
	}
}

void bwn::ConsoleItemSelector::setVariants(std::vector<String>&& _variants)
{
	m_variants.clear();
	m_variants.swap(_variants);
	updateListSize(static_cast<uint32_t>(std::min(m_variants.size(), m_labels.size())));
}

void bwn::ConsoleItemSelector::setLabelResource(const Ref<PackedScene>& _resource)
{
	m_labelResource.setResource(_resource, false);
}

void bwn::ConsoleItemSelector::input(const Ref<InputEvent>& _event)
{
	// We could skip if there is literally no labels exist.
	// If there is only 1 label, we still should consume event, even though there is also nothing will happend.
	if (!m_initialized || m_labelUsedCount == 0 || !is_visible())
	{
		return;
	}

	const Ref<InputEventKey> keyEvent = _event;

	if (!keyEvent.is_valid() || !keyEvent->is_pressed() || keyEvent->is_meta_pressed()) 
	{
		return;
	}

	switch (keyEvent->get_keycode())
	{
		case Key::UP:
		{
			itemUp();
			accept_event();
		} 
		break;

		case Key::DOWN:
		{
			itemDown();
			accept_event();
		}
		break;

		case Key::ENTER:
		case Key::TAB:
		{
			if (!m_variants.empty())
			{
				acceptCurrentVariant();
				accept_event();
			}
		}
		break;

		case Key::ESCAPE:
		{
			m_contextOwner = nullptr;
			m_acceptedCallback = nullptr;
			hide();
			accept_event();
		}
		break;

		default:
		break;
	}
}

void bwn::ConsoleItemSelector::itemUp()
{
	if (m_labelUsedCount < 2)
	{
		// If there 0 or 1 label, nothing to do..
		return;
	}

	if (m_currentLabelIndex == 0)
	{
		// If current label the very first one, wrap selection all the way around to the end.
		updateListHead(m_labelUsedCount - 1, m_variants.size() - m_labelUsedCount);
		return;
	}

	// Test to know if we are at the start of the list.
	if (m_variantsSpanStart == 0)
	{
		// If we are at the start of the list just move current label index 1 up.
		updateListHead(m_currentLabelIndex - 1, 0);
		return;
	}

	// Stable position is one in which we could go up and only variants span would actually change, but current label positin would not.
	const size_t stablePosition = (m_labelUsedCount - 1) / 2;

	if (m_currentLabelIndex == stablePosition)
	{
		updateListHead(m_currentLabelIndex, m_variantsSpanStart - 1);
	}
	else
	{
		updateListHead(m_currentLabelIndex - 1, m_variantsSpanStart);
	}
}

void bwn::ConsoleItemSelector::itemDown()
{
	if (m_labelUsedCount < 2)
	{
		// If there 0 or 1 label, nothing to do..
		return;
	}

	if (m_currentLabelIndex == m_labelUsedCount - 1)
	{
		// If current label the very last one, wrap selection all the way around to the start.
		updateListHead(0, 0);
		return;
	}

	const size_t lastSpanPosition = m_variants.size() - m_labelUsedCount;
	if (m_variantsSpanStart >= lastSpanPosition)
	{
		// If we are at the end of the list just move current label index 1 up.
		updateListHead(m_currentLabelIndex + 1, lastSpanPosition);
		return;
	}

	// Stable position is one in which we could go up and only variants span would actually change, but current label positin would not.
	const size_t stablePosition = m_labelUsedCount / 2;

	if (m_currentLabelIndex == stablePosition)
	{
		updateListHead(m_currentLabelIndex, m_variantsSpanStart + 1);
	}
	else
	{
		updateListHead(m_currentLabelIndex + 1, m_variantsSpanStart);
	}
}

void bwn::ConsoleItemSelector::updateListSize(const uint32_t _labelsUsedCount)
{
	// Event if m_variantsSpanStart is invalid after new size, it will adjust itself.
	updateListPosition(_labelsUsedCount, m_variantsSpanStart);
	updateVisibility();
}

void bwn::ConsoleItemSelector::updateListHead(const uint32_t _labelIndex, const uint32_t _variantsStart)
{
	if (!m_initialized)
	{
		return;
	}

	if (m_labelUsedCount == 0)
	{
		m_currentLabelIndex = 0;
		m_variantsSpanStart = 0;
		return;
	}

	BWN_TRAP_COND(
		_labelIndex < m_labelUsedCount, 
		"New label index ({}) cannot be greater then total used label count ({}). This will couse buffer overflow.", 
		_labelIndex, 
		m_labelUsedCount);
	BWN_TRAP_COND(
		_variantsStart <= (m_variants.size() - m_labelUsedCount),
		"Variants span start index ({}) cannot be greater then max span position ({}). This will couse buffer overflow.",
		_variantsStart,
		m_variants.size() - m_labelUsedCount);
	
	m_currentLabelIndex = _labelIndex;
	m_variantsSpanStart = _variantsStart;

	for (size_t labelIndex = 0; labelIndex < m_labelUsedCount; ++labelIndex)
	{
		String labelText = m_variants[m_variantsSpanStart + labelIndex];
		
		if (labelIndex == m_currentLabelIndex)
		{
			labelText = String("> ") + labelText;
		}

		m_labels[labelIndex]->set_text(labelText);
	}

	if (m_variants.size() > m_labels.size())
	{
		if (m_variantsSpanStart > 0)
		{
			m_labels.front()->set_text("...");
		}

		if (m_variantsSpanStart < (m_variants.size() - m_labelUsedCount))
		{
			m_labels.back()->set_text("...");
		}
	}
}

void bwn::ConsoleItemSelector::updateListPosition(const uint32_t _labelsUsedCount, const uint32_t _variantsStart)
{
	if (!m_initialized)
	{
		return;
	}

	const uint32_t oldUsedCount = m_labelUsedCount;

	if (_labelsUsedCount > oldUsedCount)
	{
		for (uint32_t localLabelIndex = 0; localLabelIndex < (_labelsUsedCount - oldUsedCount); ++localLabelIndex)
		{
			add_child(m_labels[oldUsedCount + localLabelIndex].get());
		}
	}
	else
	{
		for (uint32_t localLabelIndex = 0; localLabelIndex < (oldUsedCount - _labelsUsedCount); ++localLabelIndex)
		{
			remove_child(m_labels[_labelsUsedCount + localLabelIndex].get());
		}
	}

	// Reseting size, because it could have been missed after children readding
	set_size(get_combined_minimum_size());

	m_labelUsedCount = _labelsUsedCount;
	if (m_labelUsedCount != 0)
	{
		updateListHead(
			std::min(m_labelUsedCount - 1, m_currentLabelIndex),
			std::min(static_cast<uint32_t>(m_variants.size()) - m_labelUsedCount, _variantsStart));
	}
	else
	{
		m_currentLabelIndex = 0;
		m_variantsSpanStart = 0;
	}
}

void bwn::ConsoleItemSelector::instanceLabels()
{
	clearLabels();

	if (!m_labelResource.isResourceValid())
	{
		return;
	}

	for (UniqueRef<Label>& label : m_labels)
	{
		if (!m_labelResource.forceLoadResource())
		{
			clearLabels();
			return;
		}

		label = UniqueRef<Label>(m_labelResource.releaseNode());
	}

	if (!m_labelResource.forceLoadResource())
	{
		clearLabels();
		return;
	}

	m_descriptionLabel = UniqueRef<Label>(m_labelResource.releaseNode());

	m_initialized = true;
	updateListSize(std::min(m_labels.size(), m_variants.size()));
}

void bwn::ConsoleItemSelector::clearLabels()
{
	for (UniqueRef<Label>& label : m_labels)
	{
		Label*const labelPtr = label.get();
		if (labelPtr != nullptr && labelPtr->get_parent() == this)
		{
			remove_child(labelPtr);
		}

		label.reset();
	}

	m_descriptionLabel.reset();

	m_initialized = false;
}

void bwn::ConsoleItemSelector::clearVariants()
{
	m_variants.clear();
	updateListSize(0);
}

void bwn::ConsoleItemSelector::clearDescription()
{
	if (Label*const descriptionLabelPtr = m_descriptionLabel.get())
	{
		descriptionLabelPtr->set_text(String());
		if (descriptionLabelPtr->get_parent() == this)
		{
			remove_child(descriptionLabelPtr);
			set_size(get_combined_minimum_size());
		}
	}
}

void bwn::ConsoleItemSelector::updatePosition()
{
	const Rect2 viewportRect = get_viewport_rect();
	const Rect2 selectorRect = get_global_rect();
	const Rect2 lineEditRect = m_parentConsoleLine->get_global_rect();

	const bool showSelectorAtBottom = lineEditRect.position.y + lineEditRect.size.height + selectorRect.size.height < viewportRect.size.height;
	const Point2 newPosition = Point2(
		lineEditRect.position.x,
		showSelectorAtBottom
			? lineEditRect.position.y + lineEditRect.size.height
			: lineEditRect.position.y - selectorRect.size.height);

	set_global_position(newPosition);
}

void bwn::ConsoleItemSelector::updateVisibility()
{
	const bool shouldBeActive = m_parentConsoleLine.isValid() && !m_variants.empty();
	set_visible(shouldBeActive);
	set_process(shouldBeActive);
	set_process_input(shouldBeActive);
}

void bwn::ConsoleItemSelector::acceptCurrentVariant()
{
	Object*const contextOwnerPtr = m_contextOwner.get();
	if (contextOwnerPtr == nullptr)
	{
		return;
	}

	const size_t currentVariantIndex = m_variantsSpanStart + m_currentLabelIndex;
	if (currentVariantIndex >= m_variants.size())
	{
		return;
	}

	m_acceptedCallback(*contextOwnerPtr, *this, m_variants[currentVariantIndex]);
}