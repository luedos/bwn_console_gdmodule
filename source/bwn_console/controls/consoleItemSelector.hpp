#pragma once

#include "bwn_core/types/sceneResourceProperty.hpp"
#include "bwn_core/types/nodeProperty.hpp"
#include "bwn_core/containers/arrayView.hpp"
#include "bwn_core/types/uniqueRef.hpp"
#include "bwn_core/types/objectIdRef.hpp"

#include <core/object/ref_counted.h>
#include <scene/gui/box_container.h>

#include <vector>
#include <array>

class Label;
class InputEvent;
class PackedScene;
class String;
class LineEdit;

namespace bwn
{
	class ConsoleLineEdit;

	class ConsoleItemSelector : public VBoxContainer
	{
		GDCLASS(ConsoleItemSelector, VBoxContainer);

		//
		// Public typedefs.
		//
	public:
		// A simple static callback which is called whenever item in selector is accepted.
		using ItemAcceptedCCallbackType = void(*)(Object& _contextOwner, ConsoleItemSelector& _itemSelector, const String& _variant);

		//
		// Construction and destruction.
		//
	public:
		ConsoleItemSelector();
		virtual ~ConsoleItemSelector() override;

		//
		// Godot methods.
		//
	private:
		static void _bind_methods();
		void _notification(int _notification);

		//
		// Public interface.
		//
	public:
		// Sets the resource for the labels used as an items in selector list
		void setLabelResource(const Ref<PackedScene>& _resource);
		// Returns the label resource used as an items in selector list.
		Ref<PackedScene> getLabelResource() const;

		// Getter/Setter for the parent line edit, this selector will be attached to.
		void setConsoleLineEditPath(const NodePath& _path);
		const NodePath& getConsoleLineEditPath() const;

		// Sets current user of the console item selector. There should be only one user, so this call will reset the old user.
		// the callback pointer should not be nullptr.
		void setCurrentUser(Object& _user, const ItemAcceptedCCallbackType _callback);
		// Tries to reset current user, and returns true if succeeded. New user is set only if current user is empty.
		// the callback pointer should not be nullptr.
		bool trySetCurrentUser(Object& _user, const ItemAcceptedCCallbackType _callback);

		// Clears current user in case when current user is one provided. If current user is not _user, this call will be ignored.
		void safeClearCurrentUser(const Object& _user);

		// Returns true if provided user is the current one.
		bool isCurrentUser(const Object& _user) const;

		// Sets small bit of text for description which will be shown above all item variants.
		void setDescription(const String& _description);
		// Resets current variants of the item selector.
		void setVariants(std::vector<String>&& _variants);

		//
		// Private members.
		//
	private:
		// Input for the gui. Mostly used for arrows up and down.
		void input(const Ref<InputEvent>& _event) override;

		// Moves item up on the list.
		void itemUp();
		// Moves item down to the list.
		void itemDown();

		// Updates labels inheritance, and sets current label position at the start.
		void updateListSize(const uint32_t _labelsUsedCount);
		// Updates list without changing the number of labels
		void updateListHead(const uint32_t _labelIndex, const uint32_t _variantsStart);
		// Updates list span position, and span size. If it's needed, also updates labelIndex.
		void updateListPosition(const uint32_t _labelsUsedCount, const uint32_t _variantsStart);

		// Creates labels from the resource, and places them into the labels array.
		void instanceLabels();
		// Clears all the labels from labels array.
		void clearLabels();

		// Clears all current variants and hides all used labels.
		void clearVariants();
		// Clears the description and hides description label.
		void clearDescription();

		// Updates self selector position based on parent line edit.
		void updatePosition();
		// Updates self visibility based on the amount of variants currently in the selector.
		void updateVisibility();

		// Sends an event that current variant was accepted.
		void acceptCurrentVariant();

		//
		// Private members.
		//
	private:
		// Resource for the label used to display.
		SceneResourceProperty<Label> m_labelResource;
		// Line edit used for the selector.
		NodeProperty<Control, ConsoleItemSelector, void> m_parentConsoleLine;

		// The current context owner
		ObjectIdRef<Object> m_contextOwner;
		// The callback which is used to alert current context owner.
		ItemAcceptedCCallbackType m_acceptedCallback;

		// All variants used to fill labels.
		std::vector<String> m_variants;
		// Actual label spawned references.
		std::array<UniqueRef<Label>, 8> m_labels;
		// The specific label used for description.
		UniqueRef<Label> m_descriptionLabel;

		// The start index of variants span of the m_variants vector (span size is m_labelUsedCount).
		uint32_t m_variantsSpanStart = 0;
		// Global index of currently used variant.
		uint32_t m_currentLabelIndex = 0;
		// Number of labels (and variants) currently used in selector.
		uint32_t m_labelUsedCount = 0;

		// Are labels were initialized (spawned).
		bool m_initialized = false;
	};
} // namespace bwn