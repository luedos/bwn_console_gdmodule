#include "precompiled_console.hpp"

#include "bwn_console/controls/consoleLineEdit.hpp"

#include "bwn_console/controls/consoleItemSelector.hpp"
#include "bwn_console/controls/consoleLineHistory.hpp"
#include "bwn_console/consoleLineFilter.hpp"
#include "bwn_console/consoleDatabase.hpp"


struct bwn::ConsoleLineEdit::LineEditBinds
{
	static constexpr NodeSignalBindsTuple k_binds = NodeSignalBindsTuple(
		NodeSignalBind( "text_entered", &ConsoleLineEdit::onTextEntered ),
		NodeSignalBind( "text_changed", &ConsoleLineEdit::onTextChanged )
	);
};

bwn::ConsoleLineEdit::ConsoleLineEdit()
	: m_parser()
	, m_context()
	, m_lineEdit( BWN_ONLY_IN_DEBUG("line edit") )
	, m_currentMethodData( String() )
{
	set_process(true);
}

bwn::ConsoleLineEdit::~ConsoleLineEdit() = default;

void bwn::ConsoleLineEdit::_bind_methods()
{	
	bwn::addSignal<ConsoleLineEdit>(
		MethodUtility<void(const String&, StringId)>("console_text_interaction").
		argName<0>("text").
		argName<0>("edition_type").
		get());

	bwn::addSignal<ConsoleLineEdit>(
		MethodUtility<void(uint32_t)>("cursor_position_changed").
		argName<0>("new_position").
		get());

	bwn::addSignal<ConsoleLineEdit>(
		MethodUtility<void()>("console_method_data_changed").
		get());

	BWN_BIND_PROPERTY(
		"line_edit",
		&ConsoleLineEdit::setLineEditPath,
		&ConsoleLineEdit::getLineEditPath,
		PROPERTY_HINT_NODE_PATH_VALID_TYPES,
		LineEdit::get_class_static());
}

void bwn::ConsoleLineEdit::_notification(const int _notification)
{
	switch (_notification)
	{
		case NOTIFICATION_PROCESS:
		{
			uint32_t newCursorPos = m_context.characterIndex;

			if (const LineEdit*const lineEditPtr = m_lineEdit.getNode())
			{
				newCursorPos = static_cast<uint32_t>(lineEditPtr->get_caret_column());
			}

			if (newCursorPos != m_context.characterIndex)
			{
				m_context = m_parser.composeContext(newCursorPos);
				emit_signal(SNAME("cursor_position_changed"), newCursorPos);
			}
		}
		break;

		case NOTIFICATION_ENTER_TREE:
		{
			m_lineEdit.onEnterTree(*this);
		}
		break;

		default:
			break;
	}
}

void bwn::ConsoleLineEdit::setLineEditPath(const NodePath& _path)
{
	m_lineEdit.setNode(*this, _path);
}

const NodePath& bwn::ConsoleLineEdit::getLineEditPath() const
{
	return m_lineEdit.getPath();
}

const bwn::console::CursorContext& bwn::ConsoleLineEdit::getCursorContext() const
{
	return m_context;
}

const bwn::console::Parser& bwn::ConsoleLineEdit::getParser() const
{
	return m_parser;
}

void bwn::ConsoleLineEdit::setConsoleText(const String& _text, const StringId _editionId)
{
	setConsoleText(_text, _text.length(), _editionId);
}

void bwn::ConsoleLineEdit::setConsoleText(const String& _text, const uint32_t _cursorPos, const StringId _editionId)
{
	LineEdit*const lineEditPtr = m_lineEdit.getNode();
	if (lineEditPtr == nullptr)
	{
		return;
	}

	const uint32_t clampedPosition = std::min(_cursorPos, static_cast<uint32_t>(_text.length()));

	lineEditPtr->set_text(_text);
	lineEditPtr->set_caret_column(static_cast<int>(clampedPosition));

	m_parser.parse(_text);
	m_context = m_parser.composeContext(clampedPosition);
	updateMethodData();

	// This will also trigger functions inside ConsoleLineEdit itself, to change reparse console text, and change it's cursor position.
	emit_signal(SNAME("console_text_interaction"), _text, _editionId);
}

const String& bwn::ConsoleLineEdit::getText() const
{
	return m_parser.getText();
}

void bwn::ConsoleLineEdit::setCursorPosition(const uint32_t _position)
{
	LineEdit*const lineEditPtr = m_lineEdit.getNode();
	if (lineEditPtr == nullptr)
	{
		return;
	}

	const uint32_t clampedPosition = std::min(_position, static_cast<uint32_t>(lineEditPtr->get_text().length()));

	lineEditPtr->set_caret_column(static_cast<int>(clampedPosition));
	m_context = m_parser.composeContext(clampedPosition);
	updateMethodData();

	emit_signal(SNAME("cursor_position_changed"), clampedPosition);
}

bool bwn::ConsoleLineEdit::hasFocus() const
{
	return m_lineEdit.isValid() && m_lineEdit->has_focus();
}

void bwn::ConsoleLineEdit::grabFocus()
{
	if (LineEdit*const lineEditPtr = m_lineEdit.getNode())
	{
		lineEditPtr->grab_focus();
	}
}

void bwn::ConsoleLineEdit::releaseFocus()
{
	if (LineEdit*const lineEditPtr = m_lineEdit.getNode())
	{
		lineEditPtr->release_focus();
	}
}

const bwn::ConsoleDatabase::MethodData& bwn::ConsoleLineEdit::getConsoleMethodData() const
{
	return m_currentMethodData;
}

void bwn::ConsoleLineEdit::onTextChanged(const String& _string)
{
	m_parser.parse(_string);
	m_context = m_parser.composeContext(m_lineEdit->get_caret_column());
	updateMethodData();

	emit_signal(SNAME("console_text_interaction"), _string, BWN_STRING_ID("input"));
}

void bwn::ConsoleLineEdit::onTextEntered(const String& _string)
{
	// Just in case, reparse string.
	m_parser.parse(_string);
	if (m_parser.hasErrors())
	{
		::print_error(bwn::format("Next parse errors were found while parsing command line \"{}\":", _string));
		m_parser.getErrorsContext().printErrors();
		return;
	}

	std::vector<Variant> arguments;
	auto consoleDatabase = ConsoleDatabase::getInstance();

	const std::vector<console::Command>& commands = m_parser.getCommandsContext().getCommands();
	for (uint32_t commandIndex = 0; commandIndex < commands.size(); ++commandIndex)
	{
		const console::Command& command = commands[commandIndex];
		const SystemResult formattingResult = command.formatArguments(arguments);
		if (!formattingResult.success())
		{
			::print_error(bwn::format("Error formatting command #{}: {}", commandIndex + 1, formattingResult.getErrorDescription()));
			break;
		}
		const bwn::string_view commandName = command.getName();
		BWN_LOG("Calling console command \"{}\".", m_parser.getText());

		const SystemResult invokeResult = consoleDatabase->call(commandName, arguments);

		if (!invokeResult.success())
		{
			::print_error(bwn::format(
				"Failed executing command \"{}\" with error ({}): {}.", 
				bwn::wrap_utf(commandName),
				bwn::wrap_utf(invokeResult.stringFromError()),
				invokeResult.getErrorDescription()));
			break;
		}
	}

	emit_signal(SNAME("console_text_interaction"), _string, BWN_STRING_ID("entered"));
	
	m_parser.clear();
	m_lineEdit->set_text(String());

	clearMethodData();
}

void bwn::ConsoleLineEdit::updateMethodData()
{
	const console::CommandsContext& commandsContext = m_parser.getCommandsContext();
	if (m_context.commandIndex >= commandsContext.getCommands().size())
	{
		clearMethodData();
		return;
	}

	const bwn::string_view commandName = commandsContext.getCommandNameRef(m_context.commandIndex);
	if (commandName.empty())
	{
		clearMethodData();
		return;
	}

	if (commandName == bwn::wrap_view(m_currentMethodData.getName()))
	{
		// If name hasn't changed, no reason to parse condole database second time
		return;
	}

	const ConsoleDatabase::MethodData*const foundMethodData = ConsoleDatabase::getInstance()->findMethodData(commandName);
	if (foundMethodData == nullptr)
	{
		clearMethodData();
		return;
	}

	m_currentMethodData = *foundMethodData;
	emit_signal(SNAME("console_method_data_changed"));
}

void bwn::ConsoleLineEdit::clearMethodData()
{
	const bool wasValid = m_currentMethodData.valid();
	m_currentMethodData.clear();
	if (wasValid)
	{
		emit_signal(SNAME("console_method_data_changed"));
	}
}