#pragma once

#include "bwn_core/types/stringId.hpp"
#include "bwn_core/types/nodeProperty.hpp"

#include "bwn_console/parser/cursorContext.hpp"
#include "bwn_console/parser/parser.hpp"

#include "bwn_console/consoleDatabase.hpp"

#include <scene/gui/panel_container.h>

class LineEdit;

namespace bwn
{
	class ConsoleLineEdit : public PanelContainer
	{
		GDCLASS(ConsoleLineEdit, PanelContainer);

		//
		// Private classes.
		//
	private:
		// Signal binds used in m_lineEdit
		struct LineEditBinds;

		//
		// Construction and destruction.
		//
	public:
		ConsoleLineEdit();
		virtual ~ConsoleLineEdit() override;

		//
		// Godot methods.
		//
	private:
		static void _bind_methods();
		void _notification(const int _notification);

		//
		// Public interface.
		//
	public:
		// Setter and getter for the line edit property.
		void setLineEditPath(const NodePath& _path);
		const NodePath& getLineEditPath() const;
		// Returns current context of the cursor.
		const console::CursorContext& getCursorContext() const;
		// Returns parser to retrieve current parsed state.
		const console::Parser& getParser() const;
		// Preferred alternative to 'set_text'. This will not only reparse text command, but also send 'console_text_set' event.
		// Additional stringId parameter is used to identify specific edition of text.
		void setConsoleText(const String& _text, const StringId _editionId);
		void setConsoleText(const String& _text, const uint32_t _cursorPos, const StringId _editionId);
		// Returns current text in the line edit field.
		const String& getText() const;
		// Directly sets new cursor position for the line edit.
		void setCursorPosition(const uint32_t _position);
		// Returns true if current line edit has focus.
		bool hasFocus() const;
		// Grabs the focus by the line edit, if one exist.
		void grabFocus();
		// Releases focus from the line edit if one exist.
		void releaseFocus();
		// Returns data for the console method which is currently edited in the console.
		// The method data could be invalid, if current method wasn't found in the console database.
		const ConsoleDatabase::MethodData& getConsoleMethodData() const;

		//
		// Private methods.
		//
	private:
		// Signal on text updated.
		void onTextChanged(const String& _string);
		// Signal on text entered.
		void onTextEntered(const String& _string);
		// Updates current method data based on the input from the console.
		void updateMethodData();
		// Clears the current method data.
		void clearMethodData();

		//
		// Private members.
		//
	private:
		// Actual parser which doing all the work.
		console::Parser m_parser;
		// Current cursor context.
		console::CursorContext m_context;
		// The line edit primarily used as an input for the console.
		NodeProperty<LineEdit, ConsoleLineEdit, LineEditBinds> m_lineEdit;
		// Current method data parsed from the console.
		ConsoleDatabase::MethodData m_currentMethodData;
	};
} // namespace bwn