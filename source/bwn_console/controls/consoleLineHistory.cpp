#include "precompiled_console.hpp"

#include "bwn_console/controls/consoleLineHistory.hpp"
#include "bwn_console/controls/consoleLineEdit.hpp"
#include "bwn_console/controls/consoleItemSelector.hpp"
#include "bwn_console/consoleLineHistoryDatabase.hpp"

bwn::ConsoleLineHistory::ConsoleLineHistory()
	: m_itemSelector( BWN_ONLY_IN_DEBUG("item selector") )
	, m_tempVariants()
	, m_parentLineEdit( nullptr )
	, m_history()
	, m_tempCommand()
	, m_historyIndex( 0 )
{}

bwn::ConsoleLineHistory::~ConsoleLineHistory() = default;

void bwn::ConsoleLineHistory::_bind_methods()
{
	BWN_BIND_PROPERTY(
		"item_selector",
		&ConsoleLineHistory::setItemSelectorPath,
		&ConsoleLineHistory::getItemSelectorPath,
		PROPERTY_HINT_NODE_PATH_VALID_TYPES,
		ConsoleItemSelector::get_class_static());
}

void bwn::ConsoleLineHistory::_notification(const int _notification)
{
	switch (_notification)
	{
		case NOTIFICATION_READY:
		{
			set_process_input(true);
		}
		// Fallthrough.
		case NOTIFICATION_PARENTED:
		case NOTIFICATION_UNPARENTED:
		{
			updateLineEdit();
		}
		break;

		case NOTIFICATION_ENTER_TREE:
		{
			m_itemSelector.onEnterTree(*this);
			if (ConsoleLineHistoryDatabase::isSingletonValid())
			{
				m_history = ConsoleLineHistoryDatabase::getInstance()->getLines();
				m_historyIndex = m_history.size();
			}
		}
		break;

		case NOTIFICATION_EXIT_TREE:
		{
			if (ConsoleLineHistoryDatabase::isSingletonValid())
			{
				ConsoleLineHistoryDatabase::getInstance()->setLines(m_history);
				ConsoleLineHistoryDatabase::getInstance()->dumpDatabase();
			}
		}
		break;
	}
}

void bwn::ConsoleLineHistory::setItemSelectorPath(const NodePath& _path)
{
	ConsoleItemSelector*const previousItemSelector = m_itemSelector.getNode();
	if (previousItemSelector != nullptr)
	{
		previousItemSelector->safeClearCurrentUser(*this);
	}

	m_itemSelector.setNode(*this, _path);
}

const NodePath& bwn::ConsoleLineHistory::getItemSelectorPath() const
{
	return m_itemSelector.getPath();
}

void bwn::ConsoleLineHistory::input(const Ref<InputEvent>& _event)
{
	if (m_parentLineEdit == nullptr || !m_parentLineEdit->hasFocus() || m_history.empty())
	{
		return;
	}

	const Ref<InputEventKey> keyEvent = _event;

	if (!keyEvent.is_valid() || !keyEvent->is_pressed())
	{
		return;
	}

	if (keyEvent->is_ctrl_pressed() && keyEvent->get_keycode() == Key::H)
	{
		if (ConsoleItemSelector*const itemSelectorPtr = m_itemSelector.getNode())
		{
			itemSelectorPtr->setCurrentUser(*this, &ConsoleLineHistory::variantAcceptedCallback);
			itemSelectorPtr->setDescription("history:");
			m_tempVariants = m_history;
			std::reverse(m_tempVariants.begin(), m_tempVariants.end());
			itemSelectorPtr->setVariants(std::move(m_tempVariants));
		}

		return;
	}

	if (keyEvent->is_command_or_control_pressed() || keyEvent->is_ctrl_pressed() || keyEvent->is_alt_pressed() || keyEvent->is_meta_pressed())
	{
		return;
	}

	switch (keyEvent->get_keycode())
	{
		case Key::UP:
		{
			moveHistoryUp();
			get_viewport()->set_input_as_handled();
		} 
		break;

		case Key::DOWN:
		{
			moveHistoryDown();
			get_viewport()->set_input_as_handled();
		}
		break;
		
		default:
		break;
	}
}

void bwn::ConsoleLineHistory::updateLineEdit()
{
	clearLineEdit();

	if (ConsoleLineEdit*const lineEdit = Object::cast_to<ConsoleLineEdit>(get_parent()))
	{
		m_parentLineEdit = lineEdit;
		m_parentLineEdit->connect("console_text_interaction", callable_mp(this, &ConsoleLineHistory::onConsoleTextInteraction));
	}
}

void bwn::ConsoleLineHistory::clearLineEdit()
{
	if (m_parentLineEdit != nullptr)
	{
		m_parentLineEdit->disconnect("console_text_interaction", callable_mp(this, &ConsoleLineHistory::onConsoleTextInteraction));
		m_parentLineEdit = nullptr;
	}
}

void bwn::ConsoleLineHistory::pushIntoHistory(const String& _command)
{
	if (m_history.empty() || m_history.back() != _command)
	{
		m_history.push_back(_command);
	}
	
	m_historyIndex = m_history.size();
}

void bwn::ConsoleLineHistory::onConsoleTextInteraction(const String &_string, const StringId _interactionId)
{
	if (_interactionId == BWN_STRING_ID("entered"))
	{
		pushIntoHistory(_string);
		m_tempCommand.clear();
	}
	else if (_interactionId != BWN_STRING_ID("history"))
	{
		// If we are updating text for any reason other then ourselves, we should reset history.
		m_historyIndex = m_history.size();
		m_tempCommand.clear();
	}
}

void bwn::ConsoleLineHistory::moveHistoryUp()
{
	if (m_historyIndex == 0)
	{
		// we can't cicle through history. If this is top position, we can't go thurther.
		return;
	}

	if (m_historyIndex == m_history.size())
	{
		if (m_parentLineEdit != nullptr)
		{
			m_tempCommand = m_parentLineEdit->getText();
		}
	}

	--m_historyIndex;
	if (m_historyIndex >= m_history.size())
	{
		// just in case..
		return;
	}

	if (m_parentLineEdit != nullptr)
	{
		const String& historyString = m_history[m_historyIndex];
		m_parentLineEdit->setConsoleText(historyString, BWN_STRING_ID("history"));
	}
}

void bwn::ConsoleLineHistory::moveHistoryDown()
{
	if (m_historyIndex >= m_history.size())
	{
		// we can't cycle through history. If this is the most bottom position, we can't go further.
		return;
	}

	++m_historyIndex;
	if (m_parentLineEdit != nullptr)
	{
		const String& historyString = m_historyIndex < m_history.size()
			? m_history[m_historyIndex]
			: m_tempCommand;

		m_parentLineEdit->setConsoleText(historyString, BWN_STRING_ID("history"));
	}
}

void bwn::ConsoleLineHistory::variantAcceptedCallback(Object& _this, ConsoleItemSelector& _itemSelector, const String& _variant)
{
	ConsoleLineHistory& thisRef = static_cast<ConsoleLineHistory&>(_this);

	if (thisRef.m_parentLineEdit == nullptr)
	{
		return;
	}

	thisRef.m_parentLineEdit->setConsoleText(_variant, BWN_STRING_ID("history"));
	thisRef.pushIntoHistory(_variant);

	_itemSelector.safeClearCurrentUser(_this);
}