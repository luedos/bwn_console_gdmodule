#pragma once

#include "bwn_core/types/nodeProperty.hpp"

#include <core/object/ref_counted.h>
#include <scene/main/node.h>

#include <vector>

class NodePath;
class LineEdit;

namespace bwn
{
	class ConsoleLineEdit;
	class ConsoleItemSelector;

	class ConsoleLineHistory : public Node
	{
		GDCLASS(ConsoleLineHistory, Node);
		//
		// Construction and destruction.
		//
	public:
		ConsoleLineHistory();
		virtual ~ConsoleLineHistory() override;

		//
		// Godot methods.
		//
	private:
		static void _bind_methods();
		void _notification(const int _notification);

		//
		// Public interface.
		//
	public:
		// Setter and getter for the item selector.
		void setItemSelectorPath(const NodePath& _path);
		const NodePath& getItemSelectorPath() const;

		//
		// Private methods.
		//
	private:
		// Updates line edit the history will be applied to.
		void updateLineEdit();
		// Completely clears parent line edit variable.
		void clearLineEdit();

		// Basically only to interapt events about fetching commands from history.
		void input(const Ref<InputEvent>& _event) override;
		// Just another callback from console line edit. For history this is the same as onTextChanged.
		void onConsoleTextInteraction(const String& _string, const StringId _interactionId);
		// Tries to push last command into the history.
		void pushIntoHistory(const String& _command);
		// Tries to move history current index up.
		void moveHistoryUp();
		// Tries to move history down.
		void moveHistoryDown();

		// This callback will be sent by the item selector on any variant being accepted.
		static void variantAcceptedCallback(Object& _this, ConsoleItemSelector& _itemSelector, const String& _variant);

		//
		// Private members.
		//
	private:
		// The selector used to display history in more conveniently.
		NodeProperty<ConsoleItemSelector, ConsoleLineHistory, void> m_itemSelector;
		// This is just a cache, used to not reallocate variants too often.
		std::vector<String> m_tempVariants;
		// Actual line edit node.
		ConsoleLineEdit* m_parentLineEdit = nullptr;
		// History of commands.
		std::vector<String> m_history;
		// This is basically the temporary command which wasn't pushed to history, but which still can be through scroling.
		String m_tempCommand;
		// Current history index. Can be equal to history count 
		size_t m_historyIndex = 0;
	};
} // namespace bwn