#include "precompiled_console.hpp"

#include "bwn_console/controls/consoleMethodHint.hpp"
#include "bwn_console/controls/consoleLineEdit.hpp"

bwn::ConsoleMethodHint::ConsoleMethodHint()
	: m_parentLineEdit( nullptr )
	, m_hintLabel( BWN_ONLY_IN_DEBUG("hint label") )
{}

bwn::ConsoleMethodHint::~ConsoleMethodHint() = default;

void bwn::ConsoleMethodHint::_bind_methods()
{
	BWN_BIND_PROPERTY(
		"hint_label",
		&ConsoleMethodHint::setHintLabelPath,
		&ConsoleMethodHint::getHintLabelPath,
		PROPERTY_HINT_NODE_PATH_VALID_TYPES,
		RichTextLabel::get_class_static());
}

void bwn::ConsoleMethodHint::_notification(const int _notification)
{
	switch (_notification)
	{
		case NOTIFICATION_ENTER_TREE:
		{
			m_hintLabel.onEnterTree(*this);
			updateHint();
		}
		break;

		case NOTIFICATION_PARENTED:
		{
			updateParentLineEdit();
		}
		break;

		case NOTIFICATION_UNPARENTED:
		{
			clearParentLineEdit();
		}
		break;

		default:
			break;
	}
}

void bwn::ConsoleMethodHint::setHintLabelPath(const NodePath& _path)
{
	m_hintLabel.setNode(*this, _path);
	updateHint();
}

const NodePath& bwn::ConsoleMethodHint::getHintLabelPath() const
{
	return m_hintLabel.getPath();
}

void bwn::ConsoleMethodHint::updateParentLineEdit()
{
	clearParentLineEdit();
	m_parentLineEdit = Object::cast_to<ConsoleLineEdit>(get_parent());

	if (m_parentLineEdit != nullptr)
	{
		m_parentLineEdit->connect("cursor_position_changed",  callable_mp(this, &ConsoleMethodHint::onCursorPositionChanged));
		m_parentLineEdit->connect("console_text_interaction",  callable_mp(this, &ConsoleMethodHint::onConsoleTextInteraction));
		updateHint();
	}
}

void bwn::ConsoleMethodHint::clearParentLineEdit()
{
	if (m_parentLineEdit != nullptr)
	{
		m_parentLineEdit->disconnect("cursor_position_changed", callable_mp(this, &ConsoleMethodHint::onCursorPositionChanged));
		m_parentLineEdit->disconnect("console_text_interaction", callable_mp(this, &ConsoleMethodHint::onConsoleTextInteraction));
	}

	m_parentLineEdit = nullptr;

	clearHint();
}

void bwn::ConsoleMethodHint::onConsoleTextInteraction(const String&, const StringId)
{
	updateHint();
}

void bwn::ConsoleMethodHint::onCursorPositionChanged(const uint32_t)
{
	updateHint();
}

void bwn::ConsoleMethodHint::updateHint()
{
	if (!m_hintLabel.isValid())
	{
		return;
	}

	clearHint();

	if (m_parentLineEdit == nullptr)
	{
		return;
	}

	const ConsoleDatabase::MethodData& methodData = m_parentLineEdit->getConsoleMethodData();
	if (!methodData.valid())
	{
		return;
	}

	m_hintLabel->add_text(bwn::format("command: {}", methodData.getName()));
	const uint32_t activeArgumentNumber = m_parentLineEdit->getCursorContext().localArgumentNumber;

	String activeArgumentDescription;
	uint32_t argumentNumber = 1;
	const String spaceString = " ";
	for (const ConsoleDatabase::ArgData& argumentData : methodData.getArgInfos())
	{
		const String& argumentDefault = argumentData.getDefault();
		const String& argumentName = argumentData.getName();

		if (argumentNumber == activeArgumentNumber)
		{
			m_hintLabel->push_color(Color(0.2f, 1.0f, 0.2f));

			const String& argumentDescription = argumentData.getDescription();
			if (!argumentDescription.is_empty())
			{
				activeArgumentDescription = !argumentName.is_empty()
					? bwn::format("{}: {}", argumentName, argumentDescription)
					: bwn::format("argument{}: {}", argumentNumber, argumentDescription);
			}
		}

		m_hintLabel->add_text(spaceString);

		m_hintLabel->push_underline();

		m_hintLabel->add_text(!argumentName.is_empty()
			? bwn::format("{}", argumentName)
			: bwn::format("argument{}", argumentNumber));

		m_hintLabel->pop();

		if (!argumentDefault.is_empty())
		{
			m_hintLabel->add_text(bwn::format("['{}']", argumentDefault));
		}

		if (argumentNumber == activeArgumentNumber)
		{
			m_hintLabel->pop();
		}

		++argumentNumber;
	}

	if (!activeArgumentDescription.is_empty())
	{
		m_hintLabel->add_newline();
		m_hintLabel->add_text(activeArgumentDescription);
	}

	m_hintLabel->set_fit_content(true);
}

void bwn::ConsoleMethodHint::clearHint()
{
	if (m_hintLabel.isValid())
	{
		m_hintLabel->clear();
		m_hintLabel->set_fit_content(false);
	}
}