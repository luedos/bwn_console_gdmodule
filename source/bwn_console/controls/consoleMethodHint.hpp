#pragma once

#include "bwn_core/types/nodeProperty.hpp"

#include <scene/main/node.h>

class RichTextLabel;

namespace bwn
{
	class ConsoleLineEdit;

	class ConsoleMethodHint : public Node
	{
		GDCLASS(ConsoleMethodHint, Node);

		//
		// Construction and destruction.
		//
	public:
		ConsoleMethodHint();
		virtual ~ConsoleMethodHint() override;

		//
		// Godot methods.
		//
	private:
		static void _bind_methods();
		void _notification(const int _notification);

		//
		// Public interface.
		//
	public:
		// Setter/Getter for the hint label.
		void setHintLabelPath(const NodePath& _path);
		const NodePath& getHintLabelPath() const;

		//
		// Private methods.
		//
	private:
		// Clears current line edit, and sets it as a parent of this node.
		void updateParentLineEdit();
		// Clears current line edit, and unsubscribes from it's signals.
		void clearParentLineEdit();
		// Those two callbacks simply updates the hint.
		void onConsoleTextInteraction(const String&, const StringId);
		void onCursorPositionChanged(const uint32_t);
		// Updates the hint label, based on the current method data from console line edit.
		void updateHint();
		// Clears current hint.
		void clearHint();

		//
		// Private members.
		//
	private:
		// The line edit used to retrieve signals and data about currently edited console method.
		ConsoleLineEdit* m_parentLineEdit = nullptr;
		// The label where we are displaying our hint about currently edited method.
		NodeProperty<RichTextLabel, ConsoleMethodHint, void> m_hintLabel;
	};
} // namespace bwn
