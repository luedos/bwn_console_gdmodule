#include "precompiled_console.hpp"

#include "bwn_console/controls/consoleRoot.hpp"
#include "bwn_console/controls/consoleLineEdit.hpp"

bwn::ConsoleRoot::ConsoleRoot()
	: m_lineEdit( BWN_ONLY_IN_DEBUG("line edit") )
{}

bwn::ConsoleRoot::~ConsoleRoot() = default;

void bwn::ConsoleRoot::_bind_methods()
{
	bwn::bindMethod(D_METHOD("show_console"), &ConsoleRoot::showConsole);
	bwn::bindMethod(D_METHOD("hide_console"), &ConsoleRoot::hideConsole);

	BWN_BIND_PROPERTY(
		"line_edit",
		&ConsoleRoot::setLineEditPath,
		&ConsoleRoot::getLineEditPath,
		PROPERTY_HINT_NODE_PATH_VALID_TYPES,
		"ConsoleLineEdit");
}

void bwn::ConsoleRoot::_notification(const int _notification)
{
	switch (_notification)
	{
		case NOTIFICATION_ENTER_CANVAS:
		{
			m_lineEdit.onEnterTree(*this);
			initSingleton(this);
		}
		break;

		case NOTIFICATION_EXIT_CANVAS:
		{
			clearSingletonSafe(this);
		}
		break;
	}
}

void bwn::ConsoleRoot::setLineEditPath(const NodePath& _path)
{
	m_lineEdit.setNode(*this, _path);
}

const NodePath& bwn::ConsoleRoot::getLineEditPath() const
{
	return m_lineEdit.getPath();
}

void bwn::ConsoleRoot::showConsole()
{
	show();

	if (ConsoleLineEdit*const lineEdit = m_lineEdit.getNode())
	{
		lineEdit->grabFocus();
	}
}

void bwn::ConsoleRoot::hideConsole()
{
	if (ConsoleLineEdit*const lineEdit = m_lineEdit.getNode())
	{
		lineEdit->releaseFocus();
	}

	hide();
}