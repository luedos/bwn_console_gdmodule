#pragma once

#include "bwn_core/types/singleton.hpp"
#include "bwn_core/types/nodeProperty.hpp"

#include <scene/gui/control.h>

namespace bwn
{
	class ConsoleLineEdit;

	class ConsoleRoot : public Control, public PlainSingleton<ConsoleRoot>
	{
		GDCLASS(ConsoleRoot, Control)

		//
		// Construction and destruction.
		//
	public:
		ConsoleRoot();
		virtual ~ConsoleRoot() override;

		//
		// Godot methods.
		//
	private:
		static void _bind_methods();
		void _notification(const int _notification);

		//
		// Public interface.
		//
	public:
		// Setter for the line edit.
		void setLineEditPath(const NodePath& _path);
		// Getter for the line edit.
		const NodePath& getLineEditPath() const;
		// Shows console and sets focus on line edit.
		void showConsole();
		// Simply hides console.
		void hideConsole();

		//
		// Private members.
		//
	private:
		NodeProperty<ConsoleLineEdit, ConsoleRoot, void> m_lineEdit;
	};
} // namespace bwn
