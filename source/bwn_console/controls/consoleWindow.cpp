#include "precompiled_console.hpp"

#include "bwn_console/controls/consoleWindow.hpp"

// This is mostly just so Logger will automatically format error for us in the style of godot itself.
struct bwn::ConsoleWindow::ConsoleLogger final : public Logger
{
	virtual void logv(const char *_format, va_list _list, bool _err) override final _PRINTF_FORMAT_ATTRIBUTE_2_0 ;

	ConsoleWindow* m_self;
};

void bwn::ConsoleWindow::ConsoleLogger::logv(const char *_format, va_list _list, bool _err)
{
	m_self->print(str_format(_format, _list), _err);
}

bwn::ConsoleWindow::ConsoleWindow()
	: m_window( BWN_ONLY_IN_DEBUG("console window") )
{
	m_printHandler.printfunc = printLineHandler;
	m_printHandler.userdata = this;

	m_errorHandler.errfunc = printErrorHandler;
	m_errorHandler.userdata = this;
}
bwn::ConsoleWindow::~ConsoleWindow() = default;

void bwn::ConsoleWindow::_bind_methods()
{
	BWN_BIND_PROPERTY(
		"console_window",
		&ConsoleWindow::setWindowPath,
		&ConsoleWindow::getWindowPath,
		PROPERTY_HINT_NODE_PATH_VALID_TYPES,
		RichTextLabel::get_class_static());
}

void bwn::ConsoleWindow::_notification(const int _notification)
{
	switch (_notification)
	{
		case NOTIFICATION_ENTER_TREE:
		{
			m_window.onEnterTree(*this);

			add_print_handler(&m_printHandler);
			add_error_handler(&m_errorHandler);
		}
		break;

		case NOTIFICATION_EXIT_TREE:
		{
			remove_print_handler(&m_printHandler);
			remove_error_handler(&m_errorHandler);
		}
		break;

		default:
		break;
	}
}

void bwn::ConsoleWindow::setWindowPath(const NodePath& _path)
{
	m_window.setNode(*this, _path);
}
const NodePath& bwn::ConsoleWindow::getWindowPath() const
{
	return m_window.getPath();
}

void bwn::ConsoleWindow::print(const String& _text, const bool _error)
{
	if (RichTextLabel*const window = m_window.getNode())
	{
		if (_error)
		{
			window->push_color(Color(0.8f, 0.3f, 0.3f));
		}

		window->add_text(_text);

		if (_error)
		{
			window->pop();
		}
	}
}
void bwn::ConsoleWindow::newLine()
{
	if (RichTextLabel*const window = m_window.getNode())
	{
		window->add_newline();
	}
}

void bwn::ConsoleWindow::printLineHandler(
	void*const _this,
	const String &_string,
	const bool _error,
	const bool _rich)
{
	ConsoleWindow*const castedThis = reinterpret_cast<ConsoleWindow*>(_this);
	castedThis->print(_string, _error);
	castedThis->newLine();
}
void bwn::ConsoleWindow::printErrorHandler(
	void*const _this, 
	const char*const _function, 
	const char*const _file, 
	const int _line, 
	const char*const _error, 
	const char*const _message,
	const bool _editor_notify,
	ErrorHandlerType _type)
{
	ConsoleWindow*const castedThis = reinterpret_cast<ConsoleWindow*>(_this);
	ConsoleLogger logger;
	logger.m_self = castedThis;
	logger.log_error(_function, _file, _line, _error, _message, (Logger::ErrorType)_type);
}
