#pragma once

#include "bwn_core/types/nodeProperty.hpp"

#include <scene/gui/control.h>

#include <core/io/logger.h>

class RichTextLabel;

namespace bwn
{
	class ConsoleWindow : public Control
	{
		GDCLASS(ConsoleWindow, Control);

		//
		// Private classes.
		//
	private:
		struct ConsoleLogger;

		//
		// Construction and destruction.
		//
	public:
		ConsoleWindow();
		virtual ~ConsoleWindow() override;

		//
		// Godot methods.
		//
	public:
		static void _bind_methods();
		void _notification(const int _notification);

		//
		// Public interface.
		//
	public:
		// Godot setter/getter for console window (Label).
		void setWindowPath(const NodePath& _path);
		const NodePath& getWindowPath() const;

		//
		// Private methods.
		//
	private:
		void print(const String& _text, const bool _error);
		void newLine();

		static void printLineHandler(
			void*const _this,
			const String &_string,
			const bool _error,
			const bool _rich);
		static void printErrorHandler(
			void*const _this,
			const char*const _function,
			const char*const _file,
			const int _line,
			const char*const _error,
			const char*const _message,
			const bool _editor_notify,
			ErrorHandlerType _type);

		//
		// Private members.
		//
	private:
		// Window of the console commands.
		NodeProperty<RichTextLabel, ConsoleWindow, void> m_window;
		PrintHandlerList m_printHandler;
		ErrorHandlerList m_errorHandler;
	};
} // namespace bwn