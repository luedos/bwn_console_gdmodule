#include "precompiled_console.hpp"

#include "bwn_console/debugMethods.hpp"
#include "bwn_console/consoleDatabase.hpp"
#include "bwn_core/debug/assertManager.hpp"

#if defined(DEBUG_ENABLED)

static bool parseErrorGroup(const bwn::string_view& _groupName, ErrorHandlerType& o_errorGroup)
{
	const auto caseInsensitiveCompare = [](const bwn::string_view& _left, const bwn::string_view& _right)
	{
		return std::equal(
			_left.begin(),
			_left.end(),
			_right.begin(),
			_right.end(),
			[](const wchar_t _leftChar, const wchar_t _rightChar)
			{
				return std::towupper(_leftChar) == std::towupper(_rightChar);
			});
	};

	if (caseInsensitiveCompare(_groupName, U"ERROR"))
	{
		o_errorGroup = ErrorHandlerType::ERR_HANDLER_ERROR;
		return true;
	}
	else if (caseInsensitiveCompare(_groupName, U"WARNING"))
	{
		o_errorGroup = ErrorHandlerType::ERR_HANDLER_WARNING;
		return true;
	}
	else if (caseInsensitiveCompare(_groupName, U"SCRIPT"))
	{
		o_errorGroup = ErrorHandlerType::ERR_HANDLER_SCRIPT;
		return true;
	}
	else if (caseInsensitiveCompare(_groupName, U"SHADER"))
	{
		o_errorGroup = ErrorHandlerType::ERR_HANDLER_SHADER;
		return true;
	}

	return false;
}

void bwn::registerConsoleDebugMethods()
{
	const auto assertGroupsVariants = 
		[](const bwn::string_view& _arg, std::vector<String>& _variants)
		{
			bwn::pushIfConsoleMatch(_arg, U"ERROR", _variants);
			bwn::pushIfConsoleMatch(_arg, U"WARNING", _variants);
			bwn::pushIfConsoleMatch(_arg, U"SCRIPT", _variants);
			bwn::pushIfConsoleMatch(_arg, U"SHADER", _variants);
		};

	ConsoleDatabase::getInstance()->addMethod(
		U"enable_assert_group", 
		bwn::wrap_callback([](const String& _groupName)
		{
			ErrorHandlerType groupType;
			if (!::parseErrorGroup(bwn::wrap_view(_groupName), groupType))
			{
				::print_error(bwn::format("Unknown assert group '{}'.", _groupName));
				return;
			}

			AssertManager::getInstance()->switchErrorHandler(groupType, true);
		})
	).argName<0>(U"group").argVariants<0>(assertGroupsVariants);
	
	ConsoleDatabase::getInstance()->addMethod(
		U"disable_assert_group", 
		bwn::wrap_callback([](const String& _groupName)
		{
			ErrorHandlerType groupType;
			if (!::parseErrorGroup(bwn::wrap_view(_groupName), groupType))
			{
				::print_error(bwn::format("Unknown assert group '{}'.", _groupName));
				return;
			}

			AssertManager::getInstance()->switchErrorHandler(groupType, false);
		})
	).argName<0>(U"group").argVariants<0>(assertGroupsVariants);

	ConsoleDatabase::getInstance()->addMethod(
		U"enable_assert_trap", 
		bwn::wrap_callback(
			[]()
			{ 
				debug::setTrapEnabled(true);
				::print_line(U"Enabled traps as crashes.");
			}));
	ConsoleDatabase::getInstance()->addMethod(
		U"disable_assert_trap", 
		bwn::wrap_callback(
			[]()
			{ 
				debug::setTrapEnabled(false);
				::print_line(U"Enabled traps as ERROR asserts.");
			}));
}

void bwn::unregisterConsoleDebugMethods()
{
	ConsoleDatabase::getInstance()->removeMethod(BWN_STRING_ID( "enable_assert_group" ));
	ConsoleDatabase::getInstance()->removeMethod(BWN_STRING_ID( "disable_assert_group" ));
	ConsoleDatabase::getInstance()->removeMethod(BWN_STRING_ID( "enable_assert_trap" ));
	ConsoleDatabase::getInstance()->removeMethod(BWN_STRING_ID( "disable_assert_trap" ));
}
#endif // DEBUG_ENABLED