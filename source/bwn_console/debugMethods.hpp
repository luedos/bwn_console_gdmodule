#pragma once

namespace bwn
{
#if defined(DEBUG_ENABLED)
	void registerConsoleDebugMethods();
	void unregisterConsoleDebugMethods();
#endif // DEBUG_ENABLED
} // namespace bwn