#include "precompiled_console.hpp"

#include "bwn_console/parser/argumentInserter.hpp"
#include "bwn_console/parser/commandsContext.hpp"

_FORCE_INLINE_ const bwn::string_view& bwn::console::ArgumentInserter::Replacement::getReplacementPrefix() const
{
	return m_replacementPrefix;
}

_FORCE_INLINE_ const bwn::string_view& bwn::console::ArgumentInserter::Replacement::getReplacementString() const
{
	return m_replacement;
}

_FORCE_INLINE_ const bwn::string_view& bwn::console::ArgumentInserter::Replacement::getReplacementPostfix() const
{
	return m_replacementPostfix;
}

_FORCE_INLINE_ size_t bwn::console::ArgumentInserter::Replacement::getSize() const
{
	return m_replacementPrefix.size() + m_replacement.size() + m_replacementPostfix.size();
}

uint32_t bwn::console::ArgumentInserter::replaceArgument(
	const Replacement& _replacement,
	const bwn::array_view<const Command>& _commands,
	const CursorContext& _cursorContext,
	const bwn::string_view& _oldText,
	String& o_newText)
{
	if (_cursorContext.commandIndex >= _commands.size())
	{
		return k_characterNpos;
	}

	return replaceArgument(
		_replacement,
		_commands[_cursorContext.commandIndex],
		_cursorContext,
		_oldText,
		o_newText);
}

uint32_t bwn::console::ArgumentInserter::replaceArgument(
	const Replacement& _replacement,
	const Command& _command,
	const CursorContext& _cursorContext,
	const bwn::string_view& _oldText,
	String& o_newText)
{
	if (_cursorContext.localArgumentNumber == 0)
	{
		return replaceCommandName(_replacement, _command, _oldText, o_newText);
	}

	const bwn::array_view<const Argument> argumentSpan = _command.computeArgumentSpan(_cursorContext.localArgumentNumber - 1);
	if (argumentSpan.empty())
	{
		// Basically appending to character index without removing anything.
		const uint32_t commandEndPos = std::clamp<uint32_t>(
			_cursorContext.characterIndex,
			_command.getEndPos(),
			static_cast<uint32_t>(_oldText.length()));

		return replaceString(
			commandEndPos,
			commandEndPos,
			_replacement,
			_oldText,
			o_newText);
	}

	return replaceArgumentSpan(_replacement, argumentSpan, _oldText, o_newText);
}

uint32_t bwn::console::ArgumentInserter::replaceCommandName(
	const Replacement& _replacement,
	const Command& _command,
	const bwn::string_view& _oldText,
	String& o_newText)
{
	if (!ArgumentsContext::isValidCommandName(_replacement.getReplacementString()))
	{
		// Invalid command name.
		return k_characterNpos;
	}

	const bwn::string_view oldCommandName = _command.getName();
	const uint32_t startCommandNamePos = _command.getStartPos();
	const uint32_t endCommandNamePos = startCommandNamePos + oldCommandName.length();

	return replaceString(
		startCommandNamePos,
		endCommandNamePos,
		_replacement,
		_oldText,
		o_newText);
}

uint32_t bwn::console::ArgumentInserter::replaceArgumentSpan(
	const Replacement& _replacement,
	const bwn::array_view<const Argument>& _arguments,
	const bwn::string_view& _oldText,
	String& o_newText)
{
	const Token*const startArgumentToken = _arguments.front().begin();
	const Token*const lastArgumentToken =  _arguments.back().last();

	const uint32_t startArgumentPos = startArgumentToken->getStartPos();
	const uint32_t endArgumentPos = lastArgumentToken->getEndPos();

	char32_t quote = '\0';

	// Checking for unquoted string.
	if (_arguments.size() == 1)
	{
		quote = checkUnquotedString(_arguments.front());
	}

	if (quote == '\0' && !checkArgumentValidity(_replacement.getReplacementString()))
	{
		quote = '\"';
	}

	if (quote != '\0')
	{
		return replaceStringWithQuotes(
			startArgumentPos,
			endArgumentPos,
			_replacement,
			quote,
			_oldText,
			o_newText);
	}

	return replaceString(
		startArgumentPos,
		endArgumentPos,
		_replacement,
		_oldText,
		o_newText);
}

char32_t bwn::console::ArgumentInserter::checkUnquotedString(const Argument& _argument)
{
	const Token*const startArgumentToken = _argument.begin();
	const Token*const lastArgumentToken =  _argument.last();

	if (_argument.type() != EArgumentType::k_value
		|| startArgumentToken != lastArgumentToken)
	{
		return '\0';
	}

	const bwn::string_view tokenString = startArgumentToken->getFullString();
	if (tokenString.empty())
	{
		return '\0';
	}

	const char32_t startQuoteChar = tokenString.front();
	if (!TokensContext::isQuoteChar(startQuoteChar))
	{
		return '\0';
	}

	if (tokenString.length() == 1)
	{
		return startQuoteChar;
	}

	const char32_t endQuoteChar = getEndQuote(tokenString);
	return TokensContext::isMatchingQuote(startQuoteChar, endQuoteChar)
		? '\0'
		: startQuoteChar;
}

char32_t bwn::console::ArgumentInserter::getEndQuote(const bwn::string_view& _string)
{
	const char32_t endChar = _string.back();
	if (!TokensContext::isQuoteChar(endChar))
	{
		return '\0';
	}

	uint32_t escapes = 0;
	for (bwn::string_view::const_reverse_iterator charIt = _string.crbegin() + 1; charIt != _string.crend(); ++charIt)
	{
		if (*charIt != '\\')
		{
			break;
		}

		++escapes;
	}

	const bool isEscaped = (escapes % 2) != 0;
	return isEscaped
		? '\0'
		: endChar;
}

bool bwn::console::ArgumentInserter::checkArgumentValidity(const bwn::string_view& _argument)
{
	// This most of the time will be the fastest solution
	if (!std::any_of(_argument.begin(), _argument.end(), TokensContext::isSpecialChar))
	{
		return true;
	}

	m_errors.clear();
	m_tokens.parse(_argument, m_errors);
	const size_t argumentsCount = m_arguments.parseArguments(m_tokens.getTokens(), m_errors);

	return argumentsCount == 1 && m_errors.getErrorCount() == 0;
}

bwn::string_view bwn::console::ArgumentInserter::escapeString(const bwn::string_view& _string)
{
	const auto isSpecialChar =
		[](const char32_t _char)
		{
			return _char == '\"' || _char == '\'' || _char == '\\';
		};

	if (!std::any_of(_string.begin(), _string.end(), isSpecialChar))
	{
		return _string;
	}

	m_buffer.clear();
	m_buffer.reserve(_string.length() + _string.length() / 2);
	for (const char32_t character : _string)
	{
		if (isSpecialChar(character))
		{
			m_buffer.push_back(L'\\');
		}

		m_buffer.push_back(character);
	}

	return m_buffer;
}

uint32_t bwn::console::ArgumentInserter::replaceString(
	const uint32_t _startPos,
	const uint32_t _endPos,
	const Replacement& _replacement,
	const bwn::string_view& _oldText,
	String& o_newText)
{
	o_newText = bwn::concat(
		_oldText.substr(0, _startPos),
		_replacement.getReplacementPrefix(),
		_replacement.getReplacementString(),
		_replacement.getReplacementPostfix(),
		_oldText.substr(_endPos));

	return _startPos + _replacement.getSize();
}

uint32_t bwn::console::ArgumentInserter::replaceStringWithQuotes(
	const uint32_t _startPos,
	const uint32_t _endPos,
	const Replacement& _replacement,
	const char32_t _quotes,
	const bwn::string_view& _oldText,
	String& o_newText)
{
	const char32_t quoteArr[1] { _quotes };
	const bwn::string_view quoteString = bwn::string_view( quoteArr, 1 );

	o_newText = bwn::concat(
		_oldText.substr(0, _startPos),
		_replacement.getReplacementPrefix(),
		quoteString,
		_replacement.getReplacementString(),
		quoteString,
		_replacement.getReplacementPostfix(),
		_oldText.substr(_endPos));

	return _startPos + _replacement.getSize() + 2;
}