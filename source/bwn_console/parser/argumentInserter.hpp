#pragma once

#include "bwn_core/types/stringView.hpp"

#include "bwn_console/parser/tokensContext.hpp"
#include "bwn_console/parser/argumentsContext.hpp"
#include "bwn_console/parser/errorsContext.hpp"

#include <string>
#include <limits>

namespace bwn::console
{
	//
	// Forward declaration.
	//
	class Command;

	class ArgumentInserter
	{
		//
		// Public constance.
		//
	public:
		static constexpr uint32_t k_characterNpos = std::numeric_limits<uint32_t>::max();

		//
		// Public structs.
		//
	public:
		class Replacement
		{
			//
			// Construction and destruction.
			//
		public:
			Replacement(const bwn::string_view& _replacement)
				: m_replacementPrefix()
				, m_replacement( _replacement )
				, m_replacementPostfix()
			{}

			Replacement(const bwn::string_view& _prefix, const bwn::string_view& _replacement, const bwn::string_view& _postfix)
				: m_replacementPrefix( _prefix )
				, m_replacement( _replacement )
				, m_replacementPostfix( _postfix )
			{}

			//
			// Public interface.
			//
		public:
			// Returns the replacement prefix string.
			_FORCE_INLINE_ const bwn::string_view& getReplacementPrefix() const;
			// Returns the actual replacement for the argument,
			_FORCE_INLINE_ const bwn::string_view& getReplacementString() const;
			// Returns the replacement postfix string.
			_FORCE_INLINE_ const bwn::string_view& getReplacementPostfix() const;

			// Returns the total replacement size.
			_FORCE_INLINE_ size_t getSize() const;

			//
			// Private members.
			//
		private:
			// The string which will be inserted before the argument (before the quotes for example).
			bwn::string_view m_replacementPrefix;
			// The string which will be inserted instead of the argument.
			bwn::string_view m_replacement;
			// The string which will be inserted after the argument (after the quotes for example).
			bwn::string_view m_replacementPostfix;
		};

		//
		// Construction and destruction.
		//
	public:
		ArgumentInserter() = default;

		//
		// Public interface.
		//
	public:
		// Replaces specific argument from specific parser.
		// Returns end position of the new inserted argument and new composed string for that specific parser.
		// If insertion failed, returns k_characterNpos.
		uint32_t replaceArgument(
			const Replacement& _replacement,
			const bwn::array_view<const Command>& _commands,
			const CursorContext& _cursorContext,
			const bwn::string_view& _oldText,
			String& o_newText);
		// Replaces specific argument from specific parser.
		// Returns end position of the new inserted argument and new composed string for that specific parser.
		// If insertion failed, returns k_characterNpos.
		uint32_t replaceArgument(
			const Replacement& _replacement,
			const Command& _command,
			const CursorContext& _cursorContext,
			const bwn::string_view& _oldText,
			String& o_newText);

		//
		// Private methods.
		//
	private:
		// Replaces the command name of specific argument. Returns end position of the new inserted argument.
		uint32_t replaceCommandName(
			const Replacement& _replacement,
			const Command& _command,
			const bwn::string_view& _oldText,
			String& o_newText);
		// Replaces specific arguments span. Returns end position of the new inserted argument.
		uint32_t replaceArgumentSpan(
			const Replacement& _replacement,
			const bwn::array_view<const Argument>& _arguments,
			const bwn::string_view& _oldText,
			String& o_newText);
		// Ok, this is little bit of a mess, but what we actually checking is if the current argument represents unquoted string,
		// and if it is, we are returning the quote to use. If argument is not a string, or if it's quoted properly, this function will return '\0'.
		static char32_t checkUnquotedString(const Argument& _argument);
		// Returns the end quote from the string if there is one.
		static char32_t getEndQuote(const bwn::string_view& _string);
		// Checks if argument valid. Uses internal state variables for that.
		bool checkArgumentValidity(const bwn::string_view& _argument);
		// Escapes string if needed. If no escaping required returns string_view back unchanged.
		// If escaping is required, stores new string in internal state buffer.
		bwn::string_view escapeString(const bwn::string_view& _string);
		// Replaces the specific part of the string and re-parses it.
		static uint32_t replaceString(
			const uint32_t _startPos,
			const uint32_t _endPos,
			const Replacement& _replacement,
			const bwn::string_view& _oldText,
			String& o_newText);
		// Replaces the specific part of the string surrounding it with quotes, and re-parses it.
		static uint32_t replaceStringWithQuotes(
			const uint32_t _startPos,
			const uint32_t _endPos,
			const Replacement& _replacement,
			const char32_t _quotes,
			const bwn::string_view& _oldText,
			String& o_newText);

		//
		// Private members.
		//
	private:
		// Those are mostly temporary values, used to parse argument to check it's validity.
		TokensContext m_tokens;
		ArgumentsContext m_arguments;
		ErrorsContext m_errors;
		std::basic_string<char32_t> m_buffer;
	};

} // namespace bwn::console