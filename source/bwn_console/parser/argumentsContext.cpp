#include "precompiled_console.hpp"

#include "bwn_console/parser/argumentsContext.hpp"
#include "bwn_console/parser/tokensContext.hpp"
#include "bwn_console/parser/errorsContext.hpp"

bwn::console::Argument::Argument(const Token*const _tokens)
	: m_tokens( _tokens )
	, m_tokensCount( 0 )
	, m_leadingArguments( 0 )
	, m_type( EArgumentType::k_invalid )
{
	BWN_TRAP_COND(m_tokens != nullptr, "Even if argument doesn't have any tokens, it still must point to some point in token array.");
}

bwn::console::Argument::Argument(const Token& _token, const EArgumentType _type)
	: m_tokens( &_token )
	, m_tokensCount( 1 )
	, m_leadingArguments( 0 )
	, m_type( _type )
{}

bwn::console::Argument::Argument(
	const Token*const _tokens,
	const uint32_t _tokensLength,
	const uint16_t _leadingArgumentCount,
	const EArgumentType _type)
	: m_tokens( _tokens )
	, m_tokensCount( _tokensLength )
	, m_leadingArguments( _leadingArgumentCount )
	, m_type( _type )
{
	BWN_TRAP_COND(m_tokens != nullptr, "Even if argument doesn't have any tokens, it still must point to some point in token array.");
}

uint32_t bwn::console::Argument::getStartPos() const
{
	return m_tokens->getStartPos();
}

uint32_t bwn::console::Argument::getEndPos() const
{
	return m_tokensCount != 0
		? (m_tokens + (m_tokensCount - 1))->getEndPos()
		: m_tokens->getStartPos();
}

const bwn::console::Token* bwn::console::Argument::begin() const
{
	return m_tokens;
}

const bwn::console::Token* bwn::console::Argument::end() const
{
	return m_tokens + m_tokensCount;
}

const bwn::console::Token* bwn::console::Argument::last() const
{
	return m_tokensCount != 0
		? m_tokens + (m_tokensCount - 1)
		: m_tokens;
}

bwn::console::EArgumentType bwn::console::Argument::type() const
{
	return m_type;
}

uint16_t bwn::console::Argument::getLeadingCount() const
{
	return m_leadingArguments;
}

void bwn::console::Argument::changeSize(const uint32_t _newTokensCount, const uint16_t _newLeadingCount)
{
	m_tokensCount = _newTokensCount;
	m_leadingArguments = _newLeadingCount;
}

void bwn::console::Argument::changeType(const EArgumentType _newType)
{
	m_type = _newType;
}

const char* bwn::console::Argument::typeToString(const EArgumentType _type)
{
	switch (_type)
	{
		case EArgumentType::k_invalid: return "k_invalid";
		case EArgumentType::k_openMap: return "k_openMap";
		case EArgumentType::k_openArray: return "k_openArray";
		case EArgumentType::k_map: return "k_map";
		case EArgumentType::k_mapKey: return "k_mapKey";
		case EArgumentType::k_array: return "k_array";
		case EArgumentType::k_value: return "k_value";
		case EArgumentType::k_command: return "k_command";
	}

	return "unknown";
}

class bwn::console::ArgumentsContext::InnerContext
{
	//
	// Construction and destruction.
	//
public:
	InnerContext(const bwn::array_view<const Token>& _tokens, ErrorsContext& _errors)
		: m_tokens( _tokens )
		, m_errors( &_errors )
	{}

	//
	// Public interface.
	//
public:
	// Returns the start of the tokens array.
	const Token* begin() const
	{
		return m_tokens.begin();
	}
	// Returns the end of the tokens array.
	const Token* end() const
	{
		return m_tokens.end();
	}
	// Pushes an error.
	void pushError(const EErrorType _type, const Token& _startToken) const
	{
		m_errors->pushError(_type, _startToken.getStartPos());
	}

	//
	// Private members.
	//
private:
	// Array of tokens.
	bwn::array_view<const Token> m_tokens;
	// Error context to push errors.
	ErrorsContext* m_errors;
};

bwn::console::ArgumentsContext::ArgumentsContext()
	: m_arguments()
{}

void bwn::console::ArgumentsContext::parseCommands(
	const bwn::array_view<const Token>& _tokens,
	ErrorsContext& _errors)
{
	m_arguments.clear();

	if (_tokens.empty())
	{
		return;
	}

	const InnerContext context = InnerContext(_tokens, _errors);
	const Token* tokenIt = _tokens.begin();
	const Token*const endIt = _tokens.end();

	while (tokenIt < endIt && tokenIt->type() == ETokenType::k_commandSeparator)
	{
		// Pushing empty arguments and errors for all commands separators without any commands in them.
		Argument& arg = m_arguments.emplace_back(*tokenIt, EArgumentType::k_command);
		arg.changeSize(0, 0);
		context.pushError(EErrorType::k_missingCommandName, *tokenIt);
	}

	// First of all we are parsing all the arguments.
	while (tokenIt < endIt)
	{
		const size_t newArgumentId = m_arguments.size();

		// Checking first token to be valid command name.
		if (tokenIt->type() == ETokenType::k_string && isValidCommandName(tokenIt->getFullString()))
		{
			m_arguments.emplace_back(*tokenIt, EArgumentType::k_command);
			++tokenIt;
		}
		else
		{
			// We always push argument at the start of the command, but if token count is 0, that's mean this command doesn't have a name.
			Argument& arg = m_arguments.emplace_back(*tokenIt, EArgumentType::k_command);
			arg.changeSize(0, 0);

			context.pushError(EErrorType::k_missingCommandName, *tokenIt);
		}

		const size_t argCount = parseCommandArguments(tokenIt, context);
		Argument& arg = m_arguments[newArgumentId];
		arg.changeSize(std::distance(arg.begin(), arg.end()), argCount);

		if (tokenIt != endIt && tokenIt->type() == ETokenType::k_commandSeparator)
		{
			++tokenIt;
		}
	}
}

size_t bwn::console::ArgumentsContext::parseArguments(
	const bwn::array_view<const Token>& _tokens,
	ErrorsContext& _errors)
{
	m_arguments.clear();

	const InnerContext context = InnerContext(_tokens, _errors);

	const Token* tokenIt = _tokens.begin();
	const Token*const endIt = _tokens.end();

	size_t argumentCount = 0;
	while (tokenIt < endIt)
	{
		// simply skipping separators because we are not really having any commands.
		if (tokenIt->type() == ETokenType::k_commandSeparator)
		{
			context.pushError(EErrorType::k_globalCommandSeparator, *tokenIt);
			++tokenIt;
			continue;
		}

		tokenIt = parseSingleArgument(tokenIt, context);
		++argumentCount;
	}

	return argumentCount;
}

const bwn::console::Token* bwn::console::ArgumentsContext::parseSingleArgument(
	const bwn::array_view<const Token>& _tokens,
	ErrorsContext& _errors)
{
	const InnerContext context = InnerContext(_tokens, _errors);
	return parseSingleArgument(_tokens.begin(), context);
}

const std::vector<bwn::console::Argument>& bwn::console::ArgumentsContext::getArguments() const
{
	return m_arguments;
}

void bwn::console::ArgumentsContext::clear()
{
	m_arguments.clear();
}

bwn::console::CursorContext bwn::console::ArgumentsContext::composeContext(const uint32_t _characterPos) const
{
	return composeContext(_characterPos, m_arguments);
}

bwn::SystemResult bwn::console::ArgumentsContext::formatSingleArgument(
	const bwn::array_view<const Argument>& _arguments,
	Variant& o_value)
{
	if (_arguments.empty())
	{
		o_value = Variant();
		return SystemFacility::createError(ERR_PARSE_ERROR,L"No arguments were provided for formatting.");
	}

	return formatGeneralArgument(_arguments, o_value);
}

const bwn::console::Argument* bwn::console::ArgumentsContext::skipArgument(
	const Argument*const _it,
	const Argument*const _end)
{
	// Each argument basically has an argument count, which consist of the argument itself (1) plus all the arguments which belong to this argument.
	if (_it >= _end)
	{
		return _end;
	}

	const Argument* it = _it;

	size_t skipping = it->getLeadingCount();
	++it;

	while(skipping != 0 && it < _end)
	{
		skipping += it->getLeadingCount();
		--skipping;
		++it;
	}

	return it;
}

bwn::array_view<const bwn::console::Argument> bwn::console::ArgumentsContext::getArgumentFromIndex(
	const size_t _argumentIndex,
	const bwn::array_view<const Argument>& _arguments)
{
	const Argument* argumentIt = _arguments.begin();
	const Argument*const endIt = _arguments.end();
	const Argument* nextIt = ArgumentsContext::skipArgument(argumentIt, endIt);

	for (uint32_t index = 0; index < _argumentIndex && argumentIt != endIt; ++index)
	{
		argumentIt = nextIt;
		nextIt = ArgumentsContext::skipArgument(argumentIt, endIt);
	}

	return bwn::array_view<const Argument>( argumentIt, nextIt );
}

bool bwn::console::ArgumentsContext::isValidCommandName(const bwn::string_view& _name)
{
	if (_name.empty() || std::iswdigit(_name.front()))
	{
		return false;
	}

	for (const char32_t character : _name)
	{
		if (!std::iswalnum(character) && character != '_')
		{
			return false;
		}
	}

	return true;
}

bwn::console::CursorContext bwn::console::ArgumentsContext::composeContext(
	const uint32_t _characterPos,
	const bwn::array_view<const Argument>& _arguments)
{
	if (_arguments.empty())
	{
		return CursorContext();
	}

	CursorContext context;

	const Argument* argumentIt = _arguments.begin();
	const Argument*const endIt = _arguments.end();
	const Argument* nextIt = skipArgument(argumentIt, endIt);

	uint32_t argumentIndex = 0;
	while (nextIt < endIt)
	{
		if (nextIt->begin()->getStartPos() > _characterPos)
		{
			break;
		}

		argumentIt = nextIt;
		nextIt = skipArgument(argumentIt, endIt);
		++argumentIndex;
	}

	context.localArgumentNumber = argumentIndex + 1;
	context.argumentCharacterIndex = argumentIt->begin()->getStartPos();

	const uint32_t minCharacterPos = _arguments.front().begin()->getStartPos();
	if (argumentIt->begin() == argumentIt->end())
	{
		const uint32_t maxCharacterPos = argumentIt->begin()->getStartPos();
		context.characterIndex = std::clamp(_characterPos, minCharacterPos, maxCharacterPos);
		return context;
	}

	const uint32_t argumentEndPos = argumentIt->last()->getEndPos();

	if (argumentEndPos < _characterPos)
	{
		// If we simply overshoot past the last argument, we consider that we are redacting the next (still not existing) argument.
		context.characterIndex = _characterPos;
		context.localArgumentNumber += 1;
		context.argumentCharacterIndex = _characterPos;
		return context;
	}

	context.characterIndex = std::clamp(_characterPos, minCharacterPos, argumentEndPos);
	return context;
}

const bwn::console::Token* bwn::console::ArgumentsContext::parseSingleArgument(
	const Token*const _begin,
	const InnerContext& _context)
{
	BWN_TRAP_COND(_begin < _context.end(), "Inner parse error: parseSingleArgument assumes that _begin and _end are valid by default.");
	BWN_TRAP_COND(_begin->type() != ETokenType::k_commandSeparator, "Inner parse error: parseSingleArgument assumes that it provided with at least one token before command end.");

	switch (_begin->type())
	{
		// All the value types. Invalid token is counts as a value token
		case ETokenType::k_invalid:
		case ETokenType::k_decInteger:
		case ETokenType::k_hexInteger:
		case ETokenType::k_octInteger:
		case ETokenType::k_binInteger:
		case ETokenType::k_float:
		case ETokenType::k_booleanTrue:
		case ETokenType::k_booleanFalse:
		case ETokenType::k_string:
		{
			m_arguments.emplace_back(*_begin, EArgumentType::k_value);
			return _begin + 1;
		}

		case ETokenType::k_openingMap:
		{
			return parseMapArgument(_begin, _context);
		}
		case ETokenType::k_closingMap:
		{
			_context.pushError(EErrorType::k_mapNotOpened, *_begin);
			m_arguments.emplace_back(_begin);
			return _begin + 1;
		}
		case ETokenType::k_equal:
		{
			_context.pushError(EErrorType::k_globalEqual, *_begin);
			m_arguments.emplace_back(_begin);
			return _begin + 1;
		}

		case ETokenType::k_openingArray:
		{
			return parseArrayArgument(_begin, _context);
		}

		case ETokenType::k_closingArray:
		{
			_context.pushError(EErrorType::k_arrayNotClosed, *_begin);
			m_arguments.emplace_back(_begin);
			return _begin + 1;
		}

		default:
		{
			break;
		}
	}

	BWN_TRAP("Internal parsing error: Invalid token type: {}.", Token::typeToString(_begin->type()));
	return _begin + 1;
}

size_t bwn::console::ArgumentsContext::parseCommandArguments(
	const Token*& _it,
	const InnerContext& _context)
{
	size_t commandArgumentsCount = 0;

	const Token* tokenIt = _it;
	const Token*const endIt = _context.end();

	while (tokenIt < endIt)
	{
		if (tokenIt->type() == ETokenType::k_commandSeparator)
		{
			break;
		}

		tokenIt = parseSingleArgument(tokenIt, _context);
		++commandArgumentsCount;
	}

	_it = tokenIt;
	return commandArgumentsCount;
}

const bwn::console::Token* bwn::console::ArgumentsContext::parseArrayArgument(
	const Token*const _begin,
	const InnerContext& _context)
{
	BWN_TRAP_COND(_begin < _context.end(), "Inner parse error: parseArrayArgument assumes that _begin and _end are valid by default.");
	BWN_TRAP_COND(_begin->type() != ETokenType::k_commandSeparator, "Inner parse error: parseArrayArgument assumes that it provided with at least one token before command ends.");
	BWN_ASSERT_ERROR_COND(
		_begin->type() == ETokenType::k_openingArray,
		"Inner parse error: the first type of the array token should be ETokenType::k_openingArray, the function was invoked incorrectly with token by type {}.",
		Token::typeToString(_begin->type()));

	// Pushing empty argument, which we will complete later.
	// We are saving index of the argument, instead of argument reference,
	// because it's possible the m_arguments vector will be expanded in the process of parsing current array,
	// and so reference will be invalidated.
	const size_t newArgumentId = m_arguments.size();
	m_arguments.emplace_back(_begin);

	uint16_t argumentCount = 0;

	// Skiping opening bracket.
	const Token* tokenIt = _begin + 1;
	const Token*const endIt = _context.end();

	while (tokenIt < endIt)
	{
		switch (tokenIt->type())
		{
			// All special types.
			case ETokenType::k_closingArray:
			{
				// Skip closing array.
				++tokenIt;
				// Fetching and filling our array argument.
				Argument& arrayArg = m_arguments[newArgumentId];
				arrayArg.changeType(EArgumentType::k_array);
				arrayArg.changeSize(tokenIt - _begin, argumentCount);

				return tokenIt;
			}

			case ETokenType::k_closingMap:
			case ETokenType::k_commandSeparator:
			{
				goto unfinished;
			}

			default:
			{
				tokenIt = parseSingleArgument(tokenIt, _context);
				++argumentCount;
			}
		}
	}

	// In case we have struck the end of the tokens or the command separator, without actually finishing the array.
unfinished:

	_context.pushError(EErrorType::k_arrayNotClosed, *_begin);

	// We still will fill our argument, but type will set invalid.
	Argument& arrayArg = m_arguments[newArgumentId];
	arrayArg.changeType(EArgumentType::k_invalid);
	arrayArg.changeSize(tokenIt - _begin, argumentCount);

	return tokenIt;
}

const bwn::console::Token* bwn::console::ArgumentsContext::parseMapArgument(
	const Token*const _begin,
	const InnerContext& _context)
{
	BWN_TRAP_COND(_begin < _context.end(), "Inner parse error: parseMapArgument assumes that _begin and _end are valid by default.");
	BWN_TRAP_COND(_begin->type() != ETokenType::k_commandSeparator, "Inner parse error: parseMapArgument assumes that it provided with at least one token before command ends.");
	BWN_ASSERT_ERROR_COND(
		_begin->type() == ETokenType::k_openingMap,
		"Inner parse error: the first type of the map token should be ETokenType::k_openingMap, the function was invoked incorrectly with token by type {}.",
		Token::typeToString(_begin->type()));

	// Pushing empty argument, which we will complete later.
	// We are saving index of the argument, instead of argument reference,
	// because it's possible the m_arguments vector will be expanded in the process of parsing current array,
	// and so reference will be invalidated.
	const size_t newArgumentId = m_arguments.size();
	m_arguments.emplace_back(_begin);

	// Arguments count for the map is basically the combination of the initial map argument, map keys and map values.
	uint16_t argumentCount = 0;

	const auto pushSingleArgument =
		[&argumentCount, this] (const Token*const _begin, EArgumentType _type) -> const Token*
		{
			++argumentCount;
			this->m_arguments.emplace_back(*_begin, _type);
			return _begin + 1;
		};

	// Skipping opening bracket.
	const Token* tokenIt = _begin + 1;
	const Token*const endIt = _context.end();

	// This basically flip flop value which will say what we are parsing right now: key or value.
	bool keyPushed = false;
	bool equalPushed = false;

	while (tokenIt < endIt)
	{
		switch (tokenIt->type())
		{
			// All the value types. Invalid token is counts as a value token
			case ETokenType::k_invalid:
			case ETokenType::k_decInteger:
			case ETokenType::k_hexInteger:
			case ETokenType::k_octInteger:
			case ETokenType::k_binInteger:
			case ETokenType::k_float:
			case ETokenType::k_booleanTrue:
			case ETokenType::k_booleanFalse:
			case ETokenType::k_string:
			{
				// Basically we are checking if we are at key state, or if we are at value state.
				// If key was pushed, but equal wasn't, we have an error.
				if (keyPushed && !equalPushed)
				{
					_context.pushError(EErrorType::k_missingMapKey, *tokenIt);
					// Even if we got error, we still going to push value as an argument.
					equalPushed = true;
				}

				if (!keyPushed)
				{
					tokenIt = pushSingleArgument(tokenIt, EArgumentType::k_mapKey);
					keyPushed = true;
				}
				else
				{
					tokenIt = pushSingleArgument(tokenIt, EArgumentType::k_value);

					// Resetting state.
					keyPushed = false;
					equalPushed = false;
				}

				break;
			}

			case ETokenType::k_closingMap:
			{
				if (keyPushed)
				{
					_context.pushError(EErrorType::k_missingMapValue, *tokenIt);
				}
				// Skip closing bracket.
				++tokenIt;
				// Fetching and filling our map argument.
				Argument& arrayArg = m_arguments[newArgumentId];
				arrayArg.changeType(EArgumentType::k_map);
				arrayArg.changeSize(tokenIt - _begin, argumentCount);

				return tokenIt;
			}

			case ETokenType::k_openingArray:
			{
				// The array and map could only be values.
				if (!keyPushed || !equalPushed)
				{
					_context.pushError(keyPushed ? EErrorType::k_missingMapEqual : EErrorType::k_missingMapKey, *tokenIt);
				}

				// Array recursion.
				tokenIt = parseArrayArgument(tokenIt, _context);
				++argumentCount;

				keyPushed = false;
				equalPushed = false;

				break;
			}

			case ETokenType::k_openingMap:
			{
				// The array and map could only serve as values.
				if (!keyPushed || !equalPushed)
				{
					_context.pushError(keyPushed ? EErrorType::k_missingMapEqual : EErrorType::k_missingMapKey, *tokenIt);
				}

				tokenIt = parseMapArgument(tokenIt, _context);
				++argumentCount;

				keyPushed = false;
				equalPushed = false;

				break;
			}

			case ETokenType::k_equal:
			{
				if (!keyPushed)
				{
					_context.pushError(EErrorType::k_missingMapKey, *tokenIt);
					keyPushed = true;
				}
				if (equalPushed)
				{
					// If equal was already pushed, then we are basically have two equals in a row,
					// so we are missing value from previous key/map pair and key from current one.
					_context.pushError(EErrorType::k_missingMapValue, *tokenIt);
					_context.pushError(EErrorType::k_missingMapKey, *tokenIt);
				}

				++tokenIt;
				equalPushed = true;

				break;
			}

			case ETokenType::k_closingArray:
			case ETokenType::k_commandSeparator:
			{
				goto unfinished;
			}
		}
	}

unfinished:
	_context.pushError(EErrorType::k_mapNotClosed, *tokenIt);

	// We still will fill our argument, but type will set invalid.
	Argument& arrayArg = m_arguments[newArgumentId];
	arrayArg.changeType(EArgumentType::k_invalid);
	arrayArg.changeSize(tokenIt - _begin, argumentCount);

	return tokenIt;
}

bwn::SystemResult bwn::console::ArgumentsContext::formatGeneralArgument(
	const bwn::array_view<const Argument>& _arguments,
	Variant& o_value)
{
	const Argument& argument = _arguments.front();
	switch (argument.type())
	{
		case EArgumentType::k_map:
		{
			return formatMapArgument(_arguments, o_value);
		}
		case EArgumentType::k_array:
		{
			return formatArrayArgument(_arguments, o_value);
		}
		case EArgumentType::k_value:
		{
			return formatValueArgument(_arguments, o_value);
		}

		default:
		{
			break;
		}
	}

	o_value = Variant();
	return SystemFacility::createError(
		ERR_PARSE_ERROR,
		bwn::format(
			"Trying to format argument ('{}' at position {}) with invalid type '{}'.",
			bwn::wrap_utf(argument.begin()->getContentString()),
			argument.begin()->getStartPos(),
			Argument::typeToString(argument.type())));
}

bwn::SystemResult bwn::console::ArgumentsContext::formatValueArgument(
	const bwn::array_view<const Argument>& _arguments,
	Variant& o_value)
{
	const Argument& argument = _arguments.front();
	const Token& token = *argument.begin();

	if (argument.getLeadingCount() != 0)
	{
		o_value = Variant();
		return SystemFacility::createError(
			ERR_PARSE_ERROR,
			bwn::format(
				"Trying to format argument ('{}' at position {}) as value, when this argument has additional {} arguments.",
				bwn::wrap_utf(token.getContentString()),
				token.getStartPos(),
				argument.getLeadingCount()));
	}

	const size_t tokenCount = argument.end() - argument.begin();
	if (tokenCount != 1)
	{
		o_value = Variant();
		return SystemFacility::createError(
			ERR_PARSE_ERROR,
			bwn::format(
				"Trying to format argument ('{}' at position {}) as value, when this argument has {} then 1 token in it ({} tokes in total).",
				bwn::wrap_utf(token.getContentString()),
				token.getStartPos(),
				(tokenCount > 1) ? "more" : "less",
				tokenCount));
	}

	switch (token.type())
	{
		case ETokenType::k_invalid:
		case ETokenType::k_commandSeparator:
		case ETokenType::k_openingMap:
		case ETokenType::k_closingMap:
		case ETokenType::k_equal:
		case ETokenType::k_openingArray:
		case ETokenType::k_closingArray:
		{
			o_value = Variant();
			return SystemFacility::createError(
				ERR_PARSE_ERROR,
				bwn::format(
					"Trying to format argument ('{}' at position {}) as value, when this argument token is not a value (token type: {}).",
					bwn::wrap_utf(token.getContentString()),
					token.getStartPos(),
					Token::typeToString(token.type())));
		}

		case ETokenType::k_decInteger:
		{
			int64_t intValue;
			if (!bwn::asDecimalStrict(token.getContentString(), intValue))
			{
				// Returning with error.
				break;
			}

			o_value = Variant(intValue);
			return SystemFacility::createSuccess();
		}
		case ETokenType::k_hexInteger:
		{
			uint64_t intValue;
			if (!bwn::asHexadecimalStrict(token.getContentString(), intValue))
			{
				// Returning with error.
				break;
			}

			o_value = Variant(intValue);
			return SystemFacility::createSuccess();
		}
		case ETokenType::k_octInteger:
		{
			uint64_t intValue;
			if (!bwn::asOctodecimalStrict(token.getContentString(), intValue))
			{
				// Returning with error.
				break;
			}

			o_value = Variant(intValue);
			return SystemFacility::createSuccess();
		}
		case ETokenType::k_binInteger:
		{
			uint64_t intValue;
			if (!bwn::asBinaryStrict(token.getContentString(), intValue))
			{
				// Returning with error.
				break;
			}

			o_value = Variant(intValue);
			return SystemFacility::createSuccess();
		}
		case ETokenType::k_float:
		{
			double doubleValue;
			if (!bwn::asDoubleStrict(token.getContentString(), doubleValue))
			{
				// Returning with error.
				break;
			}

			o_value = Variant(doubleValue);
			return SystemFacility::createSuccess();
		}
		case ETokenType::k_booleanTrue:
		{
			o_value = Variant(true);
			return SystemFacility::createSuccess();
		}
		case ETokenType::k_booleanFalse:
		{
			o_value = Variant(false);
			return SystemFacility::createSuccess();
		}
		case ETokenType::k_string:
		{
			const bwn::string_view tokenString = token.getContentString();
			o_value = Variant(String(tokenString.data(), tokenString.length()));
			return SystemFacility::createSuccess();
		}
	}

	o_value = Variant();
	return SystemFacility::createError(
		ERR_PARSE_ERROR,
		bwn::format(
			"Trying to format argument ('{}' at position {}) as value failed.",
			bwn::wrap_utf(token.getContentString()),
			token.getStartPos()));
}

bwn::SystemResult bwn::console::ArgumentsContext::formatMapArgument(
	const bwn::array_view<const Argument>& _arguments,
	Variant& o_value)
{
	Variant key;
	Dictionary retDictionary;

	const Argument& startArgument = _arguments.front();
	uint32_t argumentsCount = startArgument.getLeadingCount();
	for (const Argument* argumentIt = _arguments.begin(); argumentIt < _arguments.end() && argumentsCount > 0; --argumentsCount)
	{
		// Basically are we currently formatting the key.
		if (key.get_type() == Variant::NIL)
		{
			if (argumentIt->type() != EArgumentType::k_mapKey)
			{
				o_value = Variant();
				return SystemFacility::createError(
					ERR_PARSE_ERROR,
					bwn::format(
						"Trying to format map key argument ('{}' at position {}), but type of that argument is not k_mapKey (actual type {}).",
						bwn::wrap_utf(argumentIt->begin()->getContentString()),
						argumentIt->begin()->getStartPos(),
						Argument::typeToString(argumentIt->type())));
			}
			if (const SystemResult result = formatValueArgument(_arguments.sub_end(argumentIt), key); !result.success())
			{
				// If returned result is not successful, simply return it.
				o_value = Variant();
				return result;
			}
		}
		else
		{
			Variant value;
			if (const SystemResult result = formatGeneralArgument(_arguments.sub_end(argumentIt), value); !result.success())
			{
				// No reason to push error, formatGeneralArgument probably already did it.
				return result;
			}

			retDictionary[key] = value;
			key = Variant();
		}

		argumentIt = skipArgument(argumentIt, _arguments.end());
	}

	o_value = retDictionary;
	return SystemFacility::createSuccess();
}

bwn::SystemResult bwn::console::ArgumentsContext::formatArrayArgument(
	const bwn::array_view<const Argument>& _arguments,
	Variant& o_value)
{
	Array retArray;

	uint32_t argumentsCount = _arguments.front().getLeadingCount();
	for (const Argument* argumentIt = _arguments.begin() + 1;
		argumentIt < _arguments.end() && argumentsCount > 0;
		--argumentsCount)
	{
		Variant value;
		if (SystemResult result = formatGeneralArgument(_arguments.sub_end(argumentIt), value); !result.success())
		{
			return result;
		}

		argumentIt = skipArgument(argumentIt, _arguments.end());
	}

	o_value = retArray;
	return SystemFacility::createSuccess();
}

