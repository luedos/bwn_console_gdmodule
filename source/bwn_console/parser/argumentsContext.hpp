#pragma once

#include "bwn_console/parser/cursorContext.hpp"

#include "bwn_core/containers/arrayView.hpp"
#include "bwn_core/results/systemResult.hpp"

#include <core/variant/variant.h>

#include <vector>

namespace bwn::console
{
	//
	// Forward declarations.
	//
	class Token;
	class ErrorsContext;

	enum class EArgumentType : uint16_t
	{
		k_invalid,
		// If argument is unclosed (aka invalid) map
		k_openMap,
		// If argument is unclosed (aka invalid) array.
		k_openArray,

		// If argument is an map (will include open/close bracket tokens).
		k_map,
		// If the argument is the key in the map.
		k_mapKey,
		// If argument is an arary (will include open/close bracket tokens).
		k_array,
		// If the argument is general value (could be simple value, or value in map/array)
		k_value,
		// This is basically a special argument which represents an command.
		// This argument must have a token size of 1 and it's token must be a valid command name string.
		k_command
	};

	class Argument
	{
		//
		// Construction and destruction.
		//
	public:
		// Plain constructor from the array of tokens. The type is set as invalid, and tokens length is 0.
		// The array of tokens should not be nullptr (event if length is 0).
		Argument(const Token*const _tokens);
		// Constructor for the single value token (like k_value, k_mapKey etc.)
		Argument(const Token& _token, const EArgumentType _type);
		// Just a general constructor.
		// The array of tokens should not be nullptr (event if length is 0).
		Argument(
			const Token*const _tokens,
			const uint32_t _tokensLength,
			const uint16_t _leadingArgumentCount,
			const EArgumentType _type);

		//
		// Public interface.
		//
	public:
		// Returns the start character position of an argument.
		uint32_t getStartPos() const;
		// Returns the end character position of an argument.
		uint32_t getEndPos() const;
		// Returns the start token iterator (can't be nullptr).
		const Token* begin() const;
		// Returns the end token iterator (can't be nullptr).
		const Token* end() const;
		// Returns last token.
		const Token* last() const;
		// Returns the type of the argument.
		EArgumentType type() const;
		// Returns the number of tokens
		uint16_t getLeadingCount() const;
		// Sometimes it's usefull to change argument postfactum.
		void changeSize(const uint32_t _newTokensCount, const uint16_t _newLeadingCount);
		// Sometimes it's usefull to change argument postfactum.
		void changeType(const EArgumentType _newType);
		// Returns string representation of type. (never returns nullptr).
		static const char* typeToString(const EArgumentType _type);

		//
		// Private members.
		//
	private:
		// Pointer at the start array of tokens. Never nullptr even if tokens count 0.
		const Token* m_tokens;
		// The length of tokens array (can be 0)
		uint32_t m_tokensCount = 0;
		// The actual argument count not including this one.
		// (mostly usefull for arrays/maps/commands, in other argument types this will be 0).
		uint16_t m_leadingArguments = 0;
		// The type of argument.
		EArgumentType m_type = EArgumentType::k_invalid;
	};

	class ArgumentsContext
	{
		//
		// Private classes.
		//
	private:
		class InnerContext;

		//
		// Construction and destruction.
		//
	public:
		ArgumentsContext();

		//
		// Public interface.
		//
	public:
		// Parses tokens into the arguments stack as commands with arguments (aka first argument is 'k_command' and rest is arguments of this command).
		void parseCommands(const bwn::array_view<const Token>& _tokens, ErrorsContext& _errors);
		// Parses tokens into simple arguments (without 'k_command' name). Returns the amount of arguments parsed.
		size_t parseArguments(const bwn::array_view<const Token>& _tokens, ErrorsContext& _errors);
		// Parses a single argument and returns token iterator where the parsing ended.
		// Does not clear arguments stack beforehand, so if you really want single argument, don't forget to call 'clear'.
		// Also, if at the start there is bunch of invalid tokens (like closing map bracket without opening one),
		// will skip them until at least one token could be considered as a argument start.
		const Token* parseSingleArgument(const bwn::array_view<const Token>& _tokens, ErrorsContext& _errors);
		// Returns parsed arguments.
		const std::vector<Argument>& getArguments() const;
		// Clears argument stack.
		void clear();
		// Composes context from parsed arguments.
		CursorContext composeContext(const uint32_t _characterPos) const;
		// Formats the first argument in the array, and returns variant.
		// If formatting failed for some reason, will push error into errors context and return NIL.
		static SystemResult formatSingleArgument(const bwn::array_view<const Argument>& _arguments, Variant& o_value);
		// Skips the first argument in the list (following argument nesting rules) and returns argument iterator right after that first argument.
		static const Argument* skipArgument(const Argument*const _it, const Argument*const _end);
		// Returns argument span by specific index, from a general tree of arguments.
		static bwn::array_view<const Argument> getArgumentFromIndex(const size_t _argumentIndex, const bwn::array_view<const Argument>& _arguments);
		// Returns true if specific string could be counted as a valid command name.
		static bool isValidCommandName(const bwn::string_view& _name);
		// Composes context from some array of arguments.
		static CursorContext composeContext(const uint32_t _characterPos, const bwn::array_view<const Argument>& _arguments);

		//
		// Private methods.
		//
	private:
		// Basicaly same as a public version, but for internal use with context.
		const Token* parseSingleArgument(const Token*const _begin, const InnerContext& _context);
		// Parsing of the arguments from tokens list. Appends arguments list and returns the number of arguments parsed. (can append error list)
		size_t parseCommandArguments(const Token*& _it, const InnerContext& _context);
		// Parses array argument and pushes Argument to the stack. Can push errors as well.
		const Token* parseArrayArgument(const Token*const _begin, const InnerContext& _context);
		// Parses map argument and pushes Argument to the stack. Can push errors as well.
		const Token* parseMapArgument(const Token*const _begin, const InnerContext& _context);

		// Tries to format a generic argument. Returns variant as that value/map/array.
		// If failed, sets variant as NIL and returns false.
		static SystemResult formatGeneralArgument(const bwn::array_view<const Argument>& _arguments, Variant& o_value);
		// Tries to format argument as a single value. Returns variant as that value.
		// If failed, sets variant as NIL and returns false.
		static SystemResult formatValueArgument(const bwn::array_view<const Argument>& _arguments, Variant& o_value);
		// Tries to format argument as a map. Returns variant as that map.
		// If failed, sets variant as NIL and returns false.
		static SystemResult formatMapArgument(const bwn::array_view<const Argument>& _arguments, Variant& o_value);
		// Tries to format argument as a vector. Returns variant as that map.
		// If failed, sets variant as NIL and returns false.
		static SystemResult formatArrayArgument(const bwn::array_view<const Argument>& _arguments, Variant& o_value);

		//
		// Private members.
		//
	private:
		// Arguments.
		std::vector<Argument> m_arguments;
	};
} // namespace bwn::console