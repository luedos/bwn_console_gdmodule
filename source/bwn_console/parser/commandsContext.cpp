#include "precompiled_console.hpp"

#include "bwn_console/parser/commandsContext.hpp"
#include "bwn_console/parser/argumentsContext.hpp"
#include "bwn_console/parser/tokensContext.hpp"
#include "bwn_console/parser/errorsContext.hpp"

bwn::console::Command::Command(const bwn::array_view<const Argument>& _arguments)
{
	BWN_TRAP_COND(
		!_arguments.empty() && _arguments.front().type() == EArgumentType::k_command,
		"Invalid arguments provided to the command. There should be at least one argument with command name at the start.");

	const Argument& argumentName = _arguments.front();

	const Token*const tokenNameStartIt = argumentName.begin();
	const Token*const tokenNameEndIt = argumentName.end();

	const size_t tokensSize = std::distance(tokenNameStartIt, tokenNameEndIt);

	BWN_TRAP_COND(
		tokensSize <= 1,
		"Invalid arguments provided to the command. There should be no more then one token at the command argument.");

	m_name = tokensSize == 0
		? bwn::string_view() // This is valid case where the command could be incorrectly parsed so it doesn't have a name.
		: tokenNameStartIt->getFullString();

	m_arguments = _arguments.sub(1);
	// Even if token size could be 0, token begin always should be not nullptr, otherwise it was incorrectly parsed.
	m_startPos = tokenNameStartIt->getStartPos();
	m_endPos = _arguments.back().getEndPos();
}

bwn::string_view bwn::console::Command::getName() const
{
	return m_name;
}

bwn::SystemResult bwn::console::Command::formatArguments(std::vector<Variant>& o_arguments) const
{
	o_arguments.clear();

	const Argument* argumentIt = m_arguments.begin();
	const Argument*const endIt = m_arguments.end();

	while (argumentIt != endIt)
	{
		const bwn::array_view<const Argument> arguments = bwn::array_view<const Argument>(argumentIt, endIt);

		Variant value;
		const SystemResult result = ArgumentsContext::formatSingleArgument(arguments, value);
		if (!result.success())
		{
			o_arguments.clear();
			return result;
		}

		o_arguments.push_back(value);
		argumentIt = ArgumentsContext::skipArgument(argumentIt, endIt);
	}

	return SystemFacility::createSuccess();
}

bwn::array_view<const bwn::console::Argument> bwn::console::Command::getArguments() const
{
	return m_arguments;
}

uint32_t bwn::console::Command::getStartPos() const
{
	return m_startPos;
}

uint32_t bwn::console::Command::getEndPos() const
{
	return m_endPos;
}

bwn::array_view<const bwn::console::Argument> bwn::console::Command::computeArgumentSpan(const uint32_t _argumentIndex) const
{
	return ArgumentsContext::getArgumentFromIndex(_argumentIndex, m_arguments);
}

class bwn::console::CommandsContext::InnerContext
{
	//
	// Construction and destruction.
	//
public:
	InnerContext(const bwn::array_view<const Argument>& _arguments, ErrorsContext& _errors)
		: m_arguments( _arguments )
		, m_errors( &_errors )
	{}

	//
	// Public interface.
	//
public:
	// Returns the start of the tokens array.
	const Argument* begin() const
	{
		return m_arguments.begin();
	}
	// Returns the end of the tokens array.
	const Argument* end() const
	{
		return m_arguments.end();
	}
	// Pushes an error.
	void pushError(const EErrorType _type, const Argument& _argument) const
	{
		m_errors->pushError(_type, _argument.begin()->getStartPos());
	}

	//
	// Private members.
	//
private:
	// Array of tokens.
	bwn::array_view<const Argument> m_arguments;
	// Error context to push errors.
	ErrorsContext* m_errors;
};

void bwn::console::CommandsContext::parseCommands(
	const bwn::array_view<const Argument>& _arguments,
	ErrorsContext& _errors)
{
	m_commands.clear();

	const Argument* argumentIt = _arguments.begin();
	const Argument*const endIt = _arguments.end();

	const InnerContext context = InnerContext(_arguments, _errors);

	while( argumentIt != endIt )
	{
		argumentIt = parseSingleCommand(argumentIt, context);
	}
}

const std::vector<bwn::console::Command>& bwn::console::CommandsContext::getCommands() const
{
	return m_commands;
}

bwn::console::CursorContext bwn::console::CommandsContext::composeContext(const uint32_t _characterPos) const
{
	return composeContext(_characterPos, m_commands);
}

bwn::console::CursorContext bwn::console::CommandsContext::composeContext(
	const uint32_t _characterPos,
	const bwn::array_view<const Command>& _commands)
{
	if (_commands.empty())
	{
		return CursorContext();
	}

	CursorContext context;

	const Command* commandIt = _commands.begin();
	const Command* nextCommandIt = commandIt + 1;
	for (; nextCommandIt < _commands.end(); ++nextCommandIt, ++commandIt)
	{
		// Basically, as soon as we overshoot the start of the command, we should return the previos one (aka commandIt).
		if (nextCommandIt->getStartPos() > _characterPos)
		{
			break;
		}
	}

	const Command& command = *commandIt;
	context.commandIndex = commandIt - _commands.begin();
	context.commandCharacterIndex = command.getStartPos();

	if (_characterPos < command.getStartPos())
	{
		// This only happens if character pos was before the first command.
		context.characterIndex = context.commandCharacterIndex;
		context.argumentCharacterIndex = context.commandCharacterIndex;

		return context;
	}

	const bwn::array_view<const Argument> arguments = command.getArguments();
	// Is current character index is inside the command name itself.
	const bwn::string_view commandName = command.getName();
	const uint32_t commandNameEndPos = command.getStartPos() + commandName.length();
	const bool characterInsideCommand = !commandName.empty() && _characterPos <= commandNameEndPos;
	const bool commandIsEmpty = commandName.empty() && arguments.empty();

	if (characterInsideCommand || commandIsEmpty)
	{
		context.localArgumentNumber = 0;
		context.argumentCharacterIndex = context.commandCharacterIndex;
		context.characterIndex = _characterPos;
		return context;
	}

	if (arguments.empty())
	{
		// if arguments are empty but we are still not inside the command,
		// then we assume we are at the first argument which simply doesn't exist yet.
		context.localArgumentNumber = 1;
		context.argumentCharacterIndex = _characterPos;
		context.characterIndex = _characterPos;
		return context;
	}

	const CursorContext argumentsContext = ArgumentsContext::composeContext(_characterPos, arguments);
	context.localArgumentNumber = argumentsContext.localArgumentNumber;
	context.argumentCharacterIndex = argumentsContext.argumentCharacterIndex;
	context.characterIndex = argumentsContext.characterIndex;
	return context;
}

void bwn::console::CommandsContext::clear()
{
	m_commands.clear();
}

bwn::string_view bwn::console::CommandsContext::getCommandNameRef(const uint32_t _commandIndex) const
{
	if (_commandIndex < m_commands.size())
	{
		return m_commands[_commandIndex].getName();
	}

	return bwn::string_view();
}

const bwn::console::Argument* bwn::console::CommandsContext::parseSingleCommand(
	const Argument*const _begin,
	const InnerContext& _context)
{
	BWN_ASSERT_WARNING_COND(
		_begin->type() == EArgumentType::k_command,
		"Internal parsing error: Incorrect argument array is passed to command parsing. No command can be parsed from this.");

	if (_begin->type() != EArgumentType::k_command)
	{
		_context.pushError(EErrorType::k_missingCommandName, *_begin);

		const Argument* argumentIt = _begin + 1;
		const Argument*const endIt = _context.end();

		while (argumentIt != endIt && argumentIt->type() != EArgumentType::k_command)
		{
			argumentIt = ArgumentsContext::skipArgument(argumentIt, endIt);
		}

		return argumentIt;
	}

	uint32_t leadingCount = _begin->getLeadingCount();

	const Argument* argumentIt = _begin + 1;
	const Argument*const endIt = _context.end();
	while (argumentIt != endIt && leadingCount > 0)
	{
		--leadingCount;
		argumentIt = ArgumentsContext::skipArgument(argumentIt, endIt);
	}

	BWN_ASSERT_WARNING_COND(
		leadingCount == 0,
		"Internal parsing error: Incorrect number of arguments for command name '{}', {} absent.",
		bwn::wrap_utf(_begin->begin()->getContentString()),
		leadingCount);

	if (leadingCount == 0)
	{
		m_commands.push_back(bwn::array_view<const Argument>(_begin, argumentIt));
	}

	return argumentIt;
}
