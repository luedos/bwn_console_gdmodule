#pragma once

#include "bwn_console/parser/cursorContext.hpp"
#include "bwn_core/containers/arrayView.hpp"
#include "bwn_core/types/stringView.hpp"
#include "bwn_core/results/systemResult.hpp"

#include <vector>

//
// Forward declarations.
//
class Variant;
namespace bwn::console
{
	class Argument;
	class Token;
	class Error;
	class ErrorsContext;
} // namespace bwn::console

namespace bwn::console
{
	class Argument;
	class Token;

	class Command
	{
		//
		// Construction and destruction.
		//
	public:
		Command(const bwn::array_view<const Argument>& _arguments);

		//
		// Public interface.
		//
	public:
		// Simply returns name of the command.
		bwn::string_view getName() const;
		// Parses command arguments into the vector of arguments.
		SystemResult formatArguments(std::vector<Variant>& o_arguments) const;
		// Returns the arguments of the command (first 'k_command' argument not included).
		bwn::array_view<const Argument> getArguments() const;
		// Returns the character start position of the command, which was retrieved from first command token.
		uint32_t getStartPos() const;
		// Returns the command end position.
		uint32_t getEndPos() const;
		// Computes span of Argument structs for specific argument index.
		// If index is incorrect, it will return empty array.
		bwn::array_view<const Argument> computeArgumentSpan(const uint32_t _argumentIndex) const;

		//
		// Private members.
		//
	private:
		bwn::string_view m_name;
		bwn::array_view<const Argument> m_arguments;
		uint32_t m_startPos = 0;
		uint32_t m_endPos = 0;
	};

	class CommandsContext
	{
		//
		// Private classes.
		//
	private:
		class InnerContext;

		//
		// Construction and destruction.
		//
	public:
		CommandsContext() = default;

		//
		// Public interface.
		//
	public:
		// Parses arguments into commands.
		void parseCommands(const bwn::array_view<const Argument>& _arguments, ErrorsContext& _errors);
		// Just returns the commands.
		const std::vector<Command>& getCommands() const;
		// Composes a context based on specific character position.
		CursorContext composeContext(const uint32_t _characterPos) const;
		// Composes a context based on specific character position and external commands.
		static CursorContext composeContext(const uint32_t _characterPos, const bwn::array_view<const Command>& _commands);
		// Clears commands.
		void clear();
		// Returns name reference for the specific command. If index is invalid, will return empty string view.
		bwn::string_view getCommandNameRef(const uint32_t _commandIndex) const;

		//
		// Private interface.
		//
	private:
		const Argument* parseSingleCommand(const Argument*const _begin, const InnerContext& _context);

		//
		// Private members.
		//
	private:
		std::vector<Command> m_commands;
	};
} // namespace bwn::console
