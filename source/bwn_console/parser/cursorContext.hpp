#pragma once

#include <cstdint>

namespace bwn::console
{
	struct CursorContext
	{
		// General character index.
		uint32_t characterIndex = 0;
		// Index of the character for start of the command.
		uint32_t commandCharacterIndex = 0;
		// Index of the character for start of the argument of the command (if cursor currently on command name, it will point to start of the command name).
		uint32_t argumentCharacterIndex = 0;
		// Index of the command.
		uint16_t commandIndex = 0;
		// Index of the local argument of the command. Starts from 1. If context pointing to the command itself, and not to the argument, argument index will be 0.
		uint16_t localArgumentNumber = 0;
	};
} // namespace bwn::console