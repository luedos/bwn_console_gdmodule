#include "precompiled_console.hpp"

#include "bwn_console/parser/errorsContext.hpp"
#include "bwn_console/parser/cursorContext.hpp"
#include "bwn_console/parser/commandsContext.hpp"

bwn::console::Error::Error(const EErrorType _type, const uint32_t _characterIndex)
	: m_commandIndex( 0 )
	, m_localArgumentNumber( 0 )
	, m_characterIndex( _characterIndex )
	, m_type( _type )
{}

bwn::console::Error::Error(
	const EErrorType _type,
	const uint32_t _characterIndex,
	const uint32_t _localArgumentNumber,
	const uint32_t _commandIndex)
	: m_commandIndex( _commandIndex )
	, m_localArgumentNumber( _localArgumentNumber )
	, m_characterIndex( _characterIndex )
	, m_type( _type )
{}

String bwn::console::Error::format() const
{
	if (m_type == EErrorType::k_none)
	{
		return String();
	}

	std::basic_string<char32_t> buffer;
	formatTo(buffer);
	return String(buffer.data(), buffer.length());
}

void bwn::console::Error::formatTo(std::basic_string<char32_t>& _string) const
{
	if (m_type == EErrorType::k_none)
	{
		return;
	}

	const char32_t*const errorPattern = [this]() -> const char32_t*
	{
		// 0 position: command
		// 1 position: argument
		// 2 position: character
		switch (this->m_type)
		{
			case EErrorType::k_none: break; // This will never be executed, this is only so warnings will shutup.

			case EErrorType::k_mapNotClosed:
			return U"Expected closing braket at position {2} for the map used in argument {1} of the command {0}.";
			// For the case, when the closing of the map exist without opening of the map.
			case EErrorType::k_mapNotOpened:
			return U"Unexpected closing bracket at position {2}, at argument {1} for command {0}.";
			// The error for when we are missing key for the provided value in a map.
			case EErrorType::k_missingMapKey:
			return U"Expected a map key for the map value at position {2}, in map from argument {1} of the command {0}.";
			// The error for when If there was no equal sign in the map between key and the value.
			case EErrorType::k_missingMapEqual:
			return U"Expected ':' at position {2}, from argument {1} of the command {0}.";
			// The error for when we are missing value for the provided key in a map.
			case EErrorType::k_missingMapValue:
			return U"Expected a value at position {2}, from argument {1} of the command {0}.";
			// Equal sign outside of the map.
			case EErrorType::k_globalEqual:
			return U"Unexpected ':' without map at position {2}, from argument {1} of the command {0}.";
			// The error for when array argument wasn't finished with closing bracket.
			case EErrorType::k_arrayNotClosed:
			return U"Expecting closing bracket at position {2} for the map used in argument {1} of the command {0}.";
			// For the case, when the closing of the array exist without opening of the array.
			case EErrorType::k_arrayNotOpened:
			return U"Unexpected closing bracket at position {2}, at arguemnt {1} for the command {0}.";
			// The error for when string argument wasn't finished with quote.
			case EErrorType::k_stringNotClosed:
			return U"Expected string quote at position {2}, for the argument {1} of the command {0}.";
			// Mostly used for when sign of the number is provided, but number itself is not.
			case EErrorType::k_numberInvalid:
			return U"Invalid number at position {2}, used in the argument {1} of the command {0}.";
			// Error for the case when additional number check is failed.
			case EErrorType::k_intOverflow:
			return U"Number at position {2}, used in the argument {1} of the coomand {0}, is to big for the standart type int64_t.";
			// Error for the case when additional number check is failed, but for float.
			case EErrorType::k_floatOverflow:
			return U"Number at position {2}, used in the argument {1} of the coomand {0}, is to big for the standart type double.";
			// For the case when we are entering multiple commands, but there is no command between command separators, or command name is invalid.
			case EErrorType::k_missingCommandName:
			return U"Missing name for the command {0} at position {2}, or name is invalid.";
			case EErrorType::k_globalCommandSeparator:
			return U"Wasn't expecting command separator in list of arguments at position {2}.";
		}

		return U"Unknown error at the position {2}, for the argument {1} of the command {0}.";
	}();

	fmt::format_to(std::back_inserter(_string), errorPattern, m_commandIndex + 1, m_localArgumentNumber, m_characterIndex);
}

void bwn::console::Error::completeError(const bwn::array_view<const Command>& _commands)
{
	const CursorContext context = CommandsContext::composeContext(m_characterIndex, _commands);
	m_commandIndex = context.commandIndex;
	m_localArgumentNumber = context.localArgumentNumber;
}

bwn::console::Error::operator bool() const
{
	return m_type == EErrorType::k_none;
}

void bwn::console::ErrorsContext::pushError(const EErrorType _type, const CursorContext& _context)
{
	m_errors.emplace_back(
		_type,
		_context.characterIndex,
		_context.localArgumentNumber,
		_context.commandIndex);
}

void bwn::console::ErrorsContext::pushError(const EErrorType _type, const uint32_t _characterPos)
{
	m_errors.emplace_back(_type, _characterPos);
}

void bwn::console::ErrorsContext::completeErrors(const bwn::array_view<const Command>& _commands)
{
	for (console::Error& error : m_errors)
	{
		error.completeError(_commands);
	}
}

void bwn::console::ErrorsContext::clear()
{
	m_errors.clear();
}

size_t bwn::console::ErrorsContext::getErrorCount() const
{
	return m_errors.size();
}

const std::vector<bwn::console::Error>& bwn::console::ErrorsContext::getErrors() const
{
	return m_errors;
}

String bwn::console::ErrorsContext::formatErrors(const bwn::string_view& _separator) const
{
	std::basic_string<char32_t> buffer;
	bool first = true;
	for (const console::Error& error : m_errors)
	{
		if (!first)
		{
			buffer += _separator;
		}
		else
		{
			first = false;
		}

		error.formatTo(buffer);
	}

	return String(buffer.data(), buffer.length());
}

void bwn::console::ErrorsContext::printErrors() const
{
	for (const console::Error& error : m_errors)
	{
		::print_error(error.format());
	}
}