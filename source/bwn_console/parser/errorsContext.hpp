#pragma once

#include "bwn_core/containers/arrayView.hpp"

#include <core/string/ustring.h>

#include <vector>

namespace bwn::console
{
	//
	// Forward declarations
	//
	class Command;
	struct CursorContext;

	enum class EErrorType : uint32_t
	{
		k_none,
		// The error for when map argument wasn't finished with closing bracket.
		k_mapNotClosed,
		// For the case, when the closing of the map exist without opening of the map.
		k_mapNotOpened,
		// The error for when we are missing key for the provided value in a map.
		k_missingMapKey,
		// The error for when If there was no equal sign in the map between key and the value.
		k_missingMapEqual,
		// The error for when we are missing value for the provided key in a map.
		k_missingMapValue,
		// Equal sign outside of the map.
		k_globalEqual,
		// The error for when array argument wasn't finished with closing bracket.
		k_arrayNotClosed,
		// For the case, when the closing of the array exist without opening of the array.
		k_arrayNotOpened,
		// The error for when string argument wasn't finished with quote.
		k_stringNotClosed,
		// Mostly used for when sign of the number is provided, but number itself is not.
		k_numberInvalid,
		// Error for the case when additional number check is failed.
		k_intOverflow,
		// Error for the case when additional number check is failed, but for the float.
		k_floatOverflow,
		// For the case when we are entering multiple commands, but there is no command between command separators, or command name is invalid.
		k_missingCommandName,
		// There is unnecessary command separator in span of arguments.
		k_globalCommandSeparator
	};

	class Error
	{
		//
		// Construction and destruction
		//
	public:
		// Simple plain constructor.
		Error(const EErrorType _type, const uint32_t _characterIndex);
		// Constructor from full error info.
		Error(
			const EErrorType _type,
			const uint32_t _characterIndex,
			const uint32_t _localArgumentNumber,
			const uint32_t _commandIndex);

		//
		// Public interface.
		//
	public:
		// Creates a string based on parameters and type of the error.
		String format() const;
		// Formats by appending to a string.
		void formatTo(std::basic_string<char32_t>& _string) const;
		// If error was created via simple plain constructor,
		// some of it's arguments will remain defaulted,
		// and so they should be written by calling this function.
		void completeError(const bwn::array_view<const Command>& _commands);
		// Simple bool operator, which returns true if error type is 'k_none'.
		operator bool() const;

		//
		// Private members.
		//
	private:
		// Index of the command, to which error is applied.
		uint32_t m_commandIndex = 0;
		// The the specific number of the argument within specified command, to which error is applied. Start from 1. If error applied to command name itself, argument number will be 0.
		uint32_t m_localArgumentNumber = 0;
		// The index of the character in the provided string, to which error is applied.
		uint32_t m_characterIndex = 0;
		// The actual type of error.
		EErrorType m_type = EErrorType::k_none;
	};

	class ErrorsContext
	{
		//
		// Construction and destruction.
		//
	public:
		ErrorsContext() = default;

		//
		// Public interface.
		//
	public:
		// Pushes full error into the error stack.
		void pushError(const EErrorType _type, const CursorContext& _context);
		// Pushes incomplete error into the error stack (later 'completeErrors should be called').
		void pushError(const EErrorType _type, const uint32_t _characterPos);
		// Completes all incomplete errors from the stack.
		void completeErrors(const bwn::array_view<const Command>& _commands);
		// Clear all the errors from the stack.
		void clear();
		// Returns total count of errors.
		size_t getErrorCount() const;
		// Returns all the errors.
		const std::vector<console::Error>& getErrors() const;
		// Formats all the errors into one string
		String formatErrors(const bwn::string_view& _separator) const;
		// Prints all the errors from the stack.
		void printErrors() const;

		//
		// Private members.
		//
	private:
		// The stack of errors.
		std::vector<Error> m_errors;
	};
} // bwn::console
