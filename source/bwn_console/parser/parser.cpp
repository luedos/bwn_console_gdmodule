#include "precompiled_console.hpp"

#include "bwn_console/parser/parser.hpp"

void bwn::console::ParserView::parse(const bwn::string_view& _string)
{
	m_tokens.parse(_string, m_errors);
	m_arguments.parseCommands(m_tokens.getTokens(), m_errors);
	m_commands.parseCommands(m_arguments.getArguments(), m_errors);

	m_errors.completeErrors(m_commands.getCommands());
}

const bwn::console::TokensContext& bwn::console::ParserView::getTokensContext() const
{
	return m_tokens;
}

const bwn::console::ArgumentsContext& bwn::console::ParserView::getArgumentsContext() const
{
	return m_arguments;
}

const bwn::console::CommandsContext& bwn::console::ParserView::getCommandsContext() const
{
	return m_commands;
}

const bwn::console::ErrorsContext& bwn::console::ParserView::getErrorsContext() const
{
	return m_errors;
}

void bwn::console::ParserView::clear()
{
	m_tokens.clear();
	m_arguments.clear();
	m_commands.clear();
	m_errors.clear();
}

bool bwn::console::ParserView::hasErrors() const
{
	return m_errors.getErrorCount() != 0;
}

bwn::console::CursorContext bwn::console::ParserView::composeContext(const uint32_t _characterPos) const
{
	return m_commands.composeContext(_characterPos);
}

bwn::string_view bwn::console::ParserView::findArgumentStringRef(const CursorContext& _context) const
{
	if (_context.commandIndex >= m_commands.getCommands().size())
	{
		return bwn::string_view();
	}

	const Command& command = m_commands.getCommands()[_context.commandIndex];

	// If we pointing to the command name, and not to some argument.
	if (_context.localArgumentNumber == 0)
	{
		const uint32_t commandStartPos = command.getStartPos();
		const size_t commandDesiredLength = _context.characterIndex - commandStartPos;
		const size_t commandMaxLength = command.getName().length();

		return command.getName().substr(0, std::min(commandDesiredLength, commandMaxLength));
	}

	const bwn::array_view<const Argument> argumentsSpan = command.computeArgumentSpan(_context.localArgumentNumber - 1);
	if (argumentsSpan.empty())
	{
		return bwn::string_view();
	}

	const Token*const firstToken = argumentsSpan.front().begin();
	const Token*const lastToken = argumentsSpan.back().last();

	if (argumentsSpan.size() == 1 && argumentsSpan.front().type() == EArgumentType::k_value)
	{
		const uint32_t argumentStartPos = firstToken->getStartPos() + firstToken->getStartPadding();

		const char32_t*const argumentStringBegin = firstToken->begin() + firstToken->getStartPadding();
		const char32_t*const argumentStringEnd = lastToken->end() - lastToken->getEndPadding();

		const size_t argumentMaxLength = argumentStringEnd - argumentStringBegin;
		const size_t argumentDesiredLength = _context.characterIndex - argumentStartPos;

		const bwn::string_view argumentString = bwn::string_view( argumentStringBegin, argumentMaxLength );
		return argumentString.substr(0, std::min(argumentDesiredLength, argumentMaxLength));
	}

	const uint32_t argumentStartPos = argumentsSpan.front().begin()->getStartPos();

	const char32_t*const argumentStringBegin = argumentsSpan.front().begin()->begin();
	const char32_t*const argumentStringEnd = argumentsSpan.back().last()->end();

	const size_t argumentMaxLength = argumentStringEnd - argumentStringBegin;
	const size_t argumentDesiredLength = _context.characterIndex - argumentStartPos;

	const bwn::string_view argumentString = bwn::string_view( argumentStringBegin, argumentMaxLength );
	return argumentString.substr(0, std::min(argumentDesiredLength, argumentMaxLength));
}

void bwn::console::Parser::parse(const String& _string)
{
	m_text = _string;
	m_parser.parse(bwn::wrap_view(m_text));
}

const String& bwn::console::Parser::getText() const
{
	return m_text;
}

const bwn::console::TokensContext& bwn::console::Parser::getTokensContext() const
{
	return m_parser.getTokensContext();
}

const bwn::console::ArgumentsContext& bwn::console::Parser::getArgumentsContext() const
{
	return m_parser.getArgumentsContext();
}

const bwn::console::CommandsContext& bwn::console::Parser::getCommandsContext() const
{
	return m_parser.getCommandsContext();
}

const bwn::console::ErrorsContext& bwn::console::Parser::getErrorsContext() const
{
	return m_parser.getErrorsContext();
}

void bwn::console::Parser::clear()
{
	m_text.clear();
	m_parser.clear();
}

bool bwn::console::Parser::hasErrors() const
{
	return m_parser.hasErrors();
}

bwn::console::CursorContext bwn::console::Parser::composeContext(const uint32_t _characterPos) const
{
	return m_parser.composeContext(_characterPos);
}

bwn::string_view bwn::console::Parser::findArgumentStringRef(const CursorContext& _context) const
{
	return m_parser.findArgumentStringRef(_context);
}