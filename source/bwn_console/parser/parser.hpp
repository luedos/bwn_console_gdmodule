#pragma once

#include "bwn_console/parser/tokensContext.hpp"
#include "bwn_console/parser/argumentsContext.hpp"
#include "bwn_console/parser/commandsContext.hpp"
#include "bwn_console/parser/errorsContext.hpp"
#include "bwn_console/parser/cursorContext.hpp"

#include <core/string/ustring.h>

namespace bwn::console
{
	class ParserView
	{
		//
		// Construction and destruction.
		//
	public:
		ParserView() = default;

		//
		// Public interface.
		//
	public:
		// Parses full context from command.
		void parse(const bwn::string_view& _string);
		// Returns token context.
		const TokensContext& getTokensContext() const;
		// Returns arguments context.
		const ArgumentsContext& getArgumentsContext() const;
		// Returns command context.
		const CommandsContext& getCommandsContext() const;
		// Returns errors context.
		const ErrorsContext& getErrorsContext() const;
		// Completely clears the contexts.
		void clear();
		// Helper to check if had any errors.
		bool hasErrors() const;
		// Helper to compose a context on internal state.
		CursorContext composeContext(const uint32_t _characterPos) const;
		// Tries to find argument string from specific context.
		// String length will be determent by the cursor position (if the cursor position is in the middle of the argument, string will end at the cursor position).
		// Note: returns reference to the string which is owned by parser itself.
		bwn::string_view findArgumentStringRef(const CursorContext& _context) const;

		//
		// Private members.
		//
	private:
		TokensContext m_tokens;
		ArgumentsContext m_arguments;
		CommandsContext m_commands;
		ErrorsContext m_errors;
	};

	class Parser
	{
		//
		// Construction and destruction.
		//
	public:
		Parser() = default;

		//
		// Public interface.
		//
	public:
		// Parses full context from command.
		void parse(const String& _string);
		// Returns current text.
		const String& getText() const;
		// Returns token context.
		const TokensContext& getTokensContext() const;
		// Returns arguments context.
		const ArgumentsContext& getArgumentsContext() const;
		// Returns command context.
		const CommandsContext& getCommandsContext() const;
		// Returns errors context.
		const ErrorsContext& getErrorsContext() const;
		// Completely clears the contexts.
		void clear();
		// Helper to check if had any errors.
		bool hasErrors() const;
		// Helper to compose a context on internal state.
		CursorContext composeContext(const uint32_t _characterPos) const;
		// Tries to find argument string from specific context.
		// String length will be determent by the cursor position (if the cursor position is in the middle of the argument, string will end at the cursor position).
		// Note: returns reference to the string which is owned by parser itself.
		bwn::string_view findArgumentStringRef(const CursorContext& _context) const;

		//
		// Private members.
		//
	private:
		String m_text;
		ParserView m_parser;
	};
} // namespace bwn::console