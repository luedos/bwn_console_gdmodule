#include "precompiled_console.hpp"

#include "bwn_console/parser/tokensContext.hpp"
#include "bwn_console/parser/errorsContext.hpp"
#include "bwn_core/utility/stringParsingUtility.hpp"

bwn::console::Token::Token(const char32_t*const _string, const uint32_t _tokenStartPos)
	: m_string( _string )
	, m_startPos( _tokenStartPos )
	, m_fullLength( 0 )
	, m_startPaddingLength( 0 )
	, m_length( 0 )
	, m_endPaddingLength( 0 )
	, m_type( ETokenType::k_invalid )
{
	BWN_TRAP_COND(m_string != nullptr, "Token string can not be nullptr!");
}

bwn::console::Token::Token(const bwn::string_view& _string, const uint32_t _tokenStartPos, const ETokenType _type)
	: m_string( _string.data() )
	, m_startPos( _tokenStartPos )
	, m_fullLength( _string.length() )
	, m_startPaddingLength( 0 )
	, m_length( _string.length() )
	, m_endPaddingLength( 0 )
	, m_type( _type )
{
	BWN_TRAP_COND(m_string != nullptr, "Token string can not be nullptr!");
	BWN_TRAP_COND(
		_string.length() < k_maxStringLength,
		"Token string length is invalid. Max length is {}, but provided is {}.",
		k_maxStringLength,
		_string.length());
}

bwn::console::Token::Token(
	const bwn::string_view& _string,
	const uint32_t _tokenStartPos,
	const uint16_t _startPadding,
	const uint16_t _endPadding,
	const ETokenType _type)
	: m_string( _string.data() )
	, m_startPos( _tokenStartPos )
	, m_fullLength( _string.length() )
	, m_startPaddingLength( _startPadding )
	, m_length( _string.length() - _startPadding - _endPadding )
	, m_endPaddingLength( _endPadding )
	, m_type( _type )
{
	BWN_TRAP_COND(m_string != nullptr, "Token string can not be nullptr!");
	BWN_TRAP_COND(
		_string.length() >= (_startPadding + _endPadding),
		"Error constructing token. "
		"Start padding (with length {}) and end padding (with length {}) together are greater then string length (which is {}). "
		"This will result in buffer overflow later. ",
		_startPadding,
		_endPadding,
		_string.length());
	BWN_TRAP_COND(
		_string.length() < k_maxStringLength,
		"Token string length is invalid. Max length is {}, but provided is {}.",
		k_maxStringLength,
		_string.length());
}

const char32_t* bwn::console::Token::begin() const
{
	return m_string;
}

const char32_t* bwn::console::Token::end() const
{
	return m_string + m_fullLength;
}

bwn::string_view bwn::console::Token::getFullString() const
{
	return bwn::string_view(m_string, m_fullLength);
}

bwn::string_view bwn::console::Token::getContentString() const
{
	return bwn::string_view(m_string + m_startPaddingLength, m_length);
}

uint32_t bwn::console::Token::getStartPos() const
{
	return m_startPos;
}

uint32_t bwn::console::Token::getEndPos() const
{
	return m_startPos + m_fullLength;
}

uint32_t bwn::console::Token::getStartPadding() const
{
	return m_startPaddingLength;
}

uint32_t bwn::console::Token::getEndPadding() const
{
	return m_endPaddingLength;
}

bwn::console::ETokenType bwn::console::Token::type() const
{
	return m_type;
}

const char* bwn::console::Token::typeToString(const ETokenType _type)
{
	switch (_type)
	{
		case ETokenType::k_invalid: return "k_invalid";
		case ETokenType::k_commandSeparator: return "k_commandSeparator";
		case ETokenType::k_openingMap: return "k_openingMap";
		case ETokenType::k_closingMap: return "k_closingMap";
		case ETokenType::k_equal: return "k_equal";
		case ETokenType::k_openingArray: return "k_openingArray";
		case ETokenType::k_closingArray: return "k_closingArray";
		case ETokenType::k_decInteger: return "k_decInteger";
		case ETokenType::k_hexInteger: return "k_hexInteger";
		case ETokenType::k_octInteger: return "k_octInteger";
		case ETokenType::k_binInteger: return "k_binInteger";
		case ETokenType::k_float: return "k_float";
		case ETokenType::k_booleanTrue: return "k_booleanTrue";
		case ETokenType::k_booleanFalse: return "k_booleanFalse";
		case ETokenType::k_string: return "k_string";
	}

	return "unknown";
}

class bwn::console::TokensContext::InnerContext
{
	//
	// Construction and destruction.
	//
public:
	InnerContext(const bwn::string_view& _text, ErrorsContext& _errors)
		: m_text( _text )
		, m_errors( &_errors )
	{}

	//
	// Public interface.
	//
public:
	// The begin of text.
	const char32_t* begin() const
	{
		return m_text.begin();
	}
	// The end of text.
	const char32_t* end() const
	{
		return m_text.end();
	}
	// Pushes token specific error, based on the string.
	void pushError(const EErrorType _type, const char32_t*const _characterIt) const
	{
		m_errors->pushError(_type, _characterIt - m_text.begin());
	}

	//
	// Private members.
	//
private:
	bwn::string_view m_text;
	ErrorsContext* m_errors;
};

bwn::console::TokensContext::TokensContext()
	: m_tokens()
{}

void bwn::console::TokensContext::parse(const bwn::string_view& _text, ErrorsContext& _errors)
{
	m_tokens.clear();

	if (_text.empty())
	{
		return;
	}

	const InnerContext context(_text, _errors);

	const char32_t* currentIt = context.begin();
	const char32_t*const endIt = context.end();

	while (currentIt < endIt)
	{
		const char32_t character = *currentIt;

		if (std::iswspace(character))
		{
			++currentIt;
			continue;
		}

		// Main simple char tokens.
		switch (character)
		{
			case '{':
			{
				pushToken(currentIt, context.begin(), 1, ETokenType::k_openingMap);
				++currentIt;
				continue;
			}
			case '}':
			{
				pushToken(currentIt, context.begin(), 1, ETokenType::k_closingMap);
				++currentIt;
				continue;
			}
			case ':':
			{
				pushToken(currentIt, context.begin(), 1, ETokenType::k_equal);
				++currentIt;
				continue;
			}

			case '[':
			{
				pushToken(currentIt, context.begin(), 1, ETokenType::k_openingArray);
				++currentIt;
				continue;
			}
			case ']':
			{
				pushToken(currentIt, context.begin(), 1, ETokenType::k_closingArray);
				++currentIt;
				continue;
			}

			case ';':
			{
				pushToken(currentIt, context.begin(), 1, ETokenType::k_commandSeparator);
				++currentIt;
				continue;
			}

			default:
				break;
		}

		if (isQuoteChar(character))
		{
			currentIt = forceParseStringToken(currentIt, context);
			continue;
		}

		if (std::iswdigit(character))
		{
			currentIt = forceParseNumberToken(currentIt, context);
			continue;
		}

		const bool isSign = character == '-' || character == '+';
		if (isSign && (endIt - currentIt) >= 2)
		{
			const char32_t nextCharacter = *(currentIt + 1);
			if (std::iswdigit(nextCharacter))
			{
				currentIt = forceParseNumberToken(currentIt, context);
				continue;
			}
		}

		if (tryParseBooleanTrueToken(currentIt, context))
		{
			continue;
		}

		if (tryParseBooleanFalseToken(currentIt, context))
		{
			continue;
		}

		currentIt = forceParseStringToken(currentIt, context);
	}
}

const std::vector<bwn::console::Token>& bwn::console::TokensContext::getTokens() const
{
	return m_tokens;
}

void bwn::console::TokensContext::clear()
{
	m_tokens.clear();
}

bool bwn::console::TokensContext::isSpecialChar(const char32_t _character)
{
	return _character == '{'
		|| _character == '}'
		|| _character == ':'
		|| _character == '['
		|| _character == ']'
		|| _character == ';'
		|| _character == '"'
		|| _character == '\''
		|| std::iswspace(_character);
}

bool bwn::console::TokensContext::isQuoteChar(const char32_t _character)
{
	return _character == '"'
		|| _character == '\'';
}

bool bwn::console::TokensContext::isMatchingQuote(const char32_t _leftQuote, const char32_t _rightQuote)
{
	return _leftQuote == _rightQuote;
}

char32_t bwn::console::TokensContext::computeRequiredQuote(const bwn::string_view& _string)
{
	bool escape = false;
	bool hasSpecialChar = false;
	bool hasSingleQuote = false;
	bool hasDoubleQuote = false;

	for (const char32_t character : _string)
	{
		hasSpecialChar = hasSpecialChar || isSpecialChar(character);
		hasSingleQuote = hasSingleQuote || (!escape && character == '\'');
		hasDoubleQuote = hasDoubleQuote || (!escape && character == '\"');
		escape = character == '\\';
	}

	if (!hasSpecialChar)
	{
		return '\0';
	}

	return !hasDoubleQuote
		? '\"'
		: '\'';
}

const char32_t* bwn::console::TokensContext::forceParseStringToken(
	const char32_t*const _begin,
	const InnerContext& _context)
{
	BWN_TRAP_COND(_begin < _context.end(), "Inner parse error: forceParseStringToken assumes that _begin are valid by default.");

	const uint32_t tokenStartPos = _begin - _context.begin();

	// If first character is a quote, we assign it to the quoteChar.
	// If not - the quoteChar will be null, signaling that any space could be the end of the string.
	const char32_t quoteChar = isQuoteChar(*_begin) ? *_begin : '\0';

	// Quotes should not be placed into the string.
	// So basically we are incrementing the actual start of the string if we have quote character.
	const char32_t*const startIt = _begin + (quoteChar != '\0');
	const char32_t* charIt = startIt;
	const char32_t*const endIt = _context.end();

	if (quoteChar == '\0')
	{
		while (charIt < endIt)
		{
			if (isSpecialChar(*charIt))
			{
				break;
			}

			++charIt;
		}

		pushToken(startIt, _context.begin(), charIt - startIt, ETokenType::k_string);
	}
	else
	{
		// Are we escaping next quote.
		bool escape = false;

		while (charIt < endIt)
		{
			const char32_t character = *charIt;

			if (isMatchingQuote(quoteChar, *charIt) && !escape)
			{
				const uint32_t tokenFullLength = (charIt - _begin) + 1;
				m_tokens.push_back(Token(
					bwn::string_view(_begin, tokenFullLength),
					tokenStartPos,
					1,
					1,
					ETokenType::k_string));
				return charIt + 1;
			}

			// Basically: should we escape next character
			escape = character == '\\';

			++charIt;
		}

		// If we got here, then we reached the end of the string.
		// If the end of the string was reached without matching quote, then it's the error.
		_context.pushError(EErrorType::k_stringNotClosed, charIt);
		m_tokens.push_back(Token(
			bwn::string_view(_begin, charIt - _begin),
			tokenStartPos,
			1,
			0,
			ETokenType::k_invalid));
	}

	return charIt;
}

const char32_t* bwn::console::TokensContext::forceParseNumberToken(
	const char32_t*const _begin,
	const InnerContext& _context)
{
	BWN_TRAP_COND(_begin < _context.end(), "Inner parse error: forceParseNumberToken assumes that _begin are valid by default.");
	// Important note: we are checking for the sign only after we are checking for the special numbers,
	// because in general we are considering that hex, oct or bin numbers can't be signed.

	const char32_t* charIt = _begin;
	const char32_t*const endIt = _context.end();

	const char32_t firstCharacter = *charIt;

	// If first number is 0 this could be some specific number (like hex/oct/bin).
	if (firstCharacter == '0' && (endIt - charIt) > 2)
	{
		const char32_t secondChar = *(charIt + 1);
		if (secondChar == 'x' || secondChar == 'X')
		{
			return forceParseHexNumberToken(_begin, _context);
		}
		else if (std::iswdigit(secondChar))
		{
			return forceParseOctNumberToken(_begin, _context);
		}
		else if (secondChar == 'b')
		{
			return forceParseBinNumberToken(_begin, _context);
		}

		// At the end, we simply can have a dot.
	}

	if (firstCharacter == '-' || firstCharacter == '+')
	{
		++charIt;
	}

	bool isFloat = false;

	const auto skipOnError =
		[this, &_context, _begin, endIt](const char32_t* _currentIt) -> const char32_t*
		{
			_context.pushError(EErrorType::k_numberInvalid, _currentIt);
			// just skiping till the end token.
			const char32_t*const numberEnd = std::find_if(_currentIt, endIt, isSpecialChar);
			this->pushToken(_begin, _context.begin(), numberEnd - _begin, ETokenType::k_invalid);

			return numberEnd;
		};

	for (; charIt < endIt; ++charIt)
	{
		const char32_t character = *charIt;

		if (character == 'e' || character == 'E')
		{
			if (!validateExponent(charIt, endIt))
			{
				return skipOnError(charIt);
			}

			isFloat = true;
			break;
		}

		if (character == '.')
		{
			// If this is the second time we are encountering float, then this is the error.
			if (isFloat)
			{
				return skipOnError(charIt);
			}

			isFloat = true;
			continue;
		}

		if (isSpecialChar(character))
		{
			break;
		}

		if (!std::iswdigit(*charIt))
		{
			return skipOnError(charIt);
		}
	}

	{
		const bwn::string_view tokenView = bwn::string_view(_begin, charIt - _begin);
		double tempDouble;
		int64_t tempInt;

		// Just an additional check to know if any overflow is in case.
		const bool validNumber = isFloat
			? bwn::asDoubleStrict(tokenView, tempDouble)
			: bwn::asDecimalStrict(tokenView, tempInt);

		if (!validNumber)
		{
			_context.pushError(isFloat ? EErrorType::k_floatOverflow : EErrorType::k_intOverflow, _begin);
			pushToken(
				_begin,
				_context.begin(),
				charIt - _begin,
				ETokenType::k_invalid);
			return charIt;
		}
	}

	pushToken(
		_begin,
		_context.begin(),
		charIt - _begin,
		isFloat ? ETokenType::k_float : ETokenType::k_decInteger);
	return charIt;
}

const char32_t* bwn::console::TokensContext::forceParseHexNumberToken(
	const char32_t*const _begin,
	const InnerContext& _context)
{
	const char32_t* charIt = _begin;
	const char32_t*const endIt = _context.end();

	const auto skipOnError =
		[this, &_context, _begin, endIt](const char32_t* _currentIt) -> const char32_t*
		{
			_context.pushError(EErrorType::k_numberInvalid, _currentIt);
			// just skipping till the end token.
			const char32_t*const numberEnd = std::find_if(_currentIt, endIt, isSpecialChar);
			this->pushToken(_begin, _context.begin(), numberEnd - _begin, ETokenType::k_invalid);

			return numberEnd;
		};

	if (endIt - _begin < 3)
	{
		return skipOnError(_begin);
	}

	const bool firstCharSuccess = *charIt == '0';
	const bool secondCharSuccess = *(charIt + 1) == 'x' || *(charIt + 1) == 'X';

	if (!firstCharSuccess || !secondCharSuccess)
	{
		return skipOnError(_begin);
	}

	charIt += 2;

	while (charIt < endIt)
	{
		const char32_t character = *charIt;

		if (isSpecialChar(character))
		{
			break;
		}

		if (!std::iswxdigit(character))
		{
			return skipOnError(charIt);
		}

		++charIt;
	}

	{
		// Skipping hex header and checkking just a body, because we already validated header.
		const bwn::string_view tokenView = bwn::string_view(_begin + 2, charIt - _begin);
		uint64_t tempInt;

		if (!bwn::asHexadecimalBodyStrict(tokenView, tempInt) || tempInt > static_cast<uint64_t>(std::numeric_limits<int64_t>::max()))
		{
			_context.pushError(EErrorType::k_intOverflow, _begin);
			pushToken(_begin, _context.begin(), charIt - _begin, ETokenType::k_invalid);
			return charIt;
		}
	}

	pushToken(_begin, _context.begin(), charIt - _begin, ETokenType::k_hexInteger);
	return charIt;
}

const char32_t* bwn::console::TokensContext::forceParseOctNumberToken(
	const char32_t*const _begin,
	const InnerContext& _context)
{
	const char32_t* charIt = _begin;
	const char32_t*const endIt = _context.end();

	const auto skipOnError =
		[this, &_context, _begin, endIt](const char32_t* _currentIt) -> const char32_t*
		{
			_context.pushError(EErrorType::k_numberInvalid, _currentIt);
			// just skipping till the end token.
			const char32_t*const numberEnd = std::find_if(_currentIt, endIt, isSpecialChar);
			this->pushToken(_begin, _context.begin(), numberEnd - _begin, ETokenType::k_invalid);

			return numberEnd;
		};

	if (endIt - _begin < 2)
	{
		return skipOnError(_begin);
	}

	const bool firstCharSuccess = *charIt == '0';
	const bool secondCharSuccess = *(charIt + 1) >= '0' && *(charIt + 1) <= '7';

	if (!firstCharSuccess || !secondCharSuccess)
	{
		return skipOnError(_begin);
	}

	charIt += 2;

	while (charIt < endIt)
	{
		const char32_t character = *charIt;

		if (isSpecialChar(character))
		{
			break;
		}

		const bool isOctDigit = character >= '0' && character <= '7';
		if (!isOctDigit)
		{
			return skipOnError(charIt);
		}

		++charIt;
	}

	{
		// Skipping hex header and checkking just a body, because we already validated header.
		const bwn::string_view tokenView = bwn::string_view(_begin + 1, charIt - _begin);
		uint64_t tempInt;

		if (!bwn::asOctodecimalBodyStrict(tokenView, tempInt) || tempInt > static_cast<uint64_t>(std::numeric_limits<int64_t>::max()))
		{
			_context.pushError(EErrorType::k_intOverflow, _begin);
			pushToken(_begin, _context.begin(), charIt - _begin, ETokenType::k_invalid);
			return charIt;
		}
	}

	pushToken(_begin, _context.begin(), charIt - _begin, ETokenType::k_octInteger);
	return charIt;
}

const char32_t* bwn::console::TokensContext::forceParseBinNumberToken(
	const char32_t*const _begin,
	const InnerContext& _context)
{
	const char32_t* charIt = _begin;
	const char32_t*const endIt = _context.end();

	const auto skipOnError =
		[this, &_context, _begin, endIt](const char32_t* _currentIt) -> const char32_t*
		{
			_context.pushError(EErrorType::k_numberInvalid, _currentIt);
			// just skiping till the end token.
			const char32_t*const numberEnd = std::find_if(_currentIt, endIt, isSpecialChar);
			this->pushToken(_begin, _context.begin(), numberEnd - _begin, ETokenType::k_invalid);

			return numberEnd;
		};

	if (endIt - _begin < 3)
	{
		return skipOnError(_begin);
	}

	const bool firstCharSuccess = *charIt == '0';
	const bool secondCharSuccess = *(charIt + 1) == 'b';

	if (!firstCharSuccess || !secondCharSuccess)
	{
		return skipOnError(_begin);
	}

	charIt += 2;

	while (charIt < endIt)
	{
		const char32_t character = *charIt;

		if (isSpecialChar(character))
		{
			break;
		}

		const bool isBinary = character == '0' || character == '1';
		if (!isBinary)
		{
			return skipOnError(charIt);
		}

		++charIt;
	}

	{
		// Skipping hex header and checkking just a body, because we already validated header.
		const bwn::string_view tokenView = bwn::string_view(_begin + 2, charIt - _begin);
		uint64_t tempInt;

		if (!bwn::asBinaryBodyStrict(tokenView, tempInt) || tempInt > static_cast<uint64_t>(std::numeric_limits<int64_t>::max()))
		{
			_context.pushError(EErrorType::k_intOverflow, _begin);
			pushToken(_begin, _context.begin(), charIt - _begin, ETokenType::k_invalid);
			return charIt;
		}
	}

	pushToken(_begin, _context.begin(), charIt - _begin, ETokenType::k_binInteger);
	return charIt;
}

bool bwn::console::TokensContext::tryParseBooleanTrueToken(const char32_t*& _it, const InnerContext& _context)
{
	const char32_t*const startIt = _it;

	const bool success = (_context.end() - startIt) >= 4
		&& (*(startIt + 0) == 't' || *(startIt + 0) == 'T')
		&& (*(startIt + 1) == 'r' || *(startIt + 1) == 'R')
		&& (*(startIt + 2) == 'u' || *(startIt + 2) == 'U')
		&& (*(startIt + 3) == 'e' || *(startIt + 3) == 'E');

	if (success)
	{
		pushToken(startIt, _context.begin(), 4, ETokenType::k_booleanTrue);
		_it = startIt + 4;
	}

	return success;
}

bool bwn::console::TokensContext::tryParseBooleanFalseToken(const char32_t*& _it, const InnerContext& _context)
{
	const char32_t*const startIt = _it;

	const bool success = (_context.end() - startIt) >= 5
		&& (*(startIt + 0) == 'f' || *(startIt + 0) == 'F')
		&& (*(startIt + 1) == 'a' || *(startIt + 1) == 'A')
		&& (*(startIt + 2) == 'l' || *(startIt + 2) == 'L')
		&& (*(startIt + 3) == 's' || *(startIt + 3) == 'S')
		&& (*(startIt + 3) == 'e' || *(startIt + 3) == 'E');

	if (success)
	{
		pushToken(startIt, _context.begin(), 5, ETokenType::k_booleanFalse);
		_it = startIt + 5;
	}

	return success;
}

void bwn::console::TokensContext::pushToken(
	const char32_t*const _tokenBegin,
	const char32_t*const _stringBegin,
	const uint32_t _length,
	const ETokenType _type)
{
	m_tokens.push_back(Token(
		bwn::string_view(_tokenBegin, _length),
		_tokenBegin - _stringBegin,
		_type));
}

bool bwn::console::TokensContext::validateExponent(const char32_t*& _it, const char32_t*const _end)
{
	const char32_t* charIt = _it;

	if (_end - charIt < 3)
	{
		return false;
	}

	const char32_t exponentChar = *charIt;
	if (exponentChar != 'E' && exponentChar != 'e')
	{
		return false;
	}

	const char32_t signChar = *(charIt + 1);
	if (signChar != '+' && signChar != '-')
	{
		return false;
	}

	const char32_t firstNumber = *(charIt + 2);
	if (!std::iswdigit(firstNumber))
	{
		return false;
	}

	charIt += 3;
	while(charIt < _end)
	{
		const char32_t character = *charIt;
		if (isSpecialChar(character))
		{
			break;
		}

		if (!std::iswdigit(character))
		{
			return false;
		}

		++charIt;
	}

	_it = charIt;
	return true;
}
