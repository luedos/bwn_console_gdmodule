#pragma once

#include "bwn_core/types/stringView.hpp"

#include <core/string/ustring.h>

#include <limits>
#include <cstdint>

namespace bwn::console
{
	//
	// Forward declarations.
	//
	class TextContext;
	class ErrorsContext;

	enum class ETokenType : uint16_t
	{
		k_invalid,

		// Separator between commands.
		k_commandSeparator,
		// Opening map bracket.
		k_openingMap,
		// Closing map bracket.
		k_closingMap,
		// Map equal sign (like ':' in python)
		k_equal,
		// Opening array bracket.
		k_openingArray,
		// Closing array bracket.
		k_closingArray,

		// Decimal integer (could be with the sigh)
		k_decInteger,
		// Hexadecimal integer.
		k_hexInteger,
		// Octadecimal integer.
		k_octInteger,
		// Binary integer.
		k_binInteger,
		// Floats
		k_float,
		// Boolean 'true' (case insencitive)
		k_booleanTrue,
		// Boolean 'false' (case insencitive)
		k_booleanFalse,
		// String.
		k_string,

	};

	class Token
	{
		//
		// Public constants.
		//
	public:
		static constexpr size_t k_maxStringLength = std::numeric_limits<uint16_t>::max();

		//
		// Constructor and destructor.
		//
	public:
		// Each argument should point somethere in the string, even if string size is 0.
		Token(const char32_t*const _string, const uint32_t _tokenStartPos);
		Token(const bwn::string_view& _string, const uint32_t _tokenStartPos, const ETokenType _type);
		Token(const bwn::string_view& _string, const uint32_t _tokenStartPos, const uint16_t _startPadding, const uint16_t _endPadding, const ETokenType _type);

		//
		// Public interface.
		//
	public:
		// Points to the start of the string.
		const char32_t* begin() const;
		// Points to the end of the string (string length could be 0).
		const char32_t* end() const;;
		// Returns full string for simplicity (paddings included).
		bwn::string_view getFullString() const;
		// Returns just the content string (without paddings).
		bwn::string_view getContentString() const;
		// Returns the relative position of token start, compared to the string start (including paddings).
		uint32_t getStartPos() const;
		// Returns the final position of the token (including paddings).
		uint32_t getEndPos() const;
		// Returns the length of the start padding.
		uint32_t getStartPadding() const;
		// Returns the length of the end padding.
		uint32_t getEndPadding() const;
		// Returns the type of the token.
		ETokenType type() const;

		// Just a helper. (can't return a nullptr)
		static const char* typeToString(const ETokenType _type);

		//
		// Private memebers.
		//
	private:
		// Token string it actually present. Never nullptr, even if full length is 0.
		const char32_t* m_string;
		// The character position relative to the string start
		// (mostly just a helper to properly push errors in other contexts).
		uint32_t m_startPos = 0;
		// This is mostly just a helper, to quickly compute end positions.
		uint32_t m_fullLength = 0;
		// Length of the start padding.
		uint16_t m_startPaddingLength = 0;
		// The length of the string.
		uint16_t m_length = 0;
		// Length of the end padding.
		uint16_t m_endPaddingLength = 0;
		// Type of the token.
		ETokenType m_type = ETokenType::k_invalid;
	};

	class TokensContext
	{
		//
		// Private structs.
		//
	private:
		class InnerContext;

		//
		// Construction and destruction.
		//
	public:
		TokensContext();

		//
		// Public interface.
		//
	public:
		// Parses text into tokens. If there errors while parsing, errors will be pushed into error context.
		// Important note. Context doesn't actually copies the string itself. It only stores the copy of string view.
		void parse(const bwn::string_view& _text, ErrorsContext& _errors);
		// Returns parsed tokens.
		const std::vector<Token>& getTokens() const;
		// Clears tokens stack.
		void clear();
		// Returns true if provided character can't be the part of unquoted string or a number (basically if it's map/array bracket or command divider etc.)
		static bool isSpecialChar(const char32_t _character);
		// Returns true if char can be counted as a quote.
		static bool isQuoteChar(const char32_t _character);
		// Returns true if two quote characters can form a string between them.
		static bool isMatchingQuote(const char32_t _leftQuote, const char32_t _rightQuote);
		// Checks if string requires quote (aka has spaces or special characters in it) and if it does, computes which quote is preferable to use with this string.
		// If string doesn't require any quote, returns '\0'.
		static char32_t computeRequiredQuote(const bwn::string_view& _string);
		// Simply returns true if string has no special characters, etc, and can be copy pasted into as token without any quotes.
		static bool isValidUnquotedString(const bwn::string_view& _string);

		//
		// Private methods.
		//
	private:
		// Forced parsing functions:
		// In the next function the main simularity is that if parsing failes, the function still returns advanced iterator, and pushes error.
		// The token is also pushed, but with the type 'k_invalid'.
		// Tries to parse token as a string (if string starts with quote, this quote should be included as first char).
		const char32_t* forceParseStringToken(const char32_t*const _begin, const InnerContext& _context);
		// Parses token as a number (any number like integer/hex integer or float). Returns false if parsing failed.
		const char32_t* forceParseNumberToken(const char32_t*const _begin, const InnerContext& _context);
		// Those three is specific versions of first one.
		const char32_t* forceParseHexNumberToken(const char32_t*const _begin, const InnerContext& _context);
		const char32_t* forceParseOctNumberToken(const char32_t*const _begin, const InnerContext& _context);
		const char32_t* forceParseBinNumberToken(const char32_t*const _begin, const InnerContext& _context);

		// Safe parsing functions:
		// Next function will not push any tokens/errors if parsing have failed. They are also will return same iterator as _it.
		// Parses token as 'true'/'false'. If parsing failes, returnes same iterator as _it.
		bool tryParseBooleanTrueToken(const char32_t*& _it, const InnerContext& _context);
		bool tryParseBooleanFalseToken(const char32_t*& _it, const InnerContext& _context);

		// Pushes single token into the stack.
		void pushToken(
			const char32_t*const _tokenBegin,
			const char32_t*const _stringBegin,
			const uint32_t _length,
			const ETokenType _type);
		// Validates exponent of the float, and if it's correct, advances iterator to it's end.
		static bool validateExponent(const char32_t*& _it, const char32_t*const _end);

		//
		// Private members.
		//
	private:
		// All the parsed tokens.
		std::vector<Token> m_tokens;
	};
} // namespace bwn::console
